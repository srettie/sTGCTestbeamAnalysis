#include <vector>
#include <TROOT.h>
#include "sTGCTestbeamAnalysis/ChainManager.h"


int main( int argc, char* argv[] ) {
//void run(){  
  //
  // config
  //

  //  TString name = "test1121_v00_old";
  //  TString name = "test1121_v01_noSatur";
  //  TString name = "test1121_v02_noSatur4250";
  //  TString name = "test1121_v03_noSatur4250Error30";
  //  TString name = "test1121_v04_noSatur4250Error30NewFit";
  //  TString name = "test1121_v05_newCatPDOMax3500";

  //  TString name = "test1124_v00_newPedestals";

  //  TString name = "test1126_pedestals";

  //  TString name = "test1127_pedestals";
  //  TString name = "test1127_pedestals_v01Gain1";
  //  TString name = "test1128_pedestals_v02_globlal";

  //  TString name = "test1201_pedestals_v00";
  //  TString name = "test1203_pedestals_v00";
  //  TString name = "test1214_pedestals_v00";

  //  TString name = "test1208_pfit_v00";
  //  TString name = "test1215_pfit_v00";

  //  TString name = "test1216_averageSShapeAmplitudes_v00";

  //  TString name = "test1216_eff_v00";

  //  TString name = "150116_sshape_v00";

  //  TString name = "150120_clfit_v00-W";
  //  TString name = "150120_clfit_v01-I";
  //  TString name = "150120_clfit_v02-WI";

  //  TString name = "150122_clfit_v00-genG";
  //  TString name = "150122_clfit_v01-genG-WI";
  //  TString name = "150122_clfit_v02-genG-noW";

  //  TString name = "150127_clfit_v00-W-meanSShapeAmpl";
  //  TString name = "150127_clfit_v00-W-publicPlots";

  //  TString name = "150127_clfit_v00-genG";

  //  TString name = "150203_v00"; // W now default, but split in PDO max
  //  TString name = "150203_v01_default"; // W now default
  //  TString name = "150217_v00_chi5"; // W now default


  //  TString name = "150326_v00_default";

  //  TString name = "150331_newModel_v01OldSync";
  //  TString name = "150331_newModel_v02NewSync";
  //  TString name = "150331_newModel_v03NewSyncSatCl";
  //  TString name = "150331_newModel_v04NewSync1Plus1SatCl"; // public
  //  TString name = "150331_newModel_v05NewSync1Cat";

  //TString name = "150331_newModel_public";
  TString name = "test_Seb";

  TString descriptorTGCRawTxt = "event/I:channel/I:tdo/I:pdo/I:layerip:globevent";
  //  TString descriptorPixelRawTxt = "event/I:p/C:layer/I:y/D:x/D";
  TString descriptorPixelRawTxt = "event/I:p/C:layer/I:y/D:y_error/D:x/D:x_error/D:cl_y/D:cl_y_error/D:cl_x/D:cl_x_error/D:trk_chi2";
  
  Options opt;
  ////////////
  // TGC chain
  ////////////
  // Copies .smoked data file to .txt file (with same info in the file), and converts the .txt file to a .root file with a TTree
  opt.doMakeTGCRaw       = false;//OK
  
  opt.doPlotTGCRaw       = false;//OK
  opt.doPlotTGCPedestals = false;//OK
  opt.doMakeTGCCluster   = false;//OK
  opt.doPlotTGCCluster   = false;//OK
  opt.doTGCPedestalFit   = false;//OK
  // pixel chain
  opt.doMakePixelRaw     = false;//OK
  opt.doMakePixelTrack   = false;//OK
  // combined
  opt.doMakeCombEvent    = false;//OK
  opt.doPlotCombEvent    = false;//OK
  opt.doResidualFit      = false;//OK...maybe
  opt.doMergeFinalData   = false;//OK
  opt.doPlotFinalData    = false;//OK...maybe
  opt.doMergeFitResult   = false;//OK
  opt.doPlotFitResult    = true;//
  
  
  // fill the run-list providing: number, pixel-file, tgc-file
  vector<RunInput*> runList;
  runList.push_back(new RunInput(321,
				 "input/data.smoked321",
				 "input/trackHitOutfileRun321_v05.txt"));
  runList.push_back(new RunInput(323,
				 "input/data.smoked323",
				 "input/trackHitOutfileRun323_v05.txt"));
  runList.push_back(new RunInput(325,
				 "input/data.smoked325",
				 "input/trackHitOutfileRun325_v05.txt"));
  runList.push_back(new RunInput(326,
  				 "input/data.smoked326",
  				 "input/trackHitOutfileRun326_v05.txt"));
  runList.push_back(new RunInput(334,
  				 "input/data.smoked334",
  				 "input/trackHitOutfileRun334_v05.txt"));
  runList.push_back(new RunInput(335,
     				 "input/data.smoked335",
    				 "input/trackHitOutfileRun335_v05.txt"));
  
  vector<int> pedestalList;
  //   pedestalList.push_back( Pedestals::OldGlobal()  );
  //   pedestalList.push_back( Pedestals::Global()     );
  pedestalList.push_back( Pedestals::PerChannel() );
  //  pedestalList.push_back( Pedestals::Fitted() );

  //  pedestalList.push_back(   0); // 0: nominal pedestals are used

  //   pedestalList.push_back( 700);
  //   pedestalList.push_back( 750);
  //   pedestalList.push_back( 800);
  //   pedestalList.push_back( 850);
  //   pedestalList.push_back( 900);
  //   pedestalList.push_back( 950);
  //   pedestalList.push_back(1000);
  //   pedestalList.push_back(1050);
  //   pedestalList.push_back(1100);
  //   pedestalList.push_back(1150);
  //   pedestalList.push_back(1200);
  //   pedestalList.push_back(1250);
  //   pedestalList.push_back(1300);
  //   pedestalList.push_back(1350);
  //   pedestalList.push_back(1400);

  vector<int> layerList;
  layerList.push_back(1);
  layerList.push_back(2);
  layerList.push_back(3);
  layerList.push_back(4);
  
  
  //
  // execute
  //
  
  ChainManager(name, runList, opt, descriptorTGCRawTxt, descriptorPixelRawTxt, layerList, pedestalList);
  
  //return 0;
}
