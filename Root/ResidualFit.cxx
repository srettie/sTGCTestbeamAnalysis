#define ResidualFit_cxx
#include "sTGCTestbeamAnalysis/ResidualFit.h"
#include <TROOT.h>



//#if !defined(__CINT__) || defined(__MAKECINT__)
//#pragma link C++ class FitResult+;
//#pragma link C++ class std::vector<FitResult*>+;
//#endif


//using namespace std;
//using namespace RooFit;
//using namespace TMath;
//using namespace Mapping;
//using namespace FitTools;

void RunResidualFit(TString in, TString out, int run, int layer, int pedestal)
{
  if (run==325 && layer==2) {
    std::cout << "\n\n\tWARNING:\t RunResidualFit:\t Skipping run 325, layer 2\n\n";
    return;
  }
  

  TString title = TString::Format("Run %i", run);
  title += ", ";
  title += Mapping::GetLayerName(layer);

  TString sel = "cl_nstrip>=3&&cl_nstrip<=5";
  sel += "&&evtoffset==0";
  //  sel += "&&tgcglobevent==event+1";
  sel += "&&TMath::Abs(dy)<2.e4";
  
  /*
  ResidualFit nstrip3("nstrip3", title+", 3 strips/cluster",
		      in, out, sel+"&&cl_nstrip==3",
		      run, layer, pedestal);
  ResidualFit nstrip4("nstrip4", title+", 4 strips/cluster",
		      in, out, sel+"&&cl_nstrip==4",
		      run, layer, pedestal);
  ResidualFit nstrip5("nstrip5", title+", 5 strips/cluster",
		      in, out, sel+"&&cl_nstrip==5",
		      run, layer, pedestal);
  */
  
  //  ResidualFit all("all", title+", all",
  //   		  in, out, sel,
  //   		  run, layer, pedestal);
  
  //  ResidualFit chi210("chi210", title+", #chi_{pixel track}^{2} < 10",

  //   ResidualFit chi210("chi210", title+", #chi^{2} < 5",
  // 		     in, out, sel+"&&trk_chi2<5.",

  ResidualFit chi210("chi210", title+", #chi^{2} < 10",
		     in, out, sel+"&&trk_chi2<10.",
		     run, layer, pedestal);
  
  //   ResidualFit chi205("chi205", title+", #chi^{2} < 5",
  // 		      in, out, sel+"&&trk_chi2<5.",
  // 		      run, layer, pedestal);
  
}

ResidualFit::ResidualFit(TString name, TString title,
			 TString _in, TString _out, TString selection,
			 int _run, int _layer, int _pedestal)
  : Plotter(_in, _out, "comb_evt"),
    dytitle("y_{sTGC} - y_{pixel-track} [#mum]"),
    event        ("event", "", 0.),
    tgcevent     ("tgcevent", "", 0.),
    trk_nhits    ("trk_nhits", "", 0.),
    trk_chi2     ("trk_chi2", "", 0.),
    trk_x        ("trk_x", "x_{pixel-track} [#mum]", -5.5e3, 5.5e3),
    trk_y        ("trk_y", "y_{pixel-track} [#mum]", -12.e3, 12.e3),
    cl_y         ("cl_y" , "y_{sTGC-cluster} [#mum]", 0.),
    cl_yrelStrip ("cl_yrelStrip",
		  "y_{sTGC-cluster}^{rel} (rel. to strip center) [pitch]",
		  -0.5, 0.5),
    cl_yrelGap   ("cl_yrelGap",
		  // "y_{sTGC-cluster}^{rel} (rel. to gap center) [pitch]",
		  "y_{sTGC-cluster}^{rel} [pitch]",
		  -0.5, 0.5),
    cl_nstrip    ("cl_nstrip", "N strips per cluster", 3., 6.),
    cl_raw_pdomax("cl_raw_pdomax", "Raw max. PDO per cluster", 0., 10.e3),
    cl_raw_tdomin("cl_raw_tdomin", "Raw min. TDO per cluster", 0.),
    cl_raw_tdomax("cl_raw_tdomax", "Raw max. TDO per cluster", 0.),
    dy           ("dy", dytitle, 0.)
{
  new_dy = NULL;
  dy_offset0 = NULL;

  fitresult = NULL;
  
  argSet.add(event);
  argSet.add(tgcevent);

  argSet.add(trk_nhits);
  argSet.add(trk_chi2);
  argSet.add(trk_x);
  argSet.add(trk_y);

  argSet.add(cl_y);
  argSet.add(cl_yrelStrip);
  argSet.add(cl_yrelGap);
  argSet.add(cl_nstrip);
  argSet.add(cl_raw_pdomax);
  argSet.add(cl_raw_tdomin);
  argSet.add(cl_raw_tdomax);

  argSet.add(dy);
  
  
  run = _run;
  layer = _layer;
  pedestal = _pedestal;
  globname  = name;
  globtitle = title;
  TTree* tree_sel = tree->CopyTree(selection);
  
  fulldata = new RooDataSet("data", "", tree_sel, argSet);
  // 			    RooArgSet(cl_y, cl_yrelStrip, cl_yrelGap,
  // 				      cl_nstrip,
  // 				      trk_y, trk_x, dy));
  
  // set initial fit range
  // this range will be changed in "BasicFit()"
  // it will be set to: mean +/- fitrangeSize * sigma
  fitmin = -2.e3;
  fitmax =  2.e3;
  fitrangeSize = 5.;
  //  fitrangeSize = 2.;

  plotNBins = 40;
  plotmin = -400.;
  plotmax =  400.;

  if (fulldata->numEntries()>100) Execute();  
}

ResidualFit::~ResidualFit()
{
  delete fulldata;
  //  if (new_dy) delete new_dy;
  if (fitresult) delete fitresult;
}

void ResidualFit::Execute()
{
  
  //   double sigma_raw         = 9999.;
  //   double sigma_raw_error   = 0.;
  
  //   double nevts_full2       = 0.;
  //   double nevts_full2_error = 0.;

  //   double ampl3_full2        = 9999.;
  //   double ampl3_full2_error  = 0.;
  //   double ampl4_full2        = 9999.;
  //   double ampl4_full2_error  = 0.;
  //   double ampl5_full2        = 9999.;
  //   double ampl5_full2_error  = 0.;
  //   double sigma_full2       = 9999.;
  //   double sigma_full2_error = 0.;
  

  GausFitRes res_raw = BasicFit();  

  //  FitResult* fitresult = NULL;
  
  
  //   fitresult = AlignSshapeFit("fullFit",
  //     			     globtitle+", global fit",
  //     			     StrVec("all"),
  //     			     StrVec("1"),
  //     			     RooArgList());
  
  
  fitresult = AlignSshapeFit("fullFit",
      			     globtitle+", global fit",
      			     Tools::StrVec("lowPdoMax;highPdoMax"),
      			     Tools::StrVec("cl_raw_pdomax<4.e3;cl_raw_pdomax>4.e3"),
      			     RooArgList(cl_raw_pdomax));
  
  
  //   fitresult = AlignSshapeFit("fullFit",
  //   			     globtitle+", global fit",
  //   			     StrVec("nstrip3;nstrip4;nstrip5;highPdoMax"),
  //   			     StrVec("cl_nstrip==3&&cl_raw_pdomax<4.e3;cl_nstrip==4&&cl_raw_pdomax<4.e3;cl_nstrip==5&&cl_raw_pdomax<4.e3;cl_raw_pdomax>4.e3"),
  //   			     RooArgList(cl_nstrip, cl_raw_pdomax));
  
  
  //   fitresult = AlignSshapeFit("fullFit",
  //   			     globtitle+", global fit",
  //   			     StrVec("nstrip3;nstrip4;nstrip5"),
  //   			     StrVec("cl_nstrip==3;cl_nstrip==4;cl_nstrip==5"),
  //   			     RooArgList(cl_nstrip));
  
  
  //   fitresult = AlignSshapeFit("singleCat",
  // 			     globtitle+", global fit",
  // 			     StrVec("nstrip345"),
  // 			     StrVec("cl_nstrip>=3&&cl_nstrip<=5"),
  // 			     RooArgList(cl_nstrip));
  

  /*
  fitresult = AlignSshapeFit("fullFit",
			     globtitle+", global fit",
			     StrVec("nstrip3;nstrip4;nstrip5;ns5pdo3500;ns5pdo4000;ns5pdo4250"),
			     StrVec("cl_nstrip==3;cl_nstrip==4;cl_nstrip==5&&cl_raw_pdomax<3500;cl_nstrip==5&&cl_raw_pdomax>3500&&cl_raw_pdomax<4000;cl_nstrip==5&&cl_raw_pdomax>4000&&cl_raw_pdomax<4250;cl_nstrip==5&&cl_raw_pdomax>4250"),
			     RooArgList(cl_nstrip, cl_raw_pdomax));
  */
 
  /*
  fitresult = AlignSshapeFit("full_pdomax1",
			     globtitle+";offset, rot., sine-shape corr.",
			     StrVec("nstrip3;nstrip4;nstrip5;ns5pdo3500;ns5pdo4000;ns5pdo4250"),
			     StrVec("cl_nstrip==3;cl_nstrip==4;cl_nstrip==5&&cl_raw_pdomax<3500;cl_nstrip==5&&cl_raw_pdomax>3500&&cl_raw_pdomax<4000;cl_nstrip==5&&cl_raw_pdomax>4000&&cl_raw_pdomax<4250;cl_nstrip==5&&cl_raw_pdomax>4250"),
			     RooArgList(cl_nstrip, cl_raw_pdomax));
  // 			     StrVec("nstrip3;nstrip4;nstrip5;ns5pdo35;ns5pdo40"),
  // 			     StrVec("cl_nstrip==3;cl_nstrip==4;cl_nstrip==5&&cl_raw_pdomax<3500;cl_nstrip==5&&cl_raw_pdomax>3500&&cl_raw_pdomax<4000;cl_nstrip==5&&cl_raw_pdomax>4000"),
  // 			     RooArgList(cl_nstrip, cl_raw_pdomax));
  */
 
  // setting new_dy
  RooRealVar fullFit_dyFinal("fullFit_dyFinal", "", 0.);
  RooFormulaVar new_dy_f("new_dy", dytitle,
			 "fullFit_dyFinal", RooArgList(fullFit_dyFinal));
  //   RooRealVar dy_full2("dy_full2", "", 0.);
  //   RooFormulaVar new_dy_f("new_dy", "y_{strip-cluster} - y_{pixel-track} [#mum]",
  // 			 "dy_full2", RooArgList(dy_full2));
  new_dy = (RooRealVar*)fulldata->addColumn(new_dy_f);



  TTree* outtree;
  outtree = new TTree("fitresult", "");
  outtree->SetAutoSave(0);
  outtree->SetAutoFlush(0);
  outtree->SetDirectory(0);

  outtree->Branch("run", &run, "run/I");
  outtree->Branch("layer", &layer, "layer/I");
  outtree->Branch("pedestal", &pedestal, "pedestal/I");
  outtree->Branch("name", &globname, "name/C");

  outtree->Branch("full2", &fitresult, 32000, 0);

  outtree->Fill();
  std::cout<<"About to write to file..."<<std::endl;
  TString _outname = out+"/"+globname+"_fitresult.root";
  std::cout<<"Attempting to make directory"<<out<<std::endl;
  system("mkdir -p "+out);
  TFile file(_outname, "recreate");
  std::cout<<"Attempting to write file with filename"<<_outname<<std::endl;
  outtree->Write();
  file.Close();
  outtree->Reset();
  delete outtree;

  //  delete fitresult;

  SaveProcessedData();
}

void ResidualFit::SaveProcessedData()
{
  TTree tree("finaldata", "");
  tree.SetAutoSave(0);
  tree.SetAutoFlush(0);
  tree.SetDirectory(0);

  int m_event;
  int m_tgcevent;
  
  int m_trk_nhits;
  double m_trk_chi2;
  double m_trk_x;
  double m_trk_y;

  double m_cl_y;
  double m_cl_yrelStrip;
  double m_cl_yrelGap;
  int m_cl_nstrip;
  int m_cl_raw_pdomax;
  int m_cl_raw_tdomin;
  int m_cl_raw_tdomax;

  double m_dy;
  double m_dy_offset0;
  double m_new_dy;
  //  double m_dy_full_pdomax1;
  

  tree.Branch("run", &run, "run/I");
  tree.Branch("layer", &layer, "layer/I");  

  tree.Branch("full2", &fitresult, 32000, 0);
  
  tree.Branch("event", &m_event, "event/I");
  tree.Branch("tgcevent", &m_tgcevent, "tgcevent/I");
  
  tree.Branch("trk_nhits", &m_trk_nhits, "trk_nhits/I");
  tree.Branch("trk_chi2", &m_trk_chi2, "trk_chi2/D");
  tree.Branch("trk_x", &m_trk_x, "trk_x/D");
  tree.Branch("trk_y", &m_trk_y, "trk_y/D");
  
  tree.Branch("cl_y", &m_cl_y, "cl_y/D");
  tree.Branch("cl_yrelStrip", &m_cl_yrelStrip, "cl_yrelStrip/D");
  tree.Branch("cl_yrelGap", &m_cl_yrelGap, "cl_yrelGap/D");
  tree.Branch("cl_nstrip", &m_cl_nstrip, "cl_nstrip/I");
  tree.Branch("cl_raw_pdomax", &m_cl_raw_pdomax, "cl_raw_pdomax/I");
  tree.Branch("cl_raw_tdomin", &m_cl_raw_tdomin, "cl_raw_tdomin/I");
  tree.Branch("cl_raw_tdomax", &m_cl_raw_tdomax, "cl_raw_tdomax/I");
  
  tree.Branch("raw_dy", &m_dy, "raw_dy/D");
  tree.Branch("dy_offset0", &m_dy_offset0, "dy_offset0/D");
  tree.Branch("dy", &m_new_dy, "dy/D");
  //  tree.Branch("dy_full_pdomax1", &m_dy_full_pdomax1, "dy_full_pdomax1/D");
  
  //  RooRealVar dy_full_pdomax1("dy_full_pdomax1", "", 0.);


  RooArgSet outArgSet = argSet;
  if (new_dy)     outArgSet.add(*new_dy);
  if (dy_offset0) outArgSet.add(*dy_offset0);
  //  outArgSet.add(dy_full_pdomax1);

  for (int i=0; i<fulldata->numEntries(); i++) {
    outArgSet = *(fulldata->get(i));
    
    m_event         = event        .getVal();
    m_tgcevent      = tgcevent     .getVal();
  
    m_trk_nhits     = trk_nhits    .getVal();
    m_trk_chi2      = trk_chi2     .getVal();
    m_trk_x         = trk_x        .getVal();
    m_trk_y         = trk_y        .getVal();

    m_cl_y          = cl_y         .getVal();
    m_cl_yrelStrip  = cl_yrelStrip .getVal();
    m_cl_yrelGap    = cl_yrelGap   .getVal();
    m_cl_nstrip     = cl_nstrip    .getVal();
    m_cl_raw_pdomax = cl_raw_pdomax.getVal();
    m_cl_raw_tdomin = cl_raw_tdomin.getVal();
    m_cl_raw_tdomax = cl_raw_tdomax.getVal();

    m_dy            = dy           .getVal();

    if (new_dy    ) m_new_dy     = new_dy    ->getVal();
    if (dy_offset0) m_dy_offset0 = dy_offset0->getVal();
    //    m_dy_full_pdomax1 = dy_full_pdomax1.getVal();
    tree.Fill();
  }

  TString _outname = out+"/"+globname+"_finaldata.root";
  std::cout<<"In SaveProcessedData"<<std::endl;
  std::cout<<"Attempting to make directory "<<out<<std::endl;
  system("mkdir -p "+out);
  TFile file(_outname, "recreate");
  std::cout<<"Attempting to write file with filename"<<_outname<<std::endl;
  tree.Write();
  file.Close();
  tree.Reset();
}

GausFitRes ResidualFit::BasicFit()
{
  GausFitRes res = FitTools::GausFit(globname+"_dy_raw", globtitle+";raw residuals",
			   dy, fitmin, fitmax, *fulldata, this,
			   50, fitmin, fitmax, true);
  
  RooFormulaVar dy_offset_f("dy_offset0", dytitle, "@0 - @1",
			    RooArgList(dy, RooConst(res.mean.value) ));
  dy_offset0 = (RooRealVar*)fulldata->addColumn(dy_offset_f);
  
  // set fit range
  fitmin = res.mean.value - fitrangeSize*res.sigma.value;
  fitmax = res.mean.value + fitrangeSize*res.sigma.value;
  
  PlotGausFit(globname+"_dy_offset0",
	      globtitle+";raw residuals, offset corrected",
   	      *dy_offset0);
  
  return res;
}

FitResult* ResidualFit::AlignSshapeFit(TString name, TString title,
				       vector<TString> catName,
				       vector<TString> catSel,
				       RooArgList catArgList)
{
  if (catName.size()!=catSel.size()) {
    std::cout << "\n\nERROR:\tResidualFit::AlignSshapeFit:\t"
	 << "catName.size()!=catSel.size()" << std::endl;
    exit(1);
  }
  int nCats = catName.size();

  RooDataSet* data = FitTools::SelectData(*fulldata, dy, fitmin, fitmax);

  dy.setMin(fitmin);
  dy.setMax(fitmax);
  

  //  
  //  MODEL
  //

  // align model
  double rawmean = data->mean(dy);
  RooRealVar offset("offset", "", rawmean, fitmin, fitmax);
  RooRealVar phi("phi", "", 0., -20., 20.);
  RooFormulaVar trk_y_corr("trk_y_corr", "",
			   "-1.*trk_x*sin(phi*TMath::DegToRad()) + trk_y*cos(phi*TMath::DegToRad()) + offset - trk_y",
			   RooArgList(trk_x, trk_y, offset, phi) );
  
  
  // S-shape model

  // amplitude
  vector<RooRealVar*> catAmpl;
  TString ampl_str("");
  RooArgList ampl_argList(catArgList);
  for (int i=0; i<nCats; i++) {
    TString _name = "ampl_"+catName[i];
    TString _sel = catSel[i];
    RooRealVar* _ampl = new RooRealVar(_name, "", 100., -50., 500.);
    catAmpl.push_back(_ampl);
    if (ampl_str!="") ampl_str += " + ";
    ampl_str += _name+"*("+_sel+")";
    ampl_argList.add(*_ampl);
  }
  RooFormulaVar ampl("ampl", "", ampl_str, ampl_argList);
  ampl.Print();
  
  // sine function
  RooFormulaVar sine_corr("sine_corr", "",
			  "ampl*TMath::Sin(2.*TMath::Pi()*cl_yrelGap)",
			  RooArgList(cl_yrelGap, ampl) );


  
  // model
  RooFormulaVar mean("mean", "", "trk_y_corr + sine_corr",
		     RooArgList(trk_y_corr, sine_corr) );
  RooRealVar sigma("sigma", "", 50., 10., 90.);
  RooGaussian gauss("gauss","", dy, mean, sigma);
  
  RooUniform bkgFlat("bkgFlat", "", dy);

  // signal fraction
  vector<RooRealVar*> catSigfrac;
  TString sigfrac_str("");
  RooArgList sigfrac_argList(catArgList);
  for (int i=0; i<nCats; i++) {
    TString _name = "sigfrac_"+catName[i];
    TString _sel = catSel[i];
    RooRealVar* _sigfrac = new RooRealVar(_name, "", 0.90, 0., 1.);
    catSigfrac.push_back(_sigfrac);
    if (sigfrac_str!="") sigfrac_str += " + ";
    sigfrac_str += _name+"*("+_sel+")";
    sigfrac_argList.add(*_sigfrac);
  }
  RooFormulaVar sigfracPerCat("sigfracPerCat", "",
			      sigfrac_str, sigfrac_argList);
  sigfracPerCat.Print();


  RooRealVar goodFrac  ("goodFrac" , "", 0.8, 0.6, 1.);
  RooRealVar wideSigma ("wideSigma", "", 300., 100., 3000.);
  RooGaussian wideGauss("wideGauss", "", dy, mean, wideSigma);  
  
  
  RooRealVar& frac = goodFrac;
  //  RooRealVar& frac = sigfracPerCat;
  
  RooAbsPdf& outPdf = wideGauss;
  //  RooAbsPdf& outPdf = bkgFlat;
  
  
  RooAddPdf pdf("pdf", "", RooArgList(gauss, outPdf), frac);
  //  RooAddPdf pdf("pdf", "", RooArgList(gauss, wideGauss), goodFrac);
  //  RooAddPdf pdf("pdf", "", RooArgList(gauss, bkgFlat), sigfrac);
  
   

  // fit
    
  offset   .setConstant(false);
  phi      .setConstant(false);
  sigma    .setConstant(false);
  goodFrac .setConstant(false);
  wideSigma.setConstant(false);
  for (int i=0; i<nCats; i++) catAmpl   [i]->setConstant(false);
  for (int i=0; i<nCats; i++) catSigfrac[i]->setConstant(false);

  //   //
  //   // test setting the amplitudes constant at the average values
  //   for (int i=0; i<nCats; i++) catAmpl   [i]->setConstant(true);
  //   catAmpl[0]->setVal(204. );
  //   catAmpl[1]->setVal(112.4);
  //   catAmpl[2]->setVal(104.9);
  //   //

  //   //
  //   // cluster fit with "W" option
  //   // test setting the amplitudes constant at the average values
  //   for (int i=0; i<nCats; i++) catAmpl   [i]->setConstant(true);
  //   catAmpl[0]->setVal(205. );
  //   catAmpl[1]->setVal(206.);
  //   catAmpl[2]->setVal(211.);
  //   //
  //   //


  RooArgSet condObs(catArgList);
  condObs.add(RooArgSet(trk_x, trk_y, cl_yrelGap));

  dy.setRange("fitrange", fitmin, fitmax);
  RooFitResult* result = pdf.fitTo(*data, Save(), Range("fitrange"),
				   ConditionalObservables(condObs),
				   Minimizer("Minuit2", "minimize"));



  
  // result
  
  std::cout << "\n\n\t Gaussian fit result, for: " << title
       << "\n\t Fit range: " << fitmin << " , " << fitmax << std::endl;
  result->Print();
  std::cout << "\n\t Correlation matrix: " << std::endl;
  TMatrixDSym matrix = result->correlationMatrix();
  matrix.Print();
  TH2D matrixh(matrix);
  matrixh.Scale(100.);

  RooArgList pars = result->floatParsFinal();
  for (int i=0; i<pars.getSize(); i++) {
    matrixh.GetXaxis()->SetBinLabel(i+1, pars.at(i)->GetName());
    matrixh.GetYaxis()->SetBinLabel(i+1, pars.at(i)->GetName());
  }  

  gStyle->SetPaintTextFormat(".1f");
  matrixh.GetZaxis()->SetTitle("Correlation [%]");
  matrixh.Draw("zcol text");
  c->SetRightMargin(0.18);
  SetPlotTitle(title+";Fit correlation matrix");
  SavePlot(globname+"_"+name+"_fitCorrelations");
  c->SetRightMargin(0.05);
  
  
  FitResult* out = new FitResult;
  out->nevtsTot = data->numEntries();
  out->status = result->status();
  out->covqual = result->covQual();
  out->offset   .Set(offset   .getVal(), offset   .getError());
  out->phi      .Set(phi      .getVal(), phi      .getError());
  out->sigma    .Set(sigma    .getVal(), sigma    .getError());
  out->wideSigma.Set(wideSigma.getVal(), wideSigma.getError());
  out->goodFrac .Set(goodFrac .getVal(), goodFrac .getError());
  
  for (int cat=0; cat<nCats; cat++) {

    int _nevtsTot = data->sumEntries(catSel[cat]);
    Parameter _ampl   (catAmpl   [cat]->getVal(), catAmpl   [cat]->getError());
    Parameter _sigfrac(catSigfrac[cat]->getVal(), catSigfrac[cat]->getError());
    Parameter _nevts  (_nevtsTot*catSigfrac[cat]->getVal(),
		       _nevtsTot*catSigfrac[cat]->getError());

    out->catNevtsTot.push_back(_nevtsTot);
    out->catAmpl    .push_back(_ampl    );
    out->catSigfrac .push_back(_sigfrac );
    out->catNevts   .push_back(_nevts   );
  }
  
  dy.removeMin();
  dy.removeMax();



  //
  //  PLOTS
  //

  
  RooFormulaVar dyOffset_f(name+"_dyOffset", dytitle, "dy - offset",
			   RooArgList(dy, offset ));
  RooFormulaVar dyAlign_f (name+"_dyAlign" , dytitle, "dy - trk_y_corr",
			   RooArgList(dy, trk_y_corr ));
  RooFormulaVar dySshape_f(name+"_dySshape", dytitle,
			   "dy - offset - sine_corr",
			   RooArgList(dy, offset, sine_corr ));
  RooFormulaVar dyFinal_f (name+"_dyFinal" , dytitle, "dy - mean",
			   RooArgList(dy, mean ));
  
  RooRealVar* dyOffset = (RooRealVar*)fulldata->addColumn(dyOffset_f);
  RooRealVar* dyAlign  = (RooRealVar*)fulldata->addColumn(dyAlign_f );
  RooRealVar* dySshape = (RooRealVar*)fulldata->addColumn(dySshape_f);
  RooRealVar* dyFinal  = (RooRealVar*)fulldata->addColumn(dyFinal_f );
  
  RooFormulaVar trk_x_mm_f("trk_x_mm", "x_{pixel-track} [mm]", "trk_x/1.e3",
			   RooArgList(trk_x) );
  RooRealVar* trk_x_mm = (RooRealVar*)fulldata->addColumn(trk_x_mm_f);
  
  RooFormulaVar alignCorrVsX("alignCorrVsX", "",
			     "-1.*trk_x_mm*1.e3*sin(phi*TMath::DegToRad())",
			     RooArgList(*trk_x_mm, phi) );
  TF1* alignCorrVsX_f = alignCorrVsX.asTF(RooArgList(*trk_x_mm),
					  RooArgList(phi) );
  
  PlotGausFit(globname+"_"+name+"_dyOffset",
	      title+";Corrected for: #deltay-offset", *dyOffset);
  PlotDyVsVar(globname+"_"+name+"_dyOffset_vsXtrk",
	      title+";Corrected for: #deltay-offset", *dyOffset,
	      Binning(40, -400., 400.),
	      *trk_x_mm, Binning(10, -5, 5), *fulldata, alignCorrVsX_f);
  
  PlotResiduals(globname+"_"+name+"_dyAlignDataOnly",
		title+";Corrected for: #deltay-offset, rotation", *dyAlign);
  PlotGausFit(globname+"_"+name+"_dyAlign",
	      title+";Corrected for: #deltay-offset, rotation", *dyAlign);
  PlotDyVsVar(globname+"_"+name+"_dyAlign_vsXtrk",
	      title+";Corrected for: #deltay-offset, rotation", *dyAlign,
	      Binning(40, -400., 400.),
	      *trk_x_mm, Binning(10, -5, 5), *fulldata);
  
  PlotGausFit(globname+"_"+name+"_dySshape",
	      title+";Corrected for: #deltay-offset, s-shape", *dySshape);
  PlotDyVsVar(globname+"_"+name+"_dySshape_vsXtrk",
	      title+";Corrected for: #deltay-offset, s-shape", *dySshape,
	      Binning(20, -200., 200.),
	      *trk_x_mm, Binning(10, -5, 5), *fulldata, alignCorrVsX_f);
  
  PlotGausFit(globname+"_"+name+"_dyFinal",
	      title+";All corrections applied", *dyFinal);
  PlotDyVsVar(globname+"_"+name+"_dyFinal_vsXtrk",
	      title+";All corrections applied", *dyFinal,
	      Binning(20, -200., 200.),
	      *trk_x_mm, Binning(10, -5, 5), *fulldata);  
  
  for (int i=0; i<nCats; i++) {
    
    RooFormulaVar corr("corr", "",
		       "@0*TMath::Sin(2.*TMath::Pi()*@1)",
		       RooArgList(*catAmpl[i], cl_yrelGap) );
    TF1* corrf = corr.asTF(RooArgList(cl_yrelGap), RooArgList(*catAmpl[i]) );

    RooDataSet* catdata = (RooDataSet*)fulldata->reduce(catSel[i]);
    PlotDyVsVar(globname+"_"+name+"_dyAlign_vsYrelGap_"+catName[i],
		title+";Corrected for: #deltay-offset, rotation",
		*dyAlign, Binning(40, -400., 400.),
		cl_yrelGap, Binning(10), *catdata, corrf);
    PlotDyVsVar(globname+"_"+name+"_dyFinal_vsYrelGap_"+catName[i],
		title+";All corrections applied",
		*dyFinal, Binning(40, -400., 400.),
		cl_yrelGap, Binning(10), *catdata);

    GausFitRes res = PlotGausFit(globname+"_"+name+"_dyFinal_"+catName[i],
				 title+";All corrections applied, "+catName[i],
				 *dyFinal, catdata);
    out->catSigma.push_back(res.sigma);
    
    delete corrf;
    delete catdata;
  }
  
  // after s-shape corr.
  /*
  
  RooRealVar b0("b0", "", 0.);
  RooRealVar b1("b1", "", 0.);
  RooFormulaVar pol1("pol1", "",
		     "b1*cl_yrelGap + b0",
		     RooArgList(cl_yrelGap, b0, b1) );
  RooGaussian ctestGauss("ctestGauss","", *dyFinal, pol1, sigma);
  RooRealVar mf("mf", "", 0.90, 0., 1.);
  RooUniform ctestBkg("ctestBkg", "", *dyFinal);
  RooAddPdf ctestPdf("ctestPdf", "", RooArgList(ctestGauss, ctestBkg), mf);
  
  b0.setConstant(false);
  b1.setConstant(false);
  
  RooDataSet* ctestData = SelectData(*fulldata, *dyFinal, -800., 800.);
  
  RooFitResult* ctestResult = ctestPdf.fitTo(*ctestData, Range("fitrange"),
					     Save(),
					     //  RooFitResult* ctestResult = ctestPdf.fitTo(*fulldata, Range("fitrange"), Save(),
					     ConditionalObservables(RooArgSet(cl_yrelGap)),
					     Minimizer("Minuit2", "minimize"));
  ctestResult->Print();
  delete ctestData;
  delete ctestResult;
  
  */
  //


  delete alignCorrVsX_f;
  delete result;  
  delete data;
  for (int i=0; i<nCats; i++) delete catAmpl   [i];
  for (int i=0; i<nCats; i++) delete catSigfrac[i];
  
  return out;
}

GausFitRes ResidualFit::PlotGausFit(TString name, TString title,
				    RooRealVar& var,
				    RooDataSet* dat)
{
  if (!dat) dat = fulldata;

  double _max = (fitmax-fitmin)/2.;
  double _min = -_max;
  return FitTools::GausFit(name, title, var, _min, _max, *dat,
		 this, plotNBins, plotmin, plotmax);
}

void ResidualFit::PlotResiduals(TString name, TString title, RooRealVar& var,
				RooDataSet* dat)
{
  if (!dat) dat = fulldata;
  
  Plot(name, title, var, plotNBins, plotmin, plotmax, *dat);
}

void ResidualFit::PlotDyVsVar(TString name, TString title,
			      RooRealVar& dy, RooCmdArg dy_bins,
			      RooRealVar& var, RooCmdArg var_bins,
			      RooDataSet& dat, TF1* f)
{
  TH1* h = dat.createHistogram("h", var, var_bins, YVar(dy, dy_bins));
  h->Draw("box");
  if (f) {
    f->SetLineColor(2);
    f->Draw("same");
  }
  c->SetGridx();  c->SetGridy();
  SetPlotTitle(title);
  SavePlot(name);
  c->SetGridx(0);  c->SetGridy(0);
  
  delete h;
}
