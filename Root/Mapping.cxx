#define Mapping_cxx
#include "sTGCTestbeamAnalysis/Mapping.h"


//using namespace std;

double Mapping::GetPixelLayerZ(int layer)
{
  if (layer<0 || layer>5) {
    std::cout << "\n\nERROR:\tMapping::GetPixelLayerZ:\tNO info available for layer "
	 << layer
	 << "\n\n\n";
    exit(1);
  }
  
  return pixellayer_z[layer];
}

int Mapping::GetLayerIP(int run, int layer, Mapping::LAYERTYPE type)
{
  if (layer<1 || layer>4) {
    std::cout << "\n\nWARNING:\tMapping::GetLayerIP:\tNO info available for layer "
      //cout << "\n\nERROR:\tMapping::GetLayerIP:\tNO info available for layer "
	 << layer
	 << "\n\n\n";
    //    exit(1);
    return -1;
  }

  if (type!=STRIPLAYER) {
    std::cout << "\n\nERROR:\tMapping::GetLayerIP:\tNO info available for type "
	 << type
	 << "\n\n\n";
    exit(1);
  }

  if (run<321 || run>335) {
    std::cout << "\n\nERROR:\tMapping::GetLayerIP:\tNO info available for run "
	 << run
	 << "\n\n\n";
    exit(1);
  }

  int ip[] = { 25, 18, 29, 19};

  return ip[layer-1];
}

int Mapping::GetLayerNumber(int run, int layerip, Mapping::LAYERTYPE type)
{
  for (int layer=1; layer<=4; layer++)
    if (layerip==GetLayerIP(run, layer, type))
      return layer;
  
  std::cout << "\n\nERROR:\tMapping::GetLayerNumber:\tNO info available for layerIP "
       << layerip
       << ", in run " << run << ", type " << type
       << "\n\n\n";
  exit(1);
}

TString Mapping::GetLayerName(int layer, Mapping::LAYERTYPE type)
{
  if (layer<1 || layer>4) {
    std::cout << "\n\nERROR:\tMapping::GetLayerName:\tNO info available for layer "
	 << layer
	 << "\n\n\n";
    exit(1);
  }

  TString name = "";
  if (type==STRIPLAYER) name = "layer%i";
  else if (type==PADLAYER) name = "L%iP";
  else {
    std::cout << "\n\nERROR:\tMapping::GetLayerName:\tNO info available for type "
	 << type
	 << "\n\n\n";
    exit(1);
  }

  return TString::Format(name.Data(), layer);
}

double Mapping::PositionStrip(int couche, int StripNumber, double &size)
{
  if(StripNumber < 1 || couche < 1 ) {std::cout<<"Error : Invalid number enter : StripNumber and layer must be >=1"<<std::endl; return 99999;}
  if(StripNumber > NbStrips) {std::cout<<"Error : Strip Number : "<<StripNumber<<" is above "<< NbStrips <<std::endl; return 99999;}
  if(couche > 4) {std::cout<<"Error : Layer Number : "<<couche<<" is above 4"<<std::endl; return 99999;}

  int layer = couche;
  if(couche>2) layer = couche-2;
  size =  WidthStrip;
  if((layer==1 && StripNumber==1) || (layer==2 && StripNumber==365)) size = 0.5*WidthStrip;
  //   double PositionFirstStrip[2]= {0.25*WidthStrip,0.5*WidthStrip};
  //   double PositionSecondStrip[2]= {WidthStrip,1.5*WidthStrip};
  //  //  double PositionSecondStrip[2]= {WidthStrip,0.5*WidthStrip};
  double PositionFirstStrip[2]= {0.5*WidthStrip,0.25*WidthStrip};
  double PositionSecondStrip[2]= {1.5*WidthStrip,WidthStrip};
  double PositionLastStrip[2]= {(H2active-H1active)-0.5*WidthStrip,(H2active-H1active)-0.25*WidthStrip};
  double Position = PositionSecondStrip[layer-1] + 3.2*(StripNumber-2) + (H1active - H1chamber);

  if(StripNumber==1) return (PositionFirstStrip[layer-1] + (H1active - H1chamber));
  else if(StripNumber==365) return (PositionLastStrip[layer-1] + (H1active - H1chamber));
  else return(Position);
}

//int Mapping::ChannelToStrip(int channel, string IPadress, bool& IsStripNoisy)
int Mapping::ChannelToStrip(int channel, std::string IPadress)
{
  channel = channel+1;
  if(channel<1 || channel>64) {std::cout<<"Error : channel is between 1 and 64"<<std::endl; return -1;}

  int VMM=-9, Cable=-9, Jack=-9, layer=-9, Connector=-9, Ethernet=-9, Power=-9;
  CardMapping(IPadress,VMM,Cable,Jack,layer,Connector,Ethernet,Power);
  if(VMM==-9 || Cable==-9 || Jack==-9 || layer==-9 || Connector==-9 ||  Ethernet==-9 || Power==-9) {std::cout<<"Error inside channel mapping association : IPadress does not exist"<<std::endl; return -1;}
  
  //cout<<"IPadress="<<IPadress<<" VMM="<< VMM <<" Cable="<< Cable <<" Jack="<< Jack<<" layer="<<layer<<" Connector="<< Connector <<" Ethernet="<< Ethernet <<" Power="<<Power<<endl;

  if(Connector!=3 && Connector!=4) {std::cout<<" Error : You are trying to read Pad not Strips"<<std::endl; return -1;}
  if(layer==4){
    if(Connector==3 && (channel==37 || channel==38)) {std::cout<<"Warning : For connector L4S3 the Channel "<<channel<<" is connected to a shorted Strip "<<std::endl;}
    if(Connector==4 && (channel==13 || channel==14)) {std::cout<<"Warning : For connector L4S4 the Channel "<<channel<<" is connected to a shorted Strip "<<std::endl;}
  }
  
  int FirstStrip = 28 + 64*(Connector-1);
  int StripRead = -1;
  if(layer==1 || layer==4) StripRead = channel + FirstStrip -1;
  if(layer==2 || layer==3) StripRead = abs(channel-64) + FirstStrip;

  /*
    IsStripNoisy = false;
    for(int i=0; i<NoisyStrip[layer-1].size(); i++){
    if(NoisyStrip[layer-1].at(i) == StripRead) IsStripNoisy=true;
    }
    if(IsStripNoisy) {cout<<"Warning : Strip "<<StripRead<<" define as Noisy"<<endl;}
  */

  return(StripRead);
} 

void Mapping::CardMapping(std::string IPadress, int& VMM, int& Cable, int& Jack, int& layer, int& Connector, int& Ethernet, int& Power)
{
  //if(IPadress=="10.0.0.16") {VMM = 5;   Cable = 1;  Jack = 10; layer = 2; Connector = 0; Ethernet = 16; Power = 9;}
  //if(IPadress=="10.0.0.17") {VMM = 100; Cable = 4;  Jack = 2;  layer = 2; Connector = 4; Ethernet = 2;  Power = 2;}
  //  if(IPadress=="10.0.0.18") {VMM = 102; Cable = 3;  Jack = 11; layer = 2; Connector = 3; Ethernet = 3;  Power = 3;}
  //  if(IPadress=="10.0.0.19") {VMM = 107; Cable = 9;  Jack = 7;  layer = 4; Connector = 3; Ethernet = 15; Power = 6;}
  //if(IPadress=="10.0.0.20") {VMM = 106; Cable = 10; Jack = 1;  layer = 3; Connector = 4; Ethernet = 9;  Power = 7;}
  //if(IPadress=="10.0.0.21") {VMM = 115; Cable = 8;  Jack = 0;  layer = 1; Connector = 4; Ethernet = 11; Power = 11;}
  //  if(IPadress=="10.0.0.22") {VMM = 112; Cable = 14; Jack = 9;  layer = 3; Connector = 3; Ethernet = 10; Power = 10;}	
  //if(IPadress=="10.0.0.23") {VMM = 113; Cable = 6;  Jack = 3;  layer = 3; Connector = 0; Ethernet = 4;  Power = 4;}
  //if(IPadress=="10.0.0.24") {VMM = 7;   Cable = 15; Jack = 8;  layer = 1; Connector = 3; Ethernet = 1;  Power = 5;}
  //  if(IPadress=="10.0.0.25") {VMM = 4;   Cable = 7;  Jack = 12; layer = 1; Connector = 3; Ethernet = 12; Power = 12;}
  //if(IPadress=="10.0.0.26") {VMM = 108; Cable = 13; Jack = 6;  layer = 1; Connector = 0; Ethernet = 14; Power = 8;}
  //if(IPadress=="10.0.0.27") {VMM = 114; Cable = 11; Jack = 15; layer = 4; Connector = 4; Ethernet = 13; Power = 1;}


  if(IPadress=="10.0.0.25") {VMM = 100;   Cable = 15;  Jack = 12; layer = 1; Connector = 3; Ethernet = 12; Power = 12;}
  if(IPadress=="10.0.0.18") {VMM = 107; Cable = 9;  Jack = 11; layer = 2; Connector = 3; Ethernet = 3;  Power = 3;}
  if(IPadress=="10.0.0.29") {VMM = 4; Cable = 7;  Jack = 13; layer = 3; Connector = 3; Ethernet = 1;  Power = 11;}
  if(IPadress=="10.0.0.19") {VMM = 7; Cable = 3;  Jack = 7;  layer = 4; Connector = 3; Ethernet = 15; Power = 6;}  
}
