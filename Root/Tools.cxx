#define Tools_cxx
#include "sTGCTestbeamAnalysis/Tools.h"

NEvt Tools::GetNEvt(TChain& chain, TString selection, TString wname) {

  TCanvas c;
  c.cd();
  TH1D hist("hist","", 1, 0., 1.);
  chain.Draw("0.5>>hist","("+selection+")*"+wname);
  NEvt nevt(hist.GetBinContent(1), hist.GetBinError(1));
  return nevt;
}


int Tools::Magnitude(double x)
{
  if (TMath::Abs(x)<=1.e-19) return 0;

  int mag = (int)TMath::Log10(x);
  if (x>1.) mag++;
  if (x < 0.495000*pow(10., mag)) mag--;
  return mag;
}

int Tools::NDecimals(double x, int pre)
{
  int mag = Magnitude(x);
  int ndec = 0;
  if (mag<=0) ndec = -1.*mag + pre - 1;
  return ndec;
}

TString Tools::PrintVal(double val, double err, int pre)
{
  int ndec = NDecimals(TMath::Abs(err), pre);
  TString format = TString::Format("%%.%if", ndec);

  return TString::Format(format, val);
}

TString Tools::Print(double x, int pre)
{
  return PrintVal(x, x, pre);
}

TString Tools::PrintErr(double err, int pre)
{
  return Print(err, pre);
}

TString Tools::Print(double val, double err, int pre)
{
  TString ret = PrintVal(val, err, pre);
  //  ret += " +/- ";
  ret += " #pm ";
  ret += PrintErr(err, pre);
  return ret;
}

TString Tools::PrintRawAndClean(float val, float err, int pre)
{
  TString ret = TString::Format("%f ± %f", val, err);
  ret += "\t->\t";
  ret += Print(val, err, pre);
  return ret;
}

std::vector<TString> Tools::StrVec(TString str, TString delim)
{
  TObjArray* array = str.Tokenize(delim);
  std::vector<TString> out;
  for (int i=0; i<array->GetSize(); i++) {
    TObjString* _objstr = (TObjString*)array->At(i);
    if (_objstr) {
      if (_objstr->String()!="") out.push_back(_objstr->String());
    }
  }
  
  delete array;
  return out;
}

int Tools::PedestalLayer3(int ch)
{
  if (ch==0) return TMath::Nint(807.25);
  if (ch==1) return TMath::Nint(807.733);
  if (ch==2) return TMath::Nint(789.938);
  if (ch==3) return TMath::Nint(812.8);
  if (ch==4) return TMath::Nint(811.812);
  if (ch==5) return TMath::Nint(811.125);
  if (ch==6) return TMath::Nint(807.933);
  if (ch==7) return TMath::Nint(782.933);
  if (ch==8) return TMath::Nint(787.938);
  if (ch==9) return TMath::Nint(807.875);
  if (ch==10) return TMath::Nint(802.875);
  if (ch==11) return TMath::Nint(807.062);
  if (ch==12) return TMath::Nint(812.467);
  if (ch==13) return TMath::Nint(803.467);
  if (ch==14) return TMath::Nint(794.438);
  if (ch==15) return TMath::Nint(800.267);
  if (ch==16) return TMath::Nint(828.867);
  if (ch==17) return TMath::Nint(803.067);
  if (ch==18) return TMath::Nint(779.5);
  if (ch==19) return TMath::Nint(794.75);
  if (ch==20) return TMath::Nint(804.4);
  if (ch==21) return TMath::Nint(797);
  if (ch==22) return TMath::Nint(794.812);
  if (ch==23) return TMath::Nint(783.688);
  if (ch==24) return TMath::Nint(800.6);
  if (ch==25) return TMath::Nint(798.188);
  if (ch==26) return TMath::Nint(812.267);
  if (ch==27) return TMath::Nint(815.5);
  if (ch==28) return TMath::Nint(789.133);
  if (ch==29) return TMath::Nint(796.733);
  if (ch==30) return TMath::Nint(802.562);
  if (ch==31) return TMath::Nint(789);
  if (ch==32) return TMath::Nint(794.867);
  if (ch==33) return TMath::Nint(799.267);
  if (ch==34) return TMath::Nint(810.688);
  if (ch==35) return TMath::Nint(798.8);
  if (ch==36) return TMath::Nint(813.875);
  if (ch==37) return TMath::Nint(804);
  if (ch==38) return TMath::Nint(817.25);
  if (ch==39) return TMath::Nint(799.688);
  if (ch==40) return TMath::Nint(810.333);
  if (ch==41) return TMath::Nint(814.625);
  if (ch==42) return TMath::Nint(815.8);
  if (ch==43) return TMath::Nint(791.4);
  if (ch==44) return TMath::Nint(811.938);
  if (ch==45) return TMath::Nint(814.438);
  if (ch==46) return TMath::Nint(818.133);
  if (ch==47) return TMath::Nint(795.312);
  if (ch==48) return TMath::Nint(803.938);
  if (ch==49) return TMath::Nint(809.562);
  if (ch==50) return TMath::Nint(792.267);
  if (ch==51) return TMath::Nint(823.867);
  if (ch==52) return TMath::Nint(816.375);
  if (ch==53) return TMath::Nint(804.875);
  if (ch==54) return TMath::Nint(794.562);
  if (ch==55) return TMath::Nint(798.438);
  if (ch==56) return TMath::Nint(811.688);
  if (ch==57) return TMath::Nint(809.125);
  if (ch==58) return TMath::Nint(793.938);
  if (ch==59) return TMath::Nint(797.625);
  if (ch==60) return TMath::Nint(794.533);
  if (ch==61) return TMath::Nint(809.188);
  if (ch==62) return TMath::Nint(797.812);
  if (ch==63) return TMath::Nint(797.812);
  std::cout << "\n\nERROR:\tTools PedestalLayer3(int ch):\t"
	    << "didn't find pedestal" << std::endl;
  exit(1);
}

int Tools::PedestalLayer4(int ch)
{
  if (ch==0) return TMath::Nint(890.25);
  if (ch==1) return TMath::Nint(878.688);
  if (ch==2) return TMath::Nint(900.625);
  if (ch==3) return TMath::Nint(900.4);
  if (ch==4) return TMath::Nint(881);
  if (ch==5) return TMath::Nint(898.375);
  if (ch==6) return TMath::Nint(901.438);
  if (ch==7) return TMath::Nint(870.533);
  if (ch==8) return TMath::Nint(919.75);
  if (ch==9) return TMath::Nint(889.188);
  if (ch==10) return TMath::Nint(901.067);
  if (ch==11) return TMath::Nint(890.214);
  if (ch==12) return TMath::Nint(911.4);
  if (ch==13) return TMath::Nint(894.938);
  if (ch==14) return TMath::Nint(876.75);
  if (ch==15) return TMath::Nint(880.062);
  if (ch==16) return TMath::Nint(884.375);
  if (ch==17) return TMath::Nint(885.125);
  if (ch==18) return TMath::Nint(900.938);
  if (ch==19) return TMath::Nint(898.25);
  if (ch==20) return TMath::Nint(909.938);
  if (ch==21) return TMath::Nint(889.867);
  if (ch==22) return TMath::Nint(894.133);
  if (ch==23) return TMath::Nint(901.062);
  if (ch==24) return TMath::Nint(896.125);
  if (ch==25) return TMath::Nint(876.188);
  if (ch==26) return TMath::Nint(913.4);
  if (ch==27) return TMath::Nint(906.75);
  if (ch==28) return TMath::Nint(906.562);
  if (ch==29) return TMath::Nint(889.733);
  if (ch==30) return TMath::Nint(897.938);
  if (ch==31) return TMath::Nint(898.667);
  if (ch==32) return TMath::Nint(885);
  if (ch==33) return TMath::Nint(906.8);
  if (ch==34) return TMath::Nint(893.333);
  if (ch==35) return TMath::Nint(889.75);
  if (ch==36) return TMath::Nint(883.188);
  if (ch==37) return TMath::Nint(879.812);
  if (ch==38) return TMath::Nint(893.188);
  if (ch==39) return TMath::Nint(900.625);
  if (ch==40) return TMath::Nint(881.25);
  if (ch==41) return TMath::Nint(896.75);
  if (ch==42) return TMath::Nint(875.333);
  if (ch==43) return TMath::Nint(905.062);
  if (ch==44) return TMath::Nint(897.267);
  if (ch==45) return TMath::Nint(885.562);
  if (ch==46) return TMath::Nint(880.688);
  if (ch==47) return TMath::Nint(880.8);
  if (ch==48) return TMath::Nint(883.6);
  if (ch==49) return TMath::Nint(900.062);
  if (ch==50) return TMath::Nint(893.875);
  if (ch==51) return TMath::Nint(903.867);
  if (ch==52) return TMath::Nint(900.5);
  if (ch==53) return TMath::Nint(884.133);
  if (ch==54) return TMath::Nint(893.875);
  if (ch==55) return TMath::Nint(878.467);
  if (ch==56) return TMath::Nint(926.5);
  if (ch==57) return TMath::Nint(904.812);
  if (ch==58) return TMath::Nint(901.312);
  if (ch==59) return TMath::Nint(916.625);
  if (ch==60) return TMath::Nint(904.467);
  if (ch==61) return TMath::Nint(897.75);
  if (ch==62) return TMath::Nint(892.222);
  if (ch==63) return TMath::Nint(892.222);
  std::cout << "\n\nERROR:\tTools PedestalLayer3(int ch):\t"
	    << "didn't find pedestal" <<std:: endl;
  exit(1);
}

int Tools::GetRefinedPedestal(int layer, int ch)
{
  if (layer==1)      return 928;
  else if (layer==2) return 875;
  else if (layer==3) return PedestalLayer3(63-ch);
  else if (layer==4) return PedestalLayer4(   ch);
  std::cout << "\n\nERROR:\tTools GetRefinedPedestal(int layer, int ch):\t"
	    << "don't have info for this layer" << std::endl;
  exit(1);
}
