#define EfficiencyFit_cxx
#include "sTGCTestbeamAnalysis/EfficiencyFit.h"


//using namespace std;

//const int EfficiencyFit::nRuns = 6;
const int EfficiencyFit::nRuns = 5;
//const int EfficiencyFit::runList[] = {321, 323, 325, 326, 334, 335};
const int EfficiencyFit::runList[] = {321, 323, 326, 334, 335};

EfficiencyFit::EfficiencyFit(TString _in, TString _out, TString treename)
  : Plotter(_in, _out, treename)
{
  c->SetGridx();
  c->SetGridy();
  gStyle->SetEndErrorSize(2.);

  double min = 0.;
  double max = 1.;
  TString xtitle = "Run, layer";
  TString ytitle = "Efficiency";
  TString title = "Requiring clusters to be at the peak within 4#sigma";
  TString name = "eff";

  //  Execute(335);

  TGraphErrors* g = new TGraphErrors();

  int point = 0;
  for (int i=0; i<nRuns; i++) {
    int run = runList[i];

    EffFitResult res = Execute(run);

    g->SetPoint     (point, GetRunLayerID(run, 1), res.eff1.value);
    g->SetPointError(point, 0.                   , res.eff1.error);
    point++;

    g->SetPoint     (point, GetRunLayerID(run, 2), res.eff2.value);
    g->SetPointError(point, 0.                   , res.eff2.error);
    point++;

    g->SetPoint     (point, GetRunLayerID(run, 3), res.eff3.value);
    g->SetPointError(point, 0.                   , res.eff3.error);
    point++;

    g->SetPoint     (point, GetRunLayerID(run, 4), res.eff4.value);
    g->SetPointError(point, 0.                   , res.eff4.error);
    point++;    
  }

  g->SetMinimum(min);
  g->SetMaximum(max);
  g->SetMarkerSize(0.5);
  g->Draw("APE");
  g->GetXaxis()->SetTitle(xtitle);
  g->GetYaxis()->SetTitle(ytitle);

  //g->GetXaxis()->SetLabelColor(1, 0.0);
  //  g->GetXaxis()->SetLimits(0., nRuns);
  g->GetXaxis()->SetLimits(-0.5, nRuns+0.5);
  
  
  for (int i=0; i<nRuns; i++) {

    int run = runList[i];
    double ypos = min-0.1*(max-min);
    latex.DrawText(i+0.3, ypos, TString::Format("%i", run));

    ypos = min-0.03*(max-min);
    latex.SetTextSize(0.025);
    for (int layer=1; layer<=4; layer++) {
      double xpos = GetRunLayerID(run, layer)-0.03;
      latex.DrawText(xpos, ypos, TString::Format("%i", layer));
    }
    latex.SetTextSize(0.05);
  }
    
  SetPlotTitle(title);
  SavePlot(name);

  // draw zoom
  min = 0.5;
  for (int i=0; i<nRuns; i++) {

    int run = runList[i];
    double ypos = min-0.1*(max-min);
    latex.DrawText(i+0.3, ypos, TString::Format("%i", run));

    ypos = min-0.03*(max-min);
    latex.SetTextSize(0.025);
    for (int layer=1; layer<=4; layer++) {
      double xpos = GetRunLayerID(run, layer)-0.03;
      latex.DrawText(xpos, ypos, TString::Format("%i", layer));
    }
    latex.SetTextSize(0.05);
  }

  g->SetMinimum(min);
  SavePlot(name+"_zoom");

  delete g;

  c->SetGridx(0);
  c->SetGridy(0);
  gStyle->SetEndErrorSize(0.);  
}

double EfficiencyFit::GetRunLayerID(int run, int layer)
{
  for (int i=0; i<nRuns; i++)
    if (run==runList[i])
      return i + 0.2*layer;

  return -9.;
}

int EfficiencyFit::GetRun(double runLayerID)
{
  return runList[int(runLayerID)];
}

int EfficiencyFit::GetLayer(double runLayerID)
{
  return Nint( (runLayerID-int(runLayerID)) / 0.2 );
}

EffFitResult EfficiencyFit::Execute(int run)
{
  TString nameRun  = TString::Format("run%i", run);
  TString titleRun = TString::Format("Run %i", run);
  
  TString selection = TString::Format("run==%i", run);
  selection += " && TMath::Abs(dy/full2.sigma.value)<4.";

  TH2F* h2d = NULL;
  tree->Draw("layer:event>>h2d(20000,0.5,20000.5,4,0.5,4.5)", selection);
  gDirectory->GetObject("h2d", h2d);

  TH1D h("h", ";Event type (layer without good cluster);Entries",
	 11, 0.5, 11.5);

  for (int evt=1; evt<=h2d->GetNbinsX(); evt++) {
    int l1 = h2d->GetBinContent(evt, 1);
    int l2 = h2d->GetBinContent(evt, 2);
    int l3 = h2d->GetBinContent(evt, 3);
    int l4 = h2d->GetBinContent(evt, 4);

    //     for (int l=1; l<=4; l++)
    //       if (h2d->GetBinContent(evt, l)>1)
    // 	cout << evt << " " << l << "\t";

    if (l1>1 || l2>1 || l3>1 || l4>1) continue;

    h.Fill( GetConfigID(l1, l2, l3, l4) );
  }

  h.Print("all");

  TF1 f("f", FitTools::EffModel, 0.5, 11.5, 5);
  f.SetParName(0 , "Nevt");
  f.SetParName(1 , "eff1");
  f.SetParName(2 , "eff2");
  f.SetParName(3 , "eff3");
  f.SetParName(4 , "eff4");

  f.SetParLimits(0 , 0., 9.e6);
  f.SetParLimits(1 , 0., 1.);
  f.SetParLimits(2 , 0., 1.);
  f.SetParLimits(3 , 0., 1.);
  f.SetParLimits(4 , 0., 1.);

  f.SetParameter(0 , h.GetEntries());
  f.SetParameter(1 , 0.9);
  f.SetParameter(2 , 0.9);
  f.SetParameter(3 , 0.9);
  f.SetParameter(4 , 0.9);

  h.SetMarkerSize(0.5);

  h.GetXaxis()->SetBinLabel( 1, "-");
  h.GetXaxis()->SetBinLabel( 2, "1");
  h.GetXaxis()->SetBinLabel( 3, "2");
  h.GetXaxis()->SetBinLabel( 4, "3");
  h.GetXaxis()->SetBinLabel( 5, "4");
  h.GetXaxis()->SetBinLabel( 6, "1,2");
  h.GetXaxis()->SetBinLabel( 7, "1,3");
  h.GetXaxis()->SetBinLabel( 8, "1,4");
  h.GetXaxis()->SetBinLabel( 9, "2,3");
  h.GetXaxis()->SetBinLabel(10, "2,4");
  h.GetXaxis()->SetBinLabel(11, "3,4");

  h.Draw("E");
  f.SetLineColor(2);
  TFitResultPtr res = h.Fit(&f, "VS");
  h.Draw("same E");
  
  SetPlotTitle(titleRun);
  SavePlot(nameRun+"_efffit");
  
  TMatrixDSym matrix = res->GetCorrelationMatrix();
  TH2D matrixh(matrix);
  matrixh.Scale(100.);
  matrixh.GetXaxis()->SetBinLabel(1, "N_{evt}");
  matrixh.GetXaxis()->SetBinLabel(2, "#varepsilon_{1}");
  matrixh.GetXaxis()->SetBinLabel(3, "#varepsilon_{2}");
  matrixh.GetXaxis()->SetBinLabel(4, "#varepsilon_{3}");
  matrixh.GetXaxis()->SetBinLabel(5, "#varepsilon_{4}");
  matrixh.GetYaxis()->SetBinLabel(1, "N_{evt}");
  matrixh.GetYaxis()->SetBinLabel(2, "#varepsilon_{1}");
  matrixh.GetYaxis()->SetBinLabel(3, "#varepsilon_{2}");
  matrixh.GetYaxis()->SetBinLabel(4, "#varepsilon_{3}");
  matrixh.GetYaxis()->SetBinLabel(5, "#varepsilon_{4}");
  gStyle->SetPaintTextFormat(".1f");
  matrixh.GetZaxis()->SetTitle("Correlation [%]");
  matrixh.Draw("zcol text");
  c->SetRightMargin(0.18);
  SetPlotTitle(titleRun+";Efficiency fit correlation matrix");
  SavePlot(nameRun+"_efffit_correlations");
  c->SetRightMargin(0.05);

  EffFitResult effFitRes;
  effFitRes.Nevt.Set( f.GetParameter(0), f.GetParError(0) );
  effFitRes.eff1.Set( f.GetParameter(1), f.GetParError(1) );
  effFitRes.eff2.Set( f.GetParameter(2), f.GetParError(2) );
  effFitRes.eff3.Set( f.GetParameter(3), f.GetParError(3) );
  effFitRes.eff4.Set( f.GetParameter(4), f.GetParError(4) );
  return effFitRes;
}

int EfficiencyFit::GetConfigID(bool l1, bool l2, bool l3, bool l4)
{
  if ( l1 &&  l2 &&  l3 &&  l4) return  1;
  if (!l1 &&  l2 &&  l3 &&  l4) return  2;
  if ( l1 && !l2 &&  l3 &&  l4) return  3;
  if ( l1 &&  l2 && !l3 &&  l4) return  4;
  if ( l1 &&  l2 &&  l3 && !l4) return  5;
  if (!l1 && !l2 &&  l3 &&  l4) return  6;
  if (!l1 &&  l2 && !l3 &&  l4) return  7;
  if (!l1 &&  l2 &&  l3 && !l4) return  8;
  if ( l1 && !l2 && !l3 &&  l4) return  9;
  if ( l1 && !l2 &&  l3 && !l4) return 10;
  if ( l1 &&  l2 && !l3 && !l4) return 11;
  return -1;
}
