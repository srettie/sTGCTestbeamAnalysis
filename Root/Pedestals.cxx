#define Pedestals_cxx
#include "sTGCTestbeamAnalysis/Pedestals.h"

#include <TMath.h>
#include <TH1.h>
#include <TLine.h>


#include <iostream>

#include "sTGCTestbeamAnalysis/Plotter.h"

//using namespace std;
//using namespace TMath;

Pedestals::Pedestals(int _option)
{
  option = _option;

  perChannelLayer2[0] = TMath::Nint(907.625);
  perChannelLayer2[1] = TMath::Nint(868.688);
  perChannelLayer2[2] = TMath::Nint(880.375);
  perChannelLayer2[3] = TMath::Nint(902.625);
  perChannelLayer2[4] = TMath::Nint(877);
  perChannelLayer2[5] = TMath::Nint(889.5);
  perChannelLayer2[6] = TMath::Nint(889.5);
  perChannelLayer2[7] = TMath::Nint(889.062);
  perChannelLayer2[8] = TMath::Nint(882.875);
  perChannelLayer2[9] = TMath::Nint(908);
  perChannelLayer2[10] = TMath::Nint(862.77);
  perChannelLayer2[11] = TMath::Nint(854.125);
  perChannelLayer2[12] = TMath::Nint(862);
  perChannelLayer2[13] = TMath::Nint(904.062);
  perChannelLayer2[14] = TMath::Nint(871.375);
  perChannelLayer2[15] = TMath::Nint(849.875);
  perChannelLayer2[16] = TMath::Nint(882.312);
  perChannelLayer2[17] = TMath::Nint(879.438);
  perChannelLayer2[18] = TMath::Nint(899.25);
  perChannelLayer2[19] = TMath::Nint(898.062);
  perChannelLayer2[20] = TMath::Nint(864);
  perChannelLayer2[21] = TMath::Nint(879.938);
  perChannelLayer2[22] = TMath::Nint(889.562);
  perChannelLayer2[23] = TMath::Nint(883.75);
  perChannelLayer2[24] = TMath::Nint(874);
  perChannelLayer2[25] = TMath::Nint(870.188);
  perChannelLayer2[26] = TMath::Nint(882.062);
  perChannelLayer2[27] = TMath::Nint(878.375);
  perChannelLayer2[28] = TMath::Nint(890.625);
  perChannelLayer2[29] = TMath::Nint(886.75);
  perChannelLayer2[30] = TMath::Nint(886.25);
  perChannelLayer2[31] = TMath::Nint(871.5);
  perChannelLayer2[32] = TMath::Nint(886.312);
  perChannelLayer2[33] = TMath::Nint(866.688);
  perChannelLayer2[34] = TMath::Nint(878.688);
  perChannelLayer2[35] = TMath::Nint(876.625);
  perChannelLayer2[36] = TMath::Nint(886.312);
  perChannelLayer2[37] = TMath::Nint(885.25);
  perChannelLayer2[38] = TMath::Nint(861.625);
  perChannelLayer2[39] = TMath::Nint(880.312);
  perChannelLayer2[40] = TMath::Nint(893.312);
  perChannelLayer2[41] = TMath::Nint(895.938);
  perChannelLayer2[42] = TMath::Nint(889.625);
  perChannelLayer2[43] = TMath::Nint(854.938);
  perChannelLayer2[44] = TMath::Nint(858.625);
  perChannelLayer2[45] = TMath::Nint(882.812);
  perChannelLayer2[46] = TMath::Nint(893.25);
  perChannelLayer2[47] = TMath::Nint(856.312);
  perChannelLayer2[48] = TMath::Nint(920.125);
  perChannelLayer2[49] = TMath::Nint(842);
  perChannelLayer2[50] = TMath::Nint(842.812);
  perChannelLayer2[51] = TMath::Nint(872.25);
  perChannelLayer2[52] = TMath::Nint(873.75);
  perChannelLayer2[53] = TMath::Nint(854.125);
  perChannelLayer2[54] = TMath::Nint(863.375);
  perChannelLayer2[55] = TMath::Nint(846.062);
  perChannelLayer2[56] = TMath::Nint(869.75);
  perChannelLayer2[57] = TMath::Nint(851.438);
  perChannelLayer2[58] = TMath::Nint(832.312);
  perChannelLayer2[59] = TMath::Nint(859.938);
  perChannelLayer2[60] = TMath::Nint(875.312);
  perChannelLayer2[61] = TMath::Nint(838.667);
  perChannelLayer2[62] = TMath::Nint(844.875);
  perChannelLayer2[63] = TMath::Nint(854.08);
  
  perChannelLayer3[0] = TMath::Nint(870.125);
  perChannelLayer3[1] = TMath::Nint(859.5);
  perChannelLayer3[2] = TMath::Nint(834.733);
  perChannelLayer3[3] = TMath::Nint(867.733);
  perChannelLayer3[4] = TMath::Nint(865.267);
  perChannelLayer3[5] = TMath::Nint(848.938);
  perChannelLayer3[6] = TMath::Nint(853.938);
  perChannelLayer3[7] = TMath::Nint(837);
  perChannelLayer3[8] = TMath::Nint(838.533);
  perChannelLayer3[9] = TMath::Nint(871.688);
  perChannelLayer3[10] = TMath::Nint(844.933);
  perChannelLayer3[11] = TMath::Nint(863.188);
  perChannelLayer3[12] = TMath::Nint(858.533);
  perChannelLayer3[13] = TMath::Nint(857.733);
  perChannelLayer3[14] = TMath::Nint(832.812);
  perChannelLayer3[15] = TMath::Nint(857.333);
  perChannelLayer3[16] = TMath::Nint(841);
  perChannelLayer3[17] = TMath::Nint(852.882);
  perChannelLayer3[18] = TMath::Nint(831.312);
  perChannelLayer3[19] = TMath::Nint(843.429);
  perChannelLayer3[20] = TMath::Nint(865.125);
  perChannelLayer3[21] = TMath::Nint(816.933);
  perChannelLayer3[22] = TMath::Nint(836.25);
  perChannelLayer3[23] = TMath::Nint(830.312);
  perChannelLayer3[24] = TMath::Nint(858.467);
  perChannelLayer3[25] = TMath::Nint(847.812);
  perChannelLayer3[26] = TMath::Nint(859.143);
  perChannelLayer3[27] = TMath::Nint(866.867);
  perChannelLayer3[28] = TMath::Nint(838.562);
  perChannelLayer3[29] = TMath::Nint(846.467);
  perChannelLayer3[30] = TMath::Nint(838.786);
  perChannelLayer3[31] = TMath::Nint(834.625);
  perChannelLayer3[32] = TMath::Nint(841.133);
  perChannelLayer3[33] = TMath::Nint(849.867);
  perChannelLayer3[34] = TMath::Nint(844.75);
  perChannelLayer3[35] = TMath::Nint(843.867);
  perChannelLayer3[36] = TMath::Nint(867.312);
  perChannelLayer3[37] = TMath::Nint(848.25);
  perChannelLayer3[38] = TMath::Nint(865.75);
  perChannelLayer3[39] = TMath::Nint(851.571);
  perChannelLayer3[40] = TMath::Nint(856.933);
  perChannelLayer3[41] = TMath::Nint(864.062);
  perChannelLayer3[42] = TMath::Nint(871);
  perChannelLayer3[43] = TMath::Nint(838.6);
  perChannelLayer3[44] = TMath::Nint(852);
  perChannelLayer3[45] = TMath::Nint(868.5);
  perChannelLayer3[46] = TMath::Nint(874.133);
  perChannelLayer3[47] = TMath::Nint(839.562);
  perChannelLayer3[48] = TMath::Nint(852);
  perChannelLayer3[49] = TMath::Nint(869.2);
  perChannelLayer3[50] = TMath::Nint(846.8);
  perChannelLayer3[51] = TMath::Nint(887.333);
  perChannelLayer3[52] = TMath::Nint(862.8);
  perChannelLayer3[53] = TMath::Nint(859.267);
  perChannelLayer3[54] = TMath::Nint(844.562);
  perChannelLayer3[55] = TMath::Nint(860.867);
  perChannelLayer3[56] = TMath::Nint(866.067);
  perChannelLayer3[57] = TMath::Nint(849.5);
  perChannelLayer3[58] = TMath::Nint(838);
  perChannelLayer3[59] = TMath::Nint(838.312);
  perChannelLayer3[60] = TMath::Nint(846.267);
  perChannelLayer3[61] = TMath::Nint(849.562);
  perChannelLayer3[62] = TMath::Nint(864.538);
  perChannelLayer3[63] = TMath::Nint(829.72);
 
  perChannelLayer3Gain1[0] = TMath::Nint(807.25);
  perChannelLayer3Gain1[1] = TMath::Nint(807.733);
  perChannelLayer3Gain1[2] = TMath::Nint(789.938);
  perChannelLayer3Gain1[3] = TMath::Nint(812.8);
  perChannelLayer3Gain1[4] = TMath::Nint(811.812);
  perChannelLayer3Gain1[5] = TMath::Nint(811.125);
  perChannelLayer3Gain1[6] = TMath::Nint(807.933);
  perChannelLayer3Gain1[7] = TMath::Nint(782.933);
  perChannelLayer3Gain1[8] = TMath::Nint(787.938);
  perChannelLayer3Gain1[9] = TMath::Nint(807.875);
  perChannelLayer3Gain1[10] = TMath::Nint(802.875);
  perChannelLayer3Gain1[11] = TMath::Nint(807.062);
  perChannelLayer3Gain1[12] = TMath::Nint(812.467);
  perChannelLayer3Gain1[13] = TMath::Nint(803.467);
  perChannelLayer3Gain1[14] = TMath::Nint(794.438);
  perChannelLayer3Gain1[15] = TMath::Nint(800.267);
  perChannelLayer3Gain1[16] = TMath::Nint(828.867);
  perChannelLayer3Gain1[17] = TMath::Nint(803.067);
  perChannelLayer3Gain1[18] = TMath::Nint(779.5);
  perChannelLayer3Gain1[19] = TMath::Nint(794.75);
  perChannelLayer3Gain1[20] = TMath::Nint(804.4);
  perChannelLayer3Gain1[21] = TMath::Nint(797);
  perChannelLayer3Gain1[22] = TMath::Nint(794.812);
  perChannelLayer3Gain1[23] = TMath::Nint(783.688);
  perChannelLayer3Gain1[24] = TMath::Nint(800.6);
  perChannelLayer3Gain1[25] = TMath::Nint(798.188);
  perChannelLayer3Gain1[26] = TMath::Nint(812.267);
  perChannelLayer3Gain1[27] = TMath::Nint(815.5);
  perChannelLayer3Gain1[28] = TMath::Nint(789.133);
  perChannelLayer3Gain1[29] = TMath::Nint(796.733);
  perChannelLayer3Gain1[30] = TMath::Nint(802.562);
  perChannelLayer3Gain1[31] = TMath::Nint(789);
  perChannelLayer3Gain1[32] = TMath::Nint(794.867);
  perChannelLayer3Gain1[33] = TMath::Nint(799.267);
  perChannelLayer3Gain1[34] = TMath::Nint(810.688);
  perChannelLayer3Gain1[35] = TMath::Nint(798.8);
  perChannelLayer3Gain1[36] = TMath::Nint(813.875);
  perChannelLayer3Gain1[37] = TMath::Nint(804);
  perChannelLayer3Gain1[38] = TMath::Nint(817.25);
  perChannelLayer3Gain1[39] = TMath::Nint(799.688);
  perChannelLayer3Gain1[40] = TMath::Nint(810.333);
  perChannelLayer3Gain1[41] = TMath::Nint(814.625);
  perChannelLayer3Gain1[42] = TMath::Nint(815.8);
  perChannelLayer3Gain1[43] = TMath::Nint(791.4);
  perChannelLayer3Gain1[44] = TMath::Nint(811.938);
  perChannelLayer3Gain1[45] = TMath::Nint(814.438);
  perChannelLayer3Gain1[46] = TMath::Nint(818.133);
  perChannelLayer3Gain1[47] = TMath::Nint(795.312);
  perChannelLayer3Gain1[48] = TMath::Nint(803.938);
  perChannelLayer3Gain1[49] = TMath::Nint(809.562);
  perChannelLayer3Gain1[50] = TMath::Nint(792.267);
  perChannelLayer3Gain1[51] = TMath::Nint(823.867);
  perChannelLayer3Gain1[52] = TMath::Nint(816.375);
  perChannelLayer3Gain1[53] = TMath::Nint(804.875);
  perChannelLayer3Gain1[54] = TMath::Nint(794.562);
  perChannelLayer3Gain1[55] = TMath::Nint(798.438);
  perChannelLayer3Gain1[56] = TMath::Nint(811.688);
  perChannelLayer3Gain1[57] = TMath::Nint(809.125);
  perChannelLayer3Gain1[58] = TMath::Nint(793.938);
  perChannelLayer3Gain1[59] = TMath::Nint(797.625);
  perChannelLayer3Gain1[60] = TMath::Nint(794.533);
  perChannelLayer3Gain1[61] = TMath::Nint(809.188);
  perChannelLayer3Gain1[62] = TMath::Nint(797.812);
  perChannelLayer3Gain1[63] = TMath::Nint(785.21);

  perChannelLayer4[0] = TMath::Nint(890.25);
  perChannelLayer4[1] = TMath::Nint(878.688);
  perChannelLayer4[2] = TMath::Nint(900.625);
  perChannelLayer4[3] = TMath::Nint(900.4);
  perChannelLayer4[4] = TMath::Nint(881);
  perChannelLayer4[5] = TMath::Nint(898.375);
  perChannelLayer4[6] = TMath::Nint(901.438);
  perChannelLayer4[7] = TMath::Nint(870.533);
  perChannelLayer4[8] = TMath::Nint(919.75);
  perChannelLayer4[9] = TMath::Nint(889.188);
  perChannelLayer4[10] = TMath::Nint(901.067);
  perChannelLayer4[11] = TMath::Nint(890.214);
  perChannelLayer4[12] = TMath::Nint(911.4);
  perChannelLayer4[13] = TMath::Nint(894.938);
  perChannelLayer4[14] = TMath::Nint(876.75);
  perChannelLayer4[15] = TMath::Nint(880.062);
  perChannelLayer4[16] = TMath::Nint(884.375);
  perChannelLayer4[17] = TMath::Nint(885.125);
  perChannelLayer4[18] = TMath::Nint(900.938);
  perChannelLayer4[19] = TMath::Nint(898.25);
  perChannelLayer4[20] = TMath::Nint(909.938);
  perChannelLayer4[21] = TMath::Nint(889.867);
  perChannelLayer4[22] = TMath::Nint(894.133);
  perChannelLayer4[23] = TMath::Nint(901.062);
  perChannelLayer4[24] = TMath::Nint(896.125);
  perChannelLayer4[25] = TMath::Nint(876.188);
  perChannelLayer4[26] = TMath::Nint(913.4);
  perChannelLayer4[27] = TMath::Nint(906.75);
  perChannelLayer4[28] = TMath::Nint(906.562);
  perChannelLayer4[29] = TMath::Nint(889.733);
  perChannelLayer4[30] = TMath::Nint(897.938);
  perChannelLayer4[31] = TMath::Nint(898.667);
  perChannelLayer4[32] = TMath::Nint(885);
  perChannelLayer4[33] = TMath::Nint(906.8);
  perChannelLayer4[34] = TMath::Nint(893.333);
  perChannelLayer4[35] = TMath::Nint(889.75);
  perChannelLayer4[36] = TMath::Nint(883.188);
  perChannelLayer4[37] = TMath::Nint(879.812);
  perChannelLayer4[38] = TMath::Nint(893.188);
  perChannelLayer4[39] = TMath::Nint(900.625);
  perChannelLayer4[40] = TMath::Nint(881.25);
  perChannelLayer4[41] = TMath::Nint(896.75);
  perChannelLayer4[42] = TMath::Nint(875.333);
  perChannelLayer4[43] = TMath::Nint(905.062);
  perChannelLayer4[44] = TMath::Nint(897.267);
  perChannelLayer4[45] = TMath::Nint(885.562);
  perChannelLayer4[46] = TMath::Nint(880.688);
  perChannelLayer4[47] = TMath::Nint(880.8);
  perChannelLayer4[48] = TMath::Nint(883.6);
  perChannelLayer4[49] = TMath::Nint(900.062);
  perChannelLayer4[50] = TMath::Nint(893.875);
  perChannelLayer4[51] = TMath::Nint(903.867);
  perChannelLayer4[52] = TMath::Nint(900.5);
  perChannelLayer4[53] = TMath::Nint(884.133);
  perChannelLayer4[54] = TMath::Nint(893.875);
  perChannelLayer4[55] = TMath::Nint(878.467);
  perChannelLayer4[56] = TMath::Nint(926.5);
  perChannelLayer4[57] = TMath::Nint(904.812);
  perChannelLayer4[58] = TMath::Nint(901.312);
  perChannelLayer4[59] = TMath::Nint(916.625);
  perChannelLayer4[60] = TMath::Nint(904.467);
  perChannelLayer4[61] = TMath::Nint(897.75);
  perChannelLayer4[62] = TMath::Nint(892.222);
  perChannelLayer4[63] = TMath::Nint(894.17);

  fittedLayer4[0] = TMath::Nint(890.25);
  fittedLayer4[1] = TMath::Nint(878.688);
  fittedLayer4[2] = TMath::Nint(900.625);
  fittedLayer4[3] = TMath::Nint(900.4);
  fittedLayer4[4] = TMath::Nint(881);
  fittedLayer4[5] = TMath::Nint(898.375);
  fittedLayer4[6] = TMath::Nint(901.438);
  fittedLayer4[7] = TMath::Nint(870.533);
  fittedLayer4[8] = TMath::Nint(919.75);
  fittedLayer4[9] = TMath::Nint(889.188);
  fittedLayer4[10] = TMath::Nint(901.067);
  fittedLayer4[11] = TMath::Nint(890.214);
  fittedLayer4[12] = TMath::Nint(911.4);
  fittedLayer4[13] = TMath::Nint(894.938);
  fittedLayer4[14] = TMath::Nint(876.75);
  fittedLayer4[15] = TMath::Nint(880.062);
  fittedLayer4[16] = TMath::Nint(884.375);
  fittedLayer4[17] = TMath::Nint(885.125);
  fittedLayer4[18] = TMath::Nint(900.938);
  fittedLayer4[19] = TMath::Nint(898.25);
  fittedLayer4[20] = TMath::Nint(909.938);
  fittedLayer4[21] = TMath::Nint(889.867);
  fittedLayer4[22] = TMath::Nint(894.133);
  fittedLayer4[23] = TMath::Nint(901.062);
  fittedLayer4[24] = TMath::Nint(896.125);
  fittedLayer4[25] = TMath::Nint(876.188);
  fittedLayer4[26] = TMath::Nint(913.4);
  fittedLayer4[27] = TMath::Nint(906.75);
  fittedLayer4[28] = TMath::Nint(906.562);
  fittedLayer4[29] = TMath::Nint(889.733);
  fittedLayer4[30] = TMath::Nint(897.938);
  fittedLayer4[31] = TMath::Nint(898.667);
  fittedLayer4[32] = TMath::Nint(885);
  fittedLayer4[33] = TMath::Nint(906.8);
  fittedLayer4[34] = TMath::Nint(893.333);
  fittedLayer4[35] = TMath::Nint(889.75);
  fittedLayer4[36] = TMath::Nint(883.188);
  fittedLayer4[37] = TMath::Nint(879.812);
  fittedLayer4[38] = TMath::Nint(893.188);
  fittedLayer4[39] = TMath::Nint(900.625);
  fittedLayer4[40] = TMath::Nint(881.25);
  fittedLayer4[41] = TMath::Nint(896.75);
  fittedLayer4[42] = TMath::Nint(875.333);
  fittedLayer4[43] = TMath::Nint(905.062);
  fittedLayer4[44] = TMath::Nint(897.267);
  fittedLayer4[45] = TMath::Nint(885.562);
  fittedLayer4[46] = TMath::Nint(880.688);
  fittedLayer4[47] = TMath::Nint(880.8);
  fittedLayer4[48] = TMath::Nint(883.6);
  fittedLayer4[49] = TMath::Nint(900.062);
  fittedLayer4[50] = TMath::Nint(893.875);
  fittedLayer4[51] = TMath::Nint(903.867);

  fittedLayer4[52] = TMath::Nint(934.055);
  fittedLayer4[53] = TMath::Nint(958.5  );
  fittedLayer4[54] = TMath::Nint(969.865);
  fittedLayer4[55] = TMath::Nint(975.493);
  fittedLayer4[56] = TMath::Nint(993.705);
  fittedLayer4[57] = TMath::Nint(969.431);
  fittedLayer4[58] = TMath::Nint(935.642);
  fittedLayer4[59] = TMath::Nint(943.028);
  fittedLayer4[60] = TMath::Nint(926.45 );
  fittedLayer4[61] = TMath::Nint(916.434);

  //   fittedLayer4[52] = TMath::Nint(900.5);
  //   fittedLayer4[53] = TMath::Nint(884.133);
  //   fittedLayer4[54] = TMath::Nint(893.875);
  //   fittedLayer4[55] = TMath::Nint(878.467);
  //   fittedLayer4[56] = TMath::Nint(926.5);
  //   fittedLayer4[57] = TMath::Nint(904.812);
  //   fittedLayer4[58] = TMath::Nint(901.312);
  //   fittedLayer4[59] = TMath::Nint(916.625);
  //   fittedLayer4[60] = TMath::Nint(904.467);
  //   fittedLayer4[61] = TMath::Nint(897.75);

  fittedLayer4[62] = TMath::Nint(892.222);
  fittedLayer4[63] = TMath::Nint(894.17);
  
  globalLayer1      = 928;
  globalLayer2      = TMath::Mean(64, perChannelLayer2);
  globalLayer3      = TMath::Mean(64, perChannelLayer3);
  globalLayer3Gain1 = TMath::Mean(64, perChannelLayer3Gain1);
  globalLayer4      = TMath::Mean(64, perChannelLayer4);

  oldGlobalLayer1 = 928; // L1S3 - 25		VMM = 100	Jack = 12
  oldGlobalLayer2 = 875; // L2S3 - 18		VMM = 107	Jack = 11
  oldGlobalLayer3 = 894; // L3S3 - 29		VMM =   4	Jack = 13
  oldGlobalLayer4 = 865; // L4S3 - 19		VMM =   7	Jack =  7
}

int Pedestals::Get(int layer, int ch)
{
  if      (option==oldGlobal)  return GetOldGlobal(layer);
  else if (option==global)     return GetGlobal(layer);
  else if (option==perChannel) {
    if (layer==1) return GetOldGlobal(layer);
    else          return GetPerChannel(layer, ch);
  }
  else if (option==fitted) {
    if (layer==1)      return GetOldGlobal (layer);
    else if (layer==4) return GetFitted    (layer, ch);
    else               return GetPerChannel(layer, ch);
  }
  else if (option>0)           return option;
  else {
    std::cout << "\n\nERROR:\tPedestals::Get(int layer, int ch):\t"
	 << "Not known action for option " << option << std::endl;
    exit(1);    
  }
}

int Pedestals::GetPerChannel(int layer, int ch)
{
  if (ch<0 || ch>63) {
    std::cout << "\n\nERROR:\tPedestals::GetPerChannel(int layer, int ch):\t"
	 << "the channel number has to be in [0, 63]" << std::endl;
    exit(1);
  }

  if (TMath::Abs(layer)==2 || TMath::Abs(layer)==3) ch = 63-ch;

  if      (layer== 2) return perChannelLayer2     [ch];
  else if (layer== 3) return perChannelLayer3     [ch];
  else if (layer==-3) return perChannelLayer3Gain1[ch];
  else if (layer== 4) return perChannelLayer4     [ch];
  std::cout << "\n\nERROR:\tPedestals::GetPerChannel(int layer, int ch):\t"
       << "there is no info for layer " << layer << std::endl;
  exit(1);
}
int Pedestals::GetFitted(int layer, int ch)
{
  if (ch<0 || ch>63) {
    std::cout << "\n\nERROR:\tPedestals::GetPerChannel(int layer, int ch):\t"
	 << "the channel number has to be in [0, 63]" << std::endl;
    exit(1);
  }

  if (layer== 4) return fittedLayer4[ch];
  std::cout << "\n\nERROR:\tPedestals::GetPerChannel(int layer, int ch):\t"
       << "there is no info for layer " << layer << std::endl;
  exit(1);
}

int Pedestals::GetLayer3Gain1PerChannel(int ch)
{
  if (ch<0 || ch>63) {
    std::cout << "\n\nERROR:\tPedestals::GetPerChannel(int layer, int ch):\t"
	 << "the channel number has to be in [0, 63]" << std::endl;
    exit(1);
  }

  ch = 63-ch;
  return perChannelLayer3Gain1[ch];
}

int Pedestals::GetGlobal(int layer)
{
  if       (layer==1) return globalLayer1;
  else if (layer== 2) return globalLayer2;
  else if (layer== 3) return globalLayer3;
  else if (layer==-3) return globalLayer3Gain1;
  else if (layer== 4) return globalLayer4;
  std::cout << "\n\nERROR:\tPedestals::GetGlobal(int layer):\t"
       << "there is no info for layer " << layer << std::endl;
  exit(1);
}

int Pedestals::GetOldGlobal(int layer)
{
  if (layer==1)      return oldGlobalLayer1;
  else if (layer==2) return oldGlobalLayer2;
  else if (layer==3) return oldGlobalLayer3;
  else if (layer==4) return oldGlobalLayer4;
  std::cout << "\n\nERROR:\tPedestals::GetGlobal(int layer):\t"
       << "there is no info for layer " << layer << std::endl;
  exit(1);
}

void Pedestals::Plot(TString out)
{
  Plotter plotter(out);
  plotter.c->SetGridx();
  plotter.c->SetGridy();

  TLine redLine;
  redLine.SetLineColor(2);
  redLine.SetLineWidth(3);
  redLine.SetLineStyle(1);
  TLine blueLine;
  blueLine.SetLineColor(4);
  blueLine.SetLineWidth(3);
  blueLine.SetLineStyle(2);
  
  TString vmmName[] = {"VMM1 R100",
		       "VMM1 R107",
		       "VMM1 R4",
		       "VMM1 R7"};

  vector<int> layers;
  layers.push_back(2);
  layers.push_back(3);
  layers.push_back(-3);
  layers.push_back(4);

  for (int i=0; i<(int)layers.size(); i++) {

    int layer = layers[i];

    int global    = GetGlobal(layer);
    int oldGlobal = GetOldGlobal(TMath::Abs(layer));
    TH1F h("h", "", 64, -0.5, 63.5);
    for (int ch=0; ch<64; ch++) h.Fill(ch, GetPerChannel(layer, ch));
    
    double width = 1.1*TMath::Max(double(TMath::Abs(oldGlobal-global)),
				  double(TMath::Max(TMath::Abs(h.GetMinimum()-global),
				      TMath::Abs(h.GetMaximum()-global))) );
    //    if (layer!=-3) {
    h.SetMinimum(global-width);
    h.SetMaximum(global+width);
    //    }
    
    TString name = TString::Format("layer%i", TMath::Abs(layer));
    TString title = TString::Format("Layer %i (%s, gain = 3)",
				    layer, vmmName[TMath::Abs(layer)-1].Data());
    title += ";Color lines: #color[4]{Pedestal average} and #color[2]{old global pedestal}";

    if (layer==-3) {
      h.SetMinimum(-1111);
      h.SetMaximum(-1111);
      
      name = "layer3gain1";
      title = "Layer 3 (VMM1 R4, gain = 1)";
      title += ";Color line: #color[4]{Pedestal average}";
    }
    
    h.GetXaxis()->SetTitle("Channel");
    h.GetYaxis()->SetTitle("Pedestal [ADC counts]");
    
    h.Draw("hist");
    plotter.SetPlotTitle(title);
    redLine .DrawLine(-0.5, oldGlobal, 63.5, oldGlobal);
    blueLine.DrawLine(-0.5, global   , 63.5, global   );
    plotter.SavePlot(name);
  }


  TH1F hl3g13diff("hl3g13diff", "", 64, -0.5, 63.5);
  for (int ch=0; ch<64; ch++) hl3g13diff.Fill(ch,
					      GetPerChannel(3, ch) -
					      GetLayer3Gain1PerChannel(ch));
  
  hl3g13diff.GetXaxis()->SetTitle("Channel");
  hl3g13diff.GetYaxis()->SetTitle("P(gain=3) - P(g=1) [ADC counts]");    
  hl3g13diff.Draw("hist");
  plotter.SetPlotTitle("Layer 3 (VMM1 R4)");
  plotter.SavePlot("layer3_gainDiff");
  
}
