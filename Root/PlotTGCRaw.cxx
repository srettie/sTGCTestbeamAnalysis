#define PlotTGCRaw_cxx
#include "sTGCTestbeamAnalysis/PlotTGCRaw.h"

//using namespace std;
using namespace Mapping;

PlotTGCRaw::PlotTGCRaw(TString _in, TString _out, TString treename, int run)
  : Plotter(_in, _out, treename)
{
  TString runtitle = TString::Format("Run %i", run);
  TString list("");

  //   TString layermap = "";
  //   layermap += TString::Format("(1*(layerip==%i) ", GetLayerIP(run, 1));
  //   layermap += TString::Format("+2*(layerip==%i) ", GetLayerIP(run, 2));
  //   layermap += TString::Format("+3*(layerip==%i) ", GetLayerIP(run, 3));
  //   layermap += TString::Format("+4*(layerip==%i))", GetLayerIP(run, 4));
  //   Plot(layermap, "", "layer", runtitle, "Layer IP", "Entries",
  //        "4, 0.5, 4.5", false, 0.);


  //TString layercmd = TString::Format("Mapping::GetLayerNumber(%i, layerip)",
  //				     run);
  
  //Plot(layercmd, "(layerip==18 || layerip==19 || layerip==25 || layerip==29)",
  //     "layer", runtitle, "Layer", "Entries",
  //     "4, 0.5, 4.5", false, 0.);
  
  list += "layer";
  
  for (int layer=1; layer<=4; layer++) {

    TString layerlist("");

    int ip = Mapping::GetLayerIP(run, layer);
    TString lnm = Mapping::GetLayerName(layer);
    TString sel = TString::Format("layerip==%i", ip);
    TString title = runtitle;
    title += ", "+lnm;
    TString name = lnm+"_all";

    Plot("channel", sel, "ch_"+name , title, "Channel", "Entries",
	 "64, -0.5, 63.5", true);
    Plot("tdo"    , sel, "tdo_"+name, title, "TDO"    , "Entries", "", true);
    Plot("pdo"    , sel, "pdo_"+name, title, "PDO"    , "Entries", "", true);

    Plot2D("tdo:pdo", sel, "tdoVsPdo_"+name, title, "PDO", "TDO", "");
    
    MergePdfs("ch_"+name+";"+
	      "pdo_"+name+";"+
	      "tdo_"+name+";"+
	      "tdoVsPdo_"+name,
	      name, "2x2");
    //     MergePdfs("ch_"+name+";"+
    // 	      "ch_"+name+"_log"+";"+
    // 	      "pdo_"+name+";"+
    // 	      "tdo_"+name,
    // 	      name, "2x2");
    layerlist += name;
    //    layerlist += ";tdoVsPdo_"+name;



    // apply pre-selection
    // tdo>2300 && tdo<3300
    // AND if layer==1 pdo<5500
    sel += " && tdo>2300 && tdo<3300";
    title += ";2300 < TDO < 3300";
    if (layer==1) {
      sel += " && pdo<5500";
      title += " & PDO < 5500";
    }
    name = lnm+"_presel";

    Plot("channel", sel, "ch_"+name , title, "Channel", "Entries",
	   "64, -0.5, 63.5", true);
    Plot("tdo"    , sel, "tdo_"+name, title, "TDO"    , "Entries", "", true);
    Plot("pdo"    , sel, "pdo_"+name, title, "PDO"    , "Entries", "", true);

    Plot2D("tdo:pdo", sel, "tdoVsPdo_"+name, title, "PDO", "TDO", "");

    MergePdfs("ch_"+name+";"+
	      "pdo_"+name+";"+
	      "tdo_"+name+";"+
	      "tdoVsPdo_"+name,
	      name, "2x2");
    //     MergePdfs("ch_"+name+";"+
    // 	      "ch_"+name+"_log"+";"+
    // 	      "pdo_"+name+";"+
    // 	      "tdo_"+name,
    // 	      name, "2x2");
    layerlist += ";"+name;
    //    layerlist += ";tdoVsPdo_"+name;



    MergePdfs(layerlist, lnm);
    list += ";"+lnm;
  }

  SaveHistos();
  MergePdfs(list);
}
