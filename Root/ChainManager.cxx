#define ChainManager_cxx
#include "sTGCTestbeamAnalysis/ChainManager.h"



using namespace std;


//#if !defined(__CINT__) || defined(__MAKECINT__)
//#pragma link C++ class std::vector<RunInput*>;
//#pragma link C++ class std::vector<std::vector<int> >+;
//#pragma link C++ class std::vector<std::vector<float> >+;
//#endif

ChainManager::ChainManager(TString name, vector<RunInput*> runList, Options opt,
	   	  TString descriptorTGCRawTxt, TString descriptorPixelRawTxt,
		  vector<int>& layerList, vector<int>& pedestalList)
{
  
  TString dirOutput = "output/"+name;

  TString nameTGCRawTxt      = "tgc_raw_txt";
  TString nameTGCRaw         = "tgc_raw";  
  TString nameTGCPedestals   = "tgc_pedestals";
  TString nameTGCCluster     = "tgc_cluster";
  TString nameTGCPedestalFit = "tgc_pedestalfit";
  TString namePixelRawTxt    = "pixel_raw_txt";
  TString namePixelRaw       = "pixel_raw";
  TString namePixelTrack     = "pixel_trk";
  TString nameCombEvent      = "comb_evt";
  TString nameResidualFit    = "fitresult";
  TString nameFinalData      = "finaldata";
  
  TString dirTGCRawTxt      = dirOutput+"/"+nameTGCRawTxt;
  TString dirTGCRaw         = dirOutput+"/"+nameTGCRaw;
  TString dirTGCPedestals   = dirOutput+"/"+nameTGCPedestals;
  TString dirTGCCluster     = dirOutput+"/"+nameTGCCluster;
  TString dirTGCPedestalFit = dirOutput+"/"+nameTGCPedestalFit;
  TString dirPixelRawTxt    = dirOutput+"/"+namePixelRawTxt;
  TString dirPixelRaw       = dirOutput+"/"+namePixelRaw;
  TString dirPixelTrack     = dirOutput+"/"+namePixelTrack;
  TString dirCombEvent      = dirOutput+"/"+nameCombEvent;
  TString dirResidualFit    = dirOutput+"/"+nameResidualFit;
  TString dirFinalData      = dirOutput+"/"+nameFinalData;
  
  TString formTGCRawTxt      = dirTGCRawTxt      +"/"+nameTGCRawTxt  +"_%s.txt";
  TString formTGCRaw         = dirTGCRaw         +"/"+nameTGCRaw     +"_%s.root";
  TString formTGCRawFig      = dirTGCRaw         +"/"+nameTGCRaw     +"_%s_fig";
  TString formTGCCluster     = dirTGCCluster     +"/"+nameTGCCluster +"_%s.root";
  TString formTGCClusterFig  = dirTGCCluster     +"/"+nameTGCCluster +"_%s_fig";
  TString formTGCPedestalFit = dirTGCPedestalFit +"/"+nameTGCPedestalFit +"_%s.root";
  TString formPixelRawTxt    = dirPixelRawTxt    +"/"+namePixelRawTxt+"_%s.txt";
  TString formPixelRaw       = dirPixelRaw       +"/"+namePixelRaw   +"_%s.root";
  TString formPixelTrack     = dirPixelTrack     +"/"+namePixelTrack +"_%s.root";
  TString formCombEvent      = dirCombEvent      +"/"+nameCombEvent  +"_%s.root";
  TString formCombEventFig   = dirCombEvent      +"/"+nameCombEvent  +"_%s_fig";
  TString formResidualFit    = dirResidualFit    +"/"+nameResidualFit+"_%s";

  TString pathFinalData      = dirFinalData      +"/"+nameFinalData  +".root";
  TString pathFinalDataFig   = dirFinalData      +"/"+nameFinalData  +"_fig";



  if (opt.doPlotTGCPedestals) {
    system("mkdir -p "+dirTGCPedestals);
    Pedestals pedestals(Pedestals::perChannel);
    pedestals.Plot(dirTGCPedestals);
  }
  

  //
  // loop over runs
  //
  
  for (int r=0; r<(int)runList.size(); r++) {
    
    RunInput& run = *(runList[r]);
    std::cout << "\n\n\tProcessing run " << run.num << "\n\n\n";

    TString labelRun;
    labelRun.Form("run%i", run.num);

    TString pathTGCRawTxt;
    TString pathTGCRaw;
    TString pathTGCRawFig;
    TString pathPixelRawTxt;
    TString pathPixelRaw;
    TString pathPixelTrack;
    pathTGCRawTxt  .Form(formTGCRawTxt  , labelRun.Data());
    pathTGCRaw     .Form(formTGCRaw     , labelRun.Data());
    pathTGCRawFig  .Form(formTGCRawFig  , labelRun.Data());    
    pathPixelRawTxt.Form(formPixelRawTxt, labelRun.Data());
    pathPixelRaw   .Form(formPixelRaw   , labelRun.Data());
    pathPixelTrack .Form(formPixelTrack , labelRun.Data());
    
    
    //
    // TGC chain
    //

    if (opt.doMakeTGCRaw) {
      system("mkdir -p "+dirTGCRawTxt);
      system("cp "+run.fileTGCRawTxt+" "+pathTGCRawTxt);
      
      system("mkdir -p "+dirTGCRaw);
      TxtToRoot(pathTGCRawTxt, pathTGCRaw, nameTGCRaw, descriptorTGCRawTxt);
    }
    
    if (opt.doPlotTGCRaw)
      PlotTGCRaw plot(pathTGCRaw, pathTGCRawFig, nameTGCRaw, run.num);
    
    
    //
    // pixel chain
    //

    if (opt.doMakePixelRaw) {
      system("mkdir -p "+dirPixelRawTxt);
      system("cp "+run.filePixelRawTxt+" "+pathPixelRawTxt);
      
      system("mkdir -p "+dirPixelRaw);
      TxtToRoot(pathPixelRawTxt, pathPixelRaw, namePixelRaw, 
		descriptorPixelRawTxt);
    }

    if (opt.doMakePixelTrack) {
      system("mkdir -p "+dirPixelTrack);
      MakePixelTrack(pathPixelRaw, pathPixelTrack);
    }


    //
    // loop over pedestals
    //
    
    for (int p=0; p<(int)pedestalList.size(); p++) {
      
      int pedestal = pedestalList[p];
      
      TString labelTGCCluster = labelRun;
      if (pedestal!=0) labelTGCCluster.Form("run%i_p%i", run.num, pedestal);
      
      TString pathTGCCluster;
      TString pathTGCClusterFig;
      pathTGCCluster   .Form(formTGCCluster,    labelTGCCluster.Data());
      pathTGCClusterFig.Form(formTGCClusterFig, labelTGCCluster.Data());
      
      if (opt.doMakeTGCCluster) {
	system("mkdir -p "+dirTGCCluster);
	MakeTGCCluster(pathTGCRaw, pathTGCCluster, run.num, pedestal);
      }
      
      if (opt.doPlotTGCCluster)
	PlotTGCCluster plot(pathTGCCluster, pathTGCClusterFig, nameTGCCluster,
			    run.num);
      
      
      //
      // layer loop
      //
      
      if (opt.doTGCPedestalFit || opt.doMakeCombEvent || opt.doPlotCombEvent ||
	  opt.doResidualFit ) {
	
	//	for (int layer=1; layer<5; layer++) {
	for (int l=0; l<(int)layerList.size(); l++) {
	  
	  int layer = layerList[l];

	  std::cout << "\n\n\t\tProcessing layer " << layer << "\n\n\n";

	  TString labelRunLayer;
	  labelRunLayer.Form("run%i_layer%i", run.num, layer);
	  if (pedestal!=0) labelRunLayer.Form("run%i_layer%i_p%i",
					      run.num, layer, pedestal);

	  TString pathTGCPedestalFit;
	  TString pathCombEvent;
	  TString pathCombEventFig;
	  TString pathResidualFit;
	  pathTGCPedestalFit.Form(formTGCPedestalFit, labelRunLayer.Data());
	  pathCombEvent     .Form(formCombEvent     , labelRunLayer.Data());
	  pathCombEventFig  .Form(formCombEventFig  , labelRunLayer.Data());
	  pathResidualFit   .Form(formResidualFit   , labelRunLayer.Data());

	  //
	  // TGC pedestal fit
	  //
	  
	  if (opt.doTGCPedestalFit) {
	    system("mkdir -p "+dirTGCPedestalFit);
	    TGCPedestalFit(pathTGCCluster, pathTGCPedestalFit,
			   run.num, layer);
	  }
	  
	  //
	  // TGC - pixel combined events
	  //
	  
	  if (opt.doMakeCombEvent) {
	    system("mkdir -p "+dirCombEvent);
	    MakeCombEvent(pathPixelTrack, pathTGCCluster, pathCombEvent,
			  run.num, layer);
	  }

	  if (opt.doPlotCombEvent)
	    PlotCombEvent plot(pathCombEvent, pathCombEventFig, nameCombEvent,
			       run.num, layer);

	  
	  
	  //
	  // residual fits
	  //
	  
	  if (opt.doResidualFit) {
	    system("mkdir -p "+dirResidualFit);
	    std::cout<<"About to execute RunResidualFit"<<std::endl;
	    RunResidualFit(pathCombEvent, pathResidualFit,
			   run.num, layer, pedestal);
	    std::cout<<"Done"<<std::endl;
	  }
	  
	} // end layer loop
      } // end if (need layer loop)
    } // end pedestal loop
  } // end run loop

  if (opt.doMergeFinalData) {
    system("mkdir -p "+dirFinalData);
    TString m_paths = (dirResidualFit+
		       //    "/fitresult_run???_layer?/all_finaldata.root");
		       // "/fitresult_run???_layer?_p-2/all_finaldata.root");
		       "/fitresult_run???_layer?_p-2/chi210_finaldata.root");
    system("hadd -f "+pathFinalData+" "+m_paths);
  }
  
  if (opt.doPlotFinalData)
    PlotFinalData(pathFinalData, pathFinalDataFig, nameFinalData);
  
  //  EfficiencyFit(pathFinalData, pathFinalDataFig, nameFinalData);  
  
  vector<TString> fitList = Tools::StrVec("chi210");
  
  if (opt.doMergeFitResult) {
    for (int f=0; f<(int)fitList.size(); f++) {
      
      TString fit = fitList[f];
      TString m_paths = (dirResidualFit+"/"+
			 nameResidualFit+"_run???_layer?*/"+
			 fit+"_"+nameResidualFit+".root");
      system("hadd -f "+dirResidualFit+"/"+
	     fit+"_"+nameResidualFit+".root "+
	     m_paths);
    }
  }

  if (opt.doPlotFitResult) {
    for (int f=0; f<(int)fitList.size(); f++) {
      TString fit = fitList[f];
      std::cout<<"using files"<<std::endl;
      std::cout<<"_in is "<<dirResidualFit+"/"+fit+"_"+nameResidualFit+".root" <<std::endl;
      std::cout<<"_out is "<<dirResidualFit+"/fig" <<std::endl;
      PlotFitResult plot(dirResidualFit+"/"+
			 fit+"_"+nameResidualFit+".root",
			 dirResidualFit+"/fig");
    }
  }
  
}
ChainManager::~ChainManager()
{
}
