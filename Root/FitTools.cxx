#define FitTools_cxx
#include "sTGCTestbeamAnalysis/FitTools.h"


RooDataSet* FitTools::SelectData(RooDataSet& fulldata,
		       RooRealVar& var, double min, double max) {
  TString cut = TString::Format("%s>%f && %s<%f",
				var.GetName(), min, var.GetName(), max);
  return (RooDataSet*)fulldata.reduce(cut);
}

GausFitRes FitTools::GausFit(TString name, TString title, RooRealVar& var, double min, double max, RooDataSet& fulldata, Plotter* plotter, int plotNBins, double plotmin, double plotmax, bool quoteMean) {
  bool fit3SigmaRange = false;
  //    bool useGaussBkg = false;
    bool useGaussBkg = true;

    RooDataSet* data = SelectData(fulldata, var, min, max);

    var.setMin(min);
    var.setMax(max);
    
    GausFitRes out;
    if (data->numEntries()<10) {
      delete data;
      return out;
    }
    

    
    // model
    
    double rawmean = data->mean(var); 
    RooRealVar mean("mean", "", rawmean, min, max);
    RooRealVar sigma("sigma", "", 100., 10., 500.);
    RooGaussian gauss("gauss","", var, mean, sigma);

    RooRealVar sigmaBkg("sigmaBkg", "", 300., 100., 3000.);
    
    RooAbsPdf* bkg = (RooAbsPdf*)new RooUniform("bkg", "", var);
    if (useGaussBkg) {
      delete bkg;
      bkg = (RooAbsPdf*)new RooGaussian("bkg","", var, mean, sigmaBkg);      
    }

    RooRealVar sigfrac("sigfrac", "", 0.8, 0.6, 1.);
    RooAddPdf pdf("pdf", "", RooArgList(gauss, *bkg), sigfrac);
    
    
    // fit
    
    mean   .setConstant(false);
    sigma  .setConstant(false);
    sigfrac.setConstant(false);

    sigmaBkg.setConstant(false);

    var.setRange("fitrange", min, max);
    RooFitResult* result = pdf.fitTo(*data, Save(), Range("fitrange"),
    				     Minimizer("Minuit2", "minimize"));
    if (fit3SigmaRange) {
      for (int n=0; n<2; n++) {
	cout << "\t Gaussian fit result, for: " << title
	     << ",\t range: " << min << " , " << max << endl;
	result->Print();

	var.removeMin();
	var.removeMax();
	delete result;
	delete data;

	min = mean.getVal()-3.*sigma.getVal();
	max = mean.getVal()+3.*sigma.getVal();
	data = SelectData(fulldata, var, min, max);

	var.setMin(min);
	var.setMax(max);

	var.setRange("fitrange", min, max);
	result = pdf.fitTo(*data, Save(), Range("fitrange"),
			   Minimizer("Minuit2", "minimize"));      
      }
    }
    
    
    // result
  
    cout << "\n\n\t Gaussian fit result, for: " << title
	 << "\n\t Fit range: " << min << " , " << max << endl;
    result->Print();
    cout << "\n\t Correlation matrix: " << endl;
    result->correlationMatrix().Print();
  
    out.mean   .value = mean   .getVal();
    out.mean   .error = mean   .getError();

    out.sigma  .value = sigma  .getVal();
    out.sigma  .error = sigma  .getError();

    out.sigfrac.value = sigfrac.getVal();
    out.sigfrac.error = sigfrac.getError();

    out.nevts = data->numEntries();
    out.status = result->status();
    out.covqual = result->covQual();

    // plot
    
    TString sigma_str = Tools::Print(sigma.getVal(), sigma.getError());
    TString mean_str = Tools::Print(mean.getVal(), mean.getError());
    title += TString::Format(";#sigma = %s #mum", sigma_str.Data() );
    if (quoteMean)
      title += TString::Format(", #mu = %s #mum", mean_str.Data() );

    if (plotmin==plotmax) {
      plotmin = min;
      plotmax = max;
    }

    TString pdfNormRange = "pdfNormRange";
    var.setRange(pdfNormRange, min, max);
    
    plotter->PlotFit(name, title, var, plotNBins, plotmin, plotmax,
		     fulldata, pdf, bkg, pdfNormRange);
    
    var.removeMin();
    var.removeMax();

    delete result;
    delete bkg;
    delete data;

    return out;
}


double FitTools::EffModel(double* x, double* par)
{
  int id = TMath::Nint(x[0]);

  double n    = par[0];
  double eff1 = par[1];
  double eff2 = par[2];
  double eff3 = par[3];
  double eff4 = par[4];
  
  double rej1 = 1.-eff1;
  double rej2 = 1.-eff2;
  double rej3 = 1.-eff3;
  double rej4 = 1.-eff4;

  if (id== 1) return n * eff1 * eff2 * eff3 * eff4;
  if (id== 2) return n * rej1 * eff2 * eff3 * eff4;
  if (id== 3) return n * eff1 * rej2 * eff3 * eff4;
  if (id== 4) return n * eff1 * eff2 * rej3 * eff4;
  if (id== 5) return n * eff1 * eff2 * eff3 * rej4;
  if (id== 6) return n * rej1 * rej2 * eff3 * eff4;
  if (id== 7) return n * rej1 * eff2 * rej3 * eff4;
  if (id== 8) return n * rej1 * eff2 * eff3 * rej4;
  if (id== 9) return n * eff1 * rej2 * rej3 * eff4;
  if (id==10) return n * eff1 * rej2 * eff3 * rej4;
  if (id==11) return n * eff1 * eff2 * rej3 * rej4;
  
  cout << "STRANGE!!" << endl;
  //  exit(1);
  return 0.;
}
