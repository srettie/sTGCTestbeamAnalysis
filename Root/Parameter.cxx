#define Parameter_cxx
#include "sTGCTestbeamAnalysis/Parameter.h"
#include <TROOT.h>

ClassImp(Parameter)


void Parameter::Set(double _value, double _error)
{
  value = _value;
  error = _error;
}
Parameter::Parameter(){
  Set(-999999., 0.); 
}
Parameter::Parameter(double _value, double _error){
  Set(_value, _error);
}

Parameter::~Parameter(){

}
