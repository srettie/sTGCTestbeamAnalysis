#define PlotTGCCluster_cxx
#include "sTGCTestbeamAnalysis/PlotTGCCluster.h"

#include <iostream>
#include <vector>

#include <TProfile.h>

#include "sTGCTestbeamAnalysis/Mapping.h"

using namespace std;
using namespace Mapping;

PlotTGCCluster::PlotTGCCluster(TString _in, TString _out, TString treename, int run)
  : Plotter(_in, _out, treename)
{
  TString runtitle = TString::Format("Run %i", run);
  TString runname = TString::Format("run%i", run);
  TString list("");
  
  for (int layer=1; layer<=4; layer++) {
    
    TString sel = TString::Format("layer==%i", layer);
    TString name = runname + TString::Format("_layer%i", layer);;
    TString title = runtitle + TString::Format(", layer %i", layer);
    
    Plot("nclusters", sel, name+"_nclusters", title,
	 "N clusters per event", "Entries", "6, -0.5, 5.5", false, 0.);
    //  list += "nclusters";
    Plot("ngoodclusters", sel, name+"_ngoodclusters", title,
	 "N good clusters per event", "Entries", "6, -0.5, 5.5", false, 0.);
    //  list += "ngoodclusters";

    Plot("cl_raw_nstrip", sel, name+"_raw_nstrip", title,
	 "Raw N strips per cluster", "Entries", "9, -0.5, 8.5", false, 0.);
    

    //
    
    tree->Draw("globevent-event:globevent>>mProfile(20000,-0.5,19999.5)",
	       sel, "profile");
    TProfile* mProfile = NULL;
    gDirectory->GetObject("mProfile", mProfile);
    mProfile->SetName(name+"_eventconf_profile");
    TH1D eventconf("eventconf", "", 3, -0.25, 1.25);
    for (int i=1; i<=mProfile->GetNbinsX(); i++) {
      if (mProfile->GetBinEntries(i)>0) {
	eventconf.Fill( mProfile->GetBinContent(i) );
      }
    }
    eventconf.Scale(100./eventconf.GetEntries());
    eventconf.SetMinimum(0.);
    eventconf.GetXaxis()->SetTitle("Config");
    eventconf.GetYaxis()->SetTitle("Fraction [%]");
    eventconf.GetXaxis()->SetBinLabel(1, "#Deltaevt=0");
    eventconf.GetXaxis()->SetBinLabel(2, "Both");
    eventconf.GetXaxis()->SetBinLabel(3, "#Deltaevt=1");
    eventconf.Draw();
    SetPlotTitle(title);
    SavePlot(name+"_eventconf");
    
    //
    
    
    // good quality clusters

    sel += " && cl_tight";
    title += ", good quality";
    
    Plot("cl_raw_pdomin/1.e3", sel, name+"_raw_pdomin", title,
	 "Raw min. PDO per cluster [10^{3}]", "Entries", "120, 0., 3.");
    Plot("cl_raw_pdomax/1.e3", sel, name+"_raw_pdomax", title,
	 "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");
    Plot("cl_raw_tdomin/1.e3", sel, name+"_raw_tdomin", title,
	 "Raw min. TDO per cluster [10^{3}]", "Entries", "100, 2.3, 3.3");
    Plot("cl_raw_tdomax/1.e3", sel, name+"_raw_tdomax", title,
	 "Raw max. TDO per cluster [10^{3}]", "Entries", "100, 2.3, 3.3");
    
    Plot("cl_nstrip", sel, name+"_nstrip", title,
	 "N strips per cluster", "Entries", "9, -0.5, 8.5", false, 0.);
    
    
    Plot("cl_y_chfitChi2", sel, name+"_y_ch", title,
	 "y_{cl} [pitch]", "Entries", "200, 23.5, 63.5");
    Plot("cl_yrelStrip_fitChi2", sel, name+"_yrelStrip", title,
	 "y_{cl}^{rel} (rel. to strip center) [pitch]", "Entries",
	 "50, -0.5, 0.5", false, 0.);
    Plot("cl_yrelGap_fitChi2", sel, name+"_yrelGap", title,
	 "y_{cl}^{rel} (rel. to gap center) [pitch]", "Entries",
	 "50, -0.5, 0.5", false, 0.);
    Plot("cl_y_fitChi2", sel, name+"_y", title,
	 "y_{cl} [mm]", "Entries", "160, -5., 27.");
    

    for (int nstrip=3; nstrip<=6; nstrip++) {
      
      TString sel2 = sel+TString::Format(" && cl_nstrip==%i", nstrip);
      TString name2 = name + TString::Format("_nstrip%i", nstrip);
      TString title2 = title + TString::Format("; %i strips/cl.", nstrip);
      
      Plot("cl_y_chfitChi2", sel2, name2+"_y_ch", title2,
	   "y_{cl} [pitch units]", "Entries", "200, 23.5, 63.5");
      Plot("cl_yrelStrip_fitChi2", sel2, name2+"_yrelStrip", title2,
	   "y_{cl}^{rel} (rel. to strip center) [pitch]", "Entries",
	   "50, -0.5, 0.5", false, 0.);
      Plot("cl_yrelGap_fitChi2", sel2, name2+"_yrelGap", title2,
	   "y_{cl}^{rel} (rel. to gap center) [pitch]", "Entries",
	   "50, -0.5, 0.5", false, 0.);
      Plot("cl_y_fitChi2", sel2, name2+"_y", title2,
	   "y_{cl} [mm]", "Entries", "160, -5., 27.");
    } // end loop, n strip

  } // end loop, layers
  
  //  SaveHistos();
  //   MergePdfs(list);
}
