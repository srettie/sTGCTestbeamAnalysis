#define PlotFinalData_cxx
#include "sTGCTestbeamAnalysis/PlotFinalData.h"

#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TStyle.h>
#include <iostream>
#include <vector>

using namespace std;

PlotFinalData::PlotFinalData(TString _in, TString _out, TString treename)
  : Plotter(_in, _out, treename)
{

  PDOMaxStudy(323);

}

GausFitRes PlotFinalData::GausFit(TString name, TString title,
				  TString selection,
				  TString dyname, double min, double max)
{
  RooRealVar dy(dyname, "y_{strip-cluster} - y_{pixel-track} [#mum]", 0.);
  TTree* tree_sel = tree->CopyTree(selection);
  RooDataSet data("data", "", tree_sel, RooArgSet(dy));
  
  return FitTools::GausFit(name, title, dy, min, max, data, this);
}

void PlotFinalData::PDOMaxStudy(int run)
{
  TString nameRun  = TString::Format("run%i", run);
  TString titleRun = TString::Format("Run %i", run);
  
  int pdomax_0    = 1500;
  int pdomax_max  = 4500;
  //  int pdomax_step =  500;
  int pdomax_step =  250;
  
  vector<int> pdomax_bound;
  pdomax_bound.push_back(pdomax_0);
  while (pdomax_bound[pdomax_bound.size()-1]<pdomax_max) {
    int last = pdomax_bound[pdomax_bound.size()-1];
    pdomax_bound.push_back(last+pdomax_step);
  }
  int pdomax_nbins = pdomax_bound.size()-1;
  
  //  vector<vector<PDOMaxBin> > layer_pdomax_bins;
  vector<TGraphErrors> layer_pdomax_graph;

  for (int layer=1; layer<=4; layer++) {

    TString nameRunLayer = TString::Format("%s_layer%i",
					   nameRun.Data(), layer);
    TString titleRunLayer = TString::Format("%s, layer %i",
				    titleRun.Data(), layer);
    TString selectionRunLayer = TString::Format("run==%i&&layer==%i",
						run, layer);
    selectionRunLayer += "&&TMath::Abs(dy)<300.";


    // pdo-max distributions

    Plot("cl_raw_pdomax/1.e3", selectionRunLayer, nameRunLayer+"_pdomax",
	 titleRunLayer,
	 "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");

    Plot("cl_raw_pdomax/1.e3", selectionRunLayer+"&&cl_nstrip==3",
	 nameRunLayer+"_pdomax_nstrip3",
	 titleRunLayer+"; 3 strips/cluster",
	 "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");
    Plot("cl_raw_pdomax/1.e3", selectionRunLayer+"&&cl_nstrip==4",
	 nameRunLayer+"_pdomax_nstrip4",
	 titleRunLayer+"; 4 strips/cluster",
	 "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");
    Plot("cl_raw_pdomax/1.e3", selectionRunLayer+"&&cl_nstrip==5",
	 nameRunLayer+"_pdomax_nstrip5",
	 titleRunLayer+"; 5 strips/cluster",
	 "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");

    vector<TString> selList;
    selList.push_back("cl_nstrip==3");
    selList.push_back("cl_nstrip==4");
    selList.push_back("cl_nstrip==5");
    vector<TString> nameList;
    nameList.push_back("_nstrip3");
    nameList.push_back("_nstrip4");
    nameList.push_back("_nstrip5");
    PlotStack("cl_raw_pdomax/1.e3", selectionRunLayer,
	      selList, nameList, nameList,
	      nameRunLayer+"_pdomax_stack",
	      titleRunLayer,
	      "Raw max. PDO per cluster [10^{3}]", "Entries", "120, 0., 6.");


    // raw residual distributions
    
    TString _name = nameRunLayer+"_nstrip5";
    TString _title = titleRunLayer+", 5 strip cl.";
    TString _selection = selectionRunLayer+"&&cl_nstrip==5";
    int _min = 3.0e3;
    int _max = 3.5e3;
    GausFit(TString::Format("%s_pdomax%ito%i_dyRaw",
			    _name.Data(), _min, _max),
	    TString::Format("%s, raw residuals; %i < PDO_{max}^{cluster} < %i",
			    _title.Data(), _min, _max),
	    TString::Format("%s&&cl_raw_pdomax>%i&&cl_raw_pdomax<%i",
			    _selection.Data(),
			    _min, _max),
	    "dy_offset0", -500., 500.);
    //
    _min = 3.5e3;
    _max = 4.0e3;
    GausFit(TString::Format("%s_pdomax%ito%i_dyRaw",
			    _name.Data(), _min, _max),
	    TString::Format("%s, raw residuals; %i < PDO_{max}^{cluster} < %i",
			    _title.Data(), _min, _max),
	    TString::Format("%s&&cl_raw_pdomax>%i&&cl_raw_pdomax<%i",
			    _selection.Data(),
			    _min, _max),
	    "dy_offset0", -500., 500.);
    //
    _min = 4.0e3;
    _max = 4.5e3;
    GausFit(TString::Format("%s_pdomax%ito%i_dyRaw",
			    _name.Data(), _min, _max),
	    TString::Format("%s, raw residuals; %i < PDO_{max}^{cluster} < %i",
			    _title.Data(), _min, _max),
	    TString::Format("%s&&cl_raw_pdomax>%i&&cl_raw_pdomax<%i",
			    _selection.Data(),
			    _min, _max),
	    "dy_offset0", -500., 500.);
    
    
    // residual fits in pdo-max bins

    TGraphErrors graph;
    int point = 0;
    
    //    vector<PDOMaxBin> pdomax_bins;
    for (int b=1; b<=pdomax_nbins; b++) {
      PDOMaxBin bin;
      bin.min = pdomax_bound[b-1];
      bin.max = pdomax_bound[b];
      bin.center = (bin.min+bin.max)/2;
      
      TString name = TString::Format("%s_pdomax%i",
				     nameRunLayer.Data(), bin.center);
      TString title = TString::Format("%s; %i < PDO_{max}^{cluster} < %i",
				      titleRunLayer.Data(), bin.min, bin.max);
      TString selection = TString::Format("%s&&cl_raw_pdomax>%i&&cl_raw_pdomax<%i",
					  selectionRunLayer.Data(),
					  bin.min, bin.max);
      cout << "\n\n\tname = " << name
	   << "\n\ttitle = " << title
	   << "\n\tselection = " << selection
	   << "\n\n\n";

      bin.res = GausFit(name, title, selection);
      
      //      pdomax_bins.push_back(bin);
      
      double pointX = bin.center + ((layer-2.5)*pdomax_step*0.05);
      graph.SetPoint     (point, pointX, bin.res.sigma.value);
      graph.SetPointError(point, 0.    , bin.res.sigma.error);
      point++;
    }
    
    //    layer_pdomax_bins.push_back(pdomax_bins);
    layer_pdomax_graph.push_back(graph);
  }

  gStyle->SetEndErrorSize(3.);
  TMultiGraph pdomax_mg;
  //  int colors[] = {kOrange, kRed, kViolet, kBlue};
  int colors[] = {kOrange-6, kRed, kBlue, kViolet};
  for (int i=0; i<4; i++) {
    layer_pdomax_graph[i].SetMarkerColor(colors[i]);
    layer_pdomax_graph[i].SetMarkerSize(0.1);
    layer_pdomax_graph[i].SetLineColor(colors[i]);
    layer_pdomax_graph[i].SetLineWidth(2.);
    pdomax_mg.Add(&layer_pdomax_graph[i], "PL");
  }
  pdomax_mg.Draw("A");
  pdomax_mg.SetMinimum( 40.);
  pdomax_mg.SetMaximum(160.);
  pdomax_mg.GetXaxis()->SetTitle("Raw max. PDO per cluster");
  pdomax_mg.GetYaxis()->SetTitle("#sigma [#mum]");

  TLegend pdomax_l(0.3,0.61,0.50,0.80);
  pdomax_l.SetFillColor(0);
  pdomax_l.AddEntry(&layer_pdomax_graph[0], "Layer 1", "l");
  pdomax_l.AddEntry(&layer_pdomax_graph[1], "Layer 2", "l");
  pdomax_l.AddEntry(&layer_pdomax_graph[2], "Layer 3", "l");
  pdomax_l.AddEntry(&layer_pdomax_graph[3], "Layer 4", "l");
  pdomax_l.Draw("same");

  c->SetGridx();
  c->SetGridy();

  SetPlotTitle(titleRun);
  SavePlot(nameRun+"_sigmaVsPDOMax");
  gStyle->SetEndErrorSize(0.);

  c->SetGridx(0);
  c->SetGridy(0);
}
