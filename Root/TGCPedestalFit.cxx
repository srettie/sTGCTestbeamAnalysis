#define TGCPedestalFit_cxx
#include "sTGCTestbeamAnalysis/TGCPedestalFit.h"



//using namespace std;
//using namespace TMath;
//using namespace PedestalFit;

double PedestalFit::PedestalF(double* x, double* par)
{
  int channel = TMath::Nint(x[0]);
  
  for (int i=0; i<(int)channelList.size(); i++)
    if (channel==channelList[i])
      return par[i+nGausPars];
  
  if (channelListLowStat.size()>0) {
    int lastParIndex = nGausPars + channelList.size();
    double pedestalLowStatCh = par[lastParIndex];
    for (int i=0; i<(int)channelListLowStat.size(); i++)
      if (channel==channelListLowStat[i])
	return pedestalLowStatCh;
  }
  
  return 0.;
}

double PedestalFit::MultiGausF(double* x, double* par)
{
  double channel = x[0];
  int evt = TMath::Nint(x[1]);
  
  double norm  = par[evt*3    ];
  double mean  = par[evt*3 + 1];
  double sigma = par[evt*3 + 2];
  
  return norm*TMath::Gaus(channel, mean, sigma);
}

double PedestalFit::FullModel(double* x, double* par)
{
  return PedestalF(x, par) + MultiGausF(x, par);
}


TGCPedestalFit::TGCPedestalFit(TString in, TString _out,
			 int _run, int _layer)
  : out(_out), run(_run), layer(_layer), outtree(NULL)
{
  printINFO = false;

  maxNTotCluster = 2000;
  //  maxNTotCluster = 600;
  //  nClusterPerFit = 400;
  //  nClusterPerFit = 200;
  nClusterPerFit = 100;
  //  nClusterPerFit = 30;
  //  nClusterPerFit = 10;
  //  nClusterPerFit = 2;

  minEntriesPerChannel = 5;

  std::cout << "\n\nINFO:\tTGCPedestalFit:\tRun " << run
       << ",\tlayer " << layer << std::endl
       << std::endl;

  for (int ch=0; ch<64; ch++) {
    fitres[ch] = new TH1D(TString::Format("pedestalFitCh%i", ch),
			  "", 80, 700., 1100.);
    fitres[ch]->SetDirectory(0);
  }
  
  TFile fcluster(in);
  TTree* treecluster;
  fcluster.GetObject("tgc_cluster",treecluster);
  cluster = new TGCCluster(treecluster);
  
  //  SetupOutput();
  Execute();
  
  fcluster.Close();
}

TGCPedestalFit::~TGCPedestalFit()
{
  if (outtree) {
    outtree->Reset();
    delete outtree;
  }
  for (int ch=0; ch<64; ch++) delete fitres[ch];
}

void TGCPedestalFit::SetupOutput()
{
}

void TGCPedestalFit::Execute()
{
  vector<EventInfo>* fullEventList = GetEventList();
 
  std::cout << "\n\n\tTGCPedestalFit::Execute:\n\t\tTotal N clusters = "
       << fullEventList->size() << std::endl;

  int evtFirst = 0;
  int evtLast  = nClusterPerFit-1;
  TMVA::Timer timer((Int_t)fullEventList->size(), "TGCPedestalFit");
  while (evtLast < (int)fullEventList->size()) {
    
    timer.DrawProgressBar( (Int_t) evtFirst );

    vector<EventInfo> eventList;
    for (int evt=evtFirst; evt<=evtLast&&evt<(int)fullEventList->size(); evt++)
      eventList.push_back(fullEventList->at(evt));
    
    TH2D* h = GetHisto(eventList);
    TF2* f = GetFunction(eventList);
    
    //    TFitResultPtr res = h->Fit(f, "QS");
    TFitResultPtr res = h->Fit(f, "QS");
    FillResultHistos(res);
    //    if (res.Get()) SaveResult(res);

    evtFirst+=nClusterPerFit;
    evtLast +=nClusterPerFit;

    delete h;
    delete f;
  }
  std::cout << "TGCPedestalFit: elapsed time: " << timer.GetElapsedTime() << std::endl;
  
  for (int ch=0; ch<64; ch++) {
    if (fitres[ch]->GetEntries()>5) {
      //      fitres[ch]->Print("all");
      std::cout << "Channel " << ch
	   << " mean " << fitres[ch]->GetMean()
	   << " +/- " << fitres[ch]->GetMeanError()
	   << " RMS " << fitres[ch]->GetRMS()
	   << " +/- " << fitres[ch]->GetRMSError()
	   << std::endl;
    }
  }
  SaveResult();

  delete fullEventList;
}

vector<EventInfo>* TGCPedestalFit::GetEventList()
{
  vector<EventInfo>* eventList = new vector<EventInfo>();

  if (cluster->fChain == 0) return eventList;
  Long64_t nentries = cluster->fChain->GetEntries();

  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = cluster->LoadTree(jentry);
    if (ientry < 0) break;
    cluster->fChain->GetEntry(jentry);
    
    if (cluster->layer!=layer) continue;
    if (cluster->ngoodclusters!=1) continue;
    
    if (int(eventList->size()) >= maxNTotCluster) break;
    
    for (int cl=0; cl<(int)cluster->cl_raw_ch_channel->size(); cl++) {
      
      if (!cluster->cl_tight->at(cl)) continue;

      EventInfo event(cluster->event);
      event.mean .value = cluster->cl_y_chfitChi2    ->at(cl);
      event.sigma.value = cluster->cl_sigma_chfitChi2->at(cl);
      event.ch_channel  = cluster->cl_raw_ch_channel ->at(cl);
      event.ch_pdo      = cluster->cl_raw_ch_pdo     ->at(cl);

      eventList->push_back(event);      
    }

  }

  return eventList;
}
  
TH2D* TGCPedestalFit::GetHisto(vector<EventInfo>& eventList)
{
  int nEvents = eventList.size();
  TH2D* h = new TH2D("h", "", 64, -0.5, 63.5, nEvents, -0.5, nEvents-0.5);
  h->SetDirectory(0);
  
  for (int evt=0; evt<nEvents; evt++) {
    
    EventInfo& event = eventList[evt];
    for (int ch=0; ch<(int)event.ch_channel.size(); ch++) {
      int channel = event.ch_channel[ch];
      int pdo     = event.ch_pdo    [ch];
      h->Fill(channel, evt, pdo);
    }
  }
  
  return h;
}

void TGCPedestalFit::SetupPedestalF(vector<EventInfo>& eventList)
{
  int nEvents = eventList.size();
  channelList       .clear();
  channelListLowStat.clear();


  // check which channels are used and fill the lists
  TH1D h("channels_h", "", 64, -0.5, 63.5);
  
  for (int evt=0; evt<nEvents; evt++) {
    
    EventInfo& event = eventList[evt];
    for (int ch=0; ch<(int)event.ch_channel.size(); ch++) {
      h.Fill(event.ch_channel[ch]);
    }
  }

  for (int bin=1; bin<=h.GetNbinsX(); bin++) {
    int channel = h.GetBinCenter (bin);
    int entries = h.GetBinContent(bin);
    if      (entries >= minEntriesPerChannel) channelList.push_back(channel);
    else if (entries > 0) channelListLowStat.push_back(channel);
  }

  nPedestals = channelList.size();
  if (channelListLowStat.size()>0) nPedestals++;


  // print info
  if (printINFO) {
    std::cout << "\n\nINFO:\tTGCPedestalFit::GetPedestalsModel:\n\t\tChannel list:";
    for (int i=0; i<(int)channelList.size(); i++)
      std::cout << "  " << channelList[i];
    std::cout << "\n\t\tLow stat channel list:";
    for (int i=0; i<(int)channelListLowStat.size(); i++)
      std::cout << "  " << channelListLowStat[i];
    std::cout << "\n\n";
  }
}

TF2* TGCPedestalFit::GetFunction(vector<EventInfo>& eventList)
{
  SetupPedestalF(eventList);
  
  int nEvents = eventList.size();
  
  int counter = 0;
  for (int evt=0; evt<nEvents; evt++) {
    
    EventInfo& event = eventList[evt];
    event.index_norm  = counter;
    event.index_mean  = counter+1;
    event.index_sigma = counter+2;
    counter += 3;    
  }
  nGausPars = counter;
  
  nTotPars = nGausPars + nPedestals;
  
  if (printINFO)
    std::cout << "\n\nINFO:\tTGCPedestalFit::GetFunction:"
	 << "\n\t\t nGausPars  = " << nGausPars
	 << "\n\t\t nPedestals = " << nPedestals
	 << "\n\t\t nTotPars   = " << nTotPars
	 << "\n\n";

  // define the TF2
  TF2* f = new TF2("f", PedestalFit::FullModel,
		   -0.5, 63.5, -0.5, nEvents-0.5, nTotPars);
  
  // set parameter names and initialize them
  for (int evt=0; evt<nEvents; evt++) {
    
    EventInfo& event = eventList[evt];
    
    TString num = TString::Format("%i", event.num);
    f->SetParName(event.index_norm , "norm" +num);
    f->SetParName(event.index_mean , "mean" +num);
    f->SetParName(event.index_sigma, "sigma"+num);
    
    f->SetParameter(event.index_norm , 3.e3);
    f->SetParameter(event.index_mean , event.mean .value);
    f->SetParameter(event.index_sigma, event.sigma.value);
  }
  
  for (int par=nGausPars; par<nTotPars; par++) {
    
    if (par==nTotPars-1 && channelListLowStat.size()>0)
      f->SetParName(par, "pedestalLowStatCh");
    else {
      int channel = channelList[par-nGausPars];
      f->SetParName(par, TString::Format("pedestal%i", channel) );
    }
    
    f->SetParameter(par, 900.);
  }
  
  return f;
}

void TGCPedestalFit::FillResultHistos(TFitResultPtr res)
{
  for (int par=nGausPars; par<nTotPars; par++) {
    
    if ( !(par==nTotPars-1 && channelListLowStat.size()>0) ) {
      
      int channel = channelList[par-nGausPars];
      double value = res->Parameter(par);
      double error = res->ParError (par);
      fitres[channel]->Fill(value);
    }
  }
}

void TGCPedestalFit::SaveResult()
{
  TH1D h("pedestals", "", 64, -0.5, 63.5);
  
  for (int ch=0; ch<64; ch++) {
    if (fitres[ch]->GetEntries()>5) {
      
      h.SetBinContent(ch+1, fitres[ch]->GetMean()     );
      h.SetBinError  (ch+1, fitres[ch]->GetMeanError());
    }
  }
  h.Print("all");
  
  TFile file(out, "recreate");
  h.Write();
  for (int ch=0; ch<64; ch++) {
    if (fitres[ch]->GetEntries()>5) {
      fitres[ch]->Write();
    }
  }
  file.Close();
}

void TGCPedestalFit::SaveResult(TFitResultPtr res)
{
  TGraphErrors g;
  g.SetName("pedestalfit");

  int point = 0;
  for (int par=nGausPars; par<nTotPars; par++) {
    
    if (par==nTotPars-1 && channelListLowStat.size()>0) {
      g.SetPoint     (point, -1, res->Parameter(par));
      g.SetPointError(point, 0., res->ParError (par));
      point++;
    }
    else {
      int channel = channelList[par-nGausPars];
      g.SetPoint     (point, channel, res->Parameter(par));
      g.SetPointError(point, 0.     , res->ParError (par));
      point++;
    }
  }
  
  TFile file(out, "recreate");
  g.Write();
  file.Close();
}
