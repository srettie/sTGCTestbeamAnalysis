#define PlotCombEvent_cxx
#include "sTGCTestbeamAnalysis/PlotCombEvent.h"

#include <TStyle.h>

#include <iostream>
#include <vector>

using namespace std;

PlotCombEvent::PlotCombEvent(TString _in, TString _out, TString treename, int run, int layer)
  : Plotter(_in, _out, treename)
{
  TString title = TString::Format("Run %i, layer %i", run, layer);
  TString name  = TString::Format("run%i_layer%i"   , run, layer);
  
  TString sel = "cl_nstrip>=3&&cl_nstrip<=5";
  sel += "&&evtoffset==0";
  sel += "&&TMath::Abs(dy)<2.e4";

  c->SetGridx();
  c->SetGridy();
  
  Plot("trk_x", sel, name+"_trk_x", title,
       "x_{pixel-track} [#mum]", "Entries", "60, -6.e3, 6.e3",
       false, 0., -1111, "e");
  Plot("trk_y", sel, name+"_trk_y", title,
       "y_{pixel-track} [#mum]", "Entries", "60, -12.e3, 12.e3",
       false, 0., -1111, "e");
  Plot2D("trk_y:trk_x", sel, name+"_trk_yx", title,
	 "x_{pixel-track} [#mum]", "y_{pixel-track} [#mum]",
	 "60, -6.e3, 6.e3, 60, -12.e3, 12.e3");
  
  Plot("cl_ch_channel", sel, name+"_cl_channels", title,
       "Channel", "Entries", "64, -0.5, 63.5");
}
