#define TxtToRoot_cxx
#include "sTGCTestbeamAnalysis/TxtToRoot.h"

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>

#include <iostream>

using namespace std;

void TxtToRoot(TString in, TString out, TString treenm, TString format)
{
  cout << "\nMaking ntuple from .txt file:" << endl
       << "\tinput    \t:\t" << in     << endl
       << "\toutput   \t:\t" << out    << endl
       << "\ttree name\t:\t" << treenm << endl
       << "\tformat   \t:\t" << format << endl;
  
  TTree tree(treenm, "");
  tree.SetAutoSave(0);
  tree.SetAutoFlush(0);
  tree.SetDirectory(0);
  tree.ReadFile(in, format, ' ');
  
  int nentries = tree.GetEntries();
  cout << "\tN entries\t:\t" << nentries << endl;
  
  TFile file(out, "recreate");
  tree.Write();
  file.Close();
}
