#define Plotter_cxx
#include "sTGCTestbeamAnalysis/Plotter.h"

#include "sTGCTestbeamAnalysis/PlotFitResult.h"

//using namespace std;
using namespace RooFit;

Plotter::Plotter(TString in, TString _out, TString treename)
{
  out = _out;
  
  SetAtlasStyle();
  gStyle->SetPalette(1,0);
  TH1::SetDefaultSumw2();
  
  //  latex.SetNDC();
  latex.SetTextFont(42);
  latex.SetTextSize(0.05);
  latex.SetTextColor(1);

  publicFormat = true;
  TString formats = "pdf;eps";

  formatList = Tools::StrVec(formats);
  makingPdfs = formats.Contains("pdf");

  c = new TCanvas("c", "", 800, 600);

  tree = NULL;
  file = TFile::Open(in, "UPDATE");
  if (file) file->GetObject(treename,tree);
  if (!tree) {
    std::cout << "\n\nERROR:\tPlotter::Plotter:\t"
	      << "Didn't find file " << file << ", or tree " << treename << std::endl;
    exit(1);
  }
}

Plotter::Plotter(TString _out)
{
  out = _out;
  file = NULL;
  tree = NULL;
  
  SetAtlasStyle();
  gStyle->SetPalette(1,0);
  TH1::SetDefaultSumw2();
  
  //  latex.SetNDC();
  latex.SetTextFont(42);
  latex.SetTextSize(0.05);
  latex.SetTextColor(1);

  publicFormat = true;
  TString formats = "pdf;eps";

  formatList = Tools::StrVec(formats);
  makingPdfs = formats.Contains("pdf");

  c = new TCanvas("c", "", 800, 600);
}

Plotter::~Plotter()
{
  delete c;
  if (file) file->Close();
}

void Plotter::Plot(Type type,
		   TString varexp, TString selection, TString name,
		   TString title, TString xtitle, TString ytitle,
		   TString bin, bool log,
		   float min, float max, TString drawopt)
{
  if (!tree) {
    std::cout << "\n\nERROR:\tPlotter:\t There is not tree available. Need to initialize the Plotter using Plotter(TString _in, TString _out, TString treename)"<< std::endl;
    exit(1);
  }

  TH1* h = NULL;
  
  if (bin!="") bin = "("+bin+")";
  tree->Draw(varexp+">>"+name+bin, selection, drawopt);
  gDirectory->GetObject(name, h);
  hlist.push_back(h);

  h->GetXaxis()->SetTitle(xtitle);
  h->GetYaxis()->SetTitle(ytitle);
  h->SetMinimum(min);
  h->SetMaximum(max);
  
  if (type==typeProfile) h->SetMarkerStyle(6);

  SetPlotTitle(title);
  SavePlot(name, log);
}

void Plotter::Plot(TString varexp, TString selection, TString name,
		   TString title, TString xtitle, TString ytitle,
		   TString bin, bool log,
		   float min, float max, TString drawopt)
{
  Plot(type1D, varexp, selection, name, title, xtitle, ytitle,
       bin, log, min, max, drawopt);
}

void Plotter::Plot2D(TString varexp, TString selection, TString name,
		     TString title, TString xtitle, TString ytitle,
		     TString bin,
		     float min, float max)
{
  Plot(type2D, varexp, selection, name, title, xtitle, ytitle,
       bin, false, min, max, "box");
}

void Plotter::PlotProfile(TString varexp, TString selection, TString name,
			  TString title, TString xtitle, TString ytitle,
			  TString bin, bool log,
			  float min, float max)
{
  Plot(type1D, varexp, selection, name, title, xtitle, ytitle,
       bin, log, min, max, "profile");
}

void Plotter::PlotStack(TString varexp, TString selection,
			std::vector<TString> selList, std::vector<TString> nameList,
			std::vector<TString> titleList,
			TString name,
			TString title, TString xtitle, TString ytitle,
			TString bin, bool log,
			float min, float max)
{
  if (!tree) {
    std::cout << "\n\nERROR:\tPlotter:\t There is not tree available. Need to initialize the Plotter using Plotter(TString _in, TString _out, TString treename)"<< std::endl;
    exit(1);
  }
  
  if (bin!="") bin = "("+bin+")";

  THStack stack("stack", "");
  
  int nhists = selList.size();
  for (int i=0; i<nhists; i++) {
    TH1* h = NULL;
    tree->Draw(varexp+">>"+name+"_"+nameList[i]+bin,
 	       selection+"&&"+selList[i], "hist");
    gDirectory->GetObject(name+"_"+nameList[i], h);
    h->SetFillColor(kOrange+1+i);
    stack.Add(h, "hist");
    hlist.push_back(h);
  }
  
  stack.SetMinimum(min);
  stack.SetMaximum(max);
  stack.Draw();
  stack.GetXaxis()->SetTitle(xtitle);
  stack.GetYaxis()->SetTitle(ytitle);
  
  SetPlotTitle(title);
  SavePlot(name, log);
}

TGraphErrors* Plotter::GetGraphErrors(TString varexp, TString selection)
{
  int n = tree->Draw(varexp, selection, "goff");
  TGraphErrors *g = new TGraphErrors( n, tree->GetV1(), tree->GetV2(),
				      tree->GetV3(), tree->GetV4()); 
  return g;
}

void Plotter::PlotFit(TString name, TString title,
		      RooRealVar& var, int nbins, double min, double max,
		      RooDataSet& dat, RooAbsPdf& pdf,
		      RooAbsPdf* bkg, TString pdfNormRange)
{
  if (pdfNormRange=="") {
    pdfNormRange = "pdfNormRange";
    var.setRange(pdfNormRange, min, max);
  }

  RooPlot* plot = var.frame(min, max, nbins);
  dat.plotOn(plot, MarkerSize(1));
  pdf.plotOn(plot, LineColor(2),
   	     NormRange(pdfNormRange));
  if (bkg) pdf.plotOn(plot, Components(*bkg), LineColor(2), LineStyle(2),
   		      NormRange(pdfNormRange));
  dat.plotOn(plot, MarkerSize(1));

  SetAtlasStyle();
  plot->Draw();
  
  SetPlotTitle(title);
  plot->GetYaxis()->SetTitle("Events");
  SavePlot(name);
  
  delete plot;
}

void Plotter::Plot(TString name, TString title,
		   RooRealVar& var, int nbins, double min, double max,
		   RooDataSet& dat)
{
  RooPlot* plot = var.frame(min, max, nbins);
  dat.plotOn(plot, MarkerSize(1));
  
  SetAtlasStyle();
  plot->Draw();
  
  SetPlotTitle(title);
  plot->GetYaxis()->SetTitle("Events");
  SavePlot(name);
  
  delete plot;
}

void Plotter::SetPlotTitle(TString title)
{
  double lx = c->GetLeftMargin();
  double ly1 = 0.94;
  double ly2 = 0.89;
  double ly3 = 0.84;

  std::vector<TString> vec = Tools::StrVec(title);
  int size = vec.size();

  if (!publicFormat) {
    if (size>=3) c->SetTopMargin(0.18);
    else c->SetTopMargin(0.13);

    if (size>=1) latex.DrawLatexNDC(lx, ly1, vec[0]);
    if (size>=2) latex.DrawLatexNDC(lx, ly2, vec[1]);
    if (size>=3) latex.DrawLatexNDC(lx, ly3, vec[2]);
  }
  else {
    if (size>=3) {
      c->SetTopMargin(0.08);
      latex.DrawLatexNDC(lx, ly1, vec[2]);
    }

    //    if (size>=3) latex.DrawLatexNDC(lx+0.05, ly3, vec[2]);

    //     legend = new TLegend(0.6, 0.70, 0.90, 0.90);
    //     legend->SetFillColor(0);
    //     legend->AddEntry();
    //     legend->Draw("same");
  }
}

void Plotter::SavePlot(TString name, bool log)
{
  TString _outname = out+"/"+name;
  system("mkdir -p "+out);
  
  for (int i=0; i<(int)formatList.size(); i++)
    c->SaveAs(_outname+"."+formatList[i]);

  if (log) {
    c->SetLogy();
    for (int i=0; i<(int)formatList.size(); i++)
      c->SaveAs(_outname+"_log."+formatList[i]);
    c->SetLogy(false);
  }
}

void Plotter::SaveHistos()
{
  TString filename = out+".root";
  TFile hfile(filename, "recreate");

  for (int i=0; i<(int)hlist.size(); i++) hlist[i]->Write();

  hfile.Close();
}

void Plotter::MergePdfs(TString plotlist, TString name, TString nupOpt)
{
  if (!makingPdfs) {
    std::cout << "\n\nWARNING:\tPlotter::Merge:\t Only works when PDF files are produced"
	      << std::endl;
    return;
  }

  TString cmdline = "pdfjam --landscape --quiet --nup "+nupOpt;

  // output  
  cmdline += " -o ";
  if (name!="") cmdline += out+"/"+name+".pdf";
  else cmdline += out+".pdf";
  
  // input
  if (plotlist=="") {
    for (int i=0; i<(int)hlist.size(); i++) {
      if (i>0) plotlist += ";";
      plotlist += hlist[i]->GetName();
    }
  }

  TObjArray* array = plotlist.Tokenize(";");
  int nentries = array->GetEntries();
  for (int i=0; i<nentries; i++) {
    TObjString* plotname = (TObjString*)array->At(i);
    TString plotpath = " "+out+"/"+plotname->String()+".pdf";
    cmdline += plotpath;
  }
  
  system(cmdline);
}
