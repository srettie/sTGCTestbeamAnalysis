#define MakeCombEvent_cxx
#include "sTGCTestbeamAnalysis/MakeCombEvent.h"


//using namespace std;
//using namespace TMath;
//using namespace Mapping;

MakeCombEvent::MakeCombEvent(TString intrack, TString incluster, TString _out,
			     int _run, int _layer)
  : out(_out), run(_run), layer(_layer)
{
  TFile ftrack(intrack);
  TTree* treetrack;
  ftrack.GetObject("pixel_trk",treetrack);
  track = new PixelTrack(treetrack);
  
  TFile fcluster(incluster);
  TTree* treecluster;
  fcluster.GetObject("tgc_cluster",treecluster);
  cluster = new TGCCluster(treecluster);
  
  SetupOutput();
  Loop();
  
  ftrack.Close();
  fcluster.Close();
}

MakeCombEvent::~MakeCombEvent()
{
  outtree->Reset();
  delete outtree;
}

void MakeCombEvent::SetupOutput()
{
  
  outtree = new TTree("comb_evt", "");
  outtree->SetAutoSave(0);
  outtree->SetAutoFlush(0);
  outtree->SetDirectory(0);

  cl_ch_channel = 0;
  cl_ch_pdo = 0;

  // pixel info
  outtree->Branch("event", &track->event, "event/I");
  
  // tgc info
  outtree->Branch("run", &cluster->run, "run/I");
  outtree->Branch("tgcevent", &cluster->event, "tgcevent/I");
  outtree->Branch("tgcglobevent", &cluster->globevent, "tgcglobevent/I");
  outtree->Branch("layer", &cluster->layer, "layer/I");
  
  // new info
  outtree->Branch("trk_nhits", &trk_nhits, "trk_nhits/I");
  outtree->Branch("trk_chi2", &trk_chi2, "trk_chi2/D");
  outtree->Branch("trk_x", &trk_x, "trk_x/D");
  outtree->Branch("trk_y", &trk_y, "trk_y/D");

  outtree->Branch("cl_y", &cl_y, "cl_y/D");
  outtree->Branch("cl_yrelStrip", &cl_yrelStrip, "cl_yrelStrip/D");
  outtree->Branch("cl_yrelGap", &cl_yrelGap, "cl_yrelGap/D");
  outtree->Branch("cl_nstrip", &cl_nstrip, "cl_nstrip/I");
  outtree->Branch("cl_raw_pdomax", &cl_raw_pdomax, "cl_raw_pdomax/I");
  outtree->Branch("cl_raw_pdomax2", &cl_raw_pdomax2, "cl_raw_pdomax2/I");
  outtree->Branch("cl_raw_tdomin", &cl_raw_tdomin, "cl_raw_tdomin/I");
  outtree->Branch("cl_raw_tdomax", &cl_raw_tdomax, "cl_raw_tdomax/I");
  outtree->Branch("cl_ch_channel", &cl_ch_channel);
  outtree->Branch("cl_ch_pdo", &cl_ch_pdo);

  outtree->Branch("dy", &cl_trk_dy, "dy/D");
  outtree->Branch("evtoffset", &evtoffset, "evtoffset/I");
  
}

void MakeCombEvent::Loop()
{

  if (track  ->fChain == 0) return;
  if (cluster->fChain == 0) return;

  Long64_t track_nentries = track  ->fChain->GetEntries();
  Long64_t cluster_nentries = cluster->fChain->GetEntries();

  std::cout << std::endl
	    << "Run " << run << ",\tlayer " << layer << std::endl
	    << "N entries in pixel_trk  \t" << track_nentries << std::endl
	    << "N entries in tgc_cluster\t" << cluster_nentries << std::endl
	    << std::endl;
  
  int preevent = -1;
  Long64_t lastmatched_cluster_jentry = 0;
  
  // pixel event loop
  for (Long64_t track_jentry=0; track_jentry<track_nentries;track_jentry++) {
    Long64_t track_ientry = track->LoadTree(track_jentry);
    if (track_ientry < 0) break;
    track->fChain->GetEntry(track_jentry);


    if (track->event<preevent) lastmatched_cluster_jentry=0;
    preevent = track->event;
    
    if (track->ntracks!=1) continue;
    
    
    
    // tgc event loop
    for (Long64_t cluster_jentry = TMath::Max(lastmatched_cluster_jentry-20,
				       (Long64_t)0);
	 cluster_jentry<cluster_nentries;cluster_jentry++) {
      Long64_t cluster_ientry = cluster->LoadTree(cluster_jentry);
      if (cluster_ientry < 0) break;
      cluster->fChain->GetEntry(cluster_jentry);
      
      
      evtoffset = cluster->event - track->event;
      if (evtoffset>2) break;
      if (TMath::Abs(evtoffset)>1) continue;
      //      if (evtoffset!=1) continue;

      lastmatched_cluster_jentry = cluster_jentry;
      
      if (cluster->layer!=layer) continue;
      if (cluster->ngoodclusters!=1) continue;
      
      
      StoreEvent();
    }

  }


  std::cout << std::endl
	    << "N events recorded: \t" << outtree->GetEntries() << std::endl
	    << std::endl;
  
  TFile file(out, "recreate");
  outtree->Write();
  file.Close();
}

void MakeCombEvent::StoreEvent()
{
  bool filled = false;

  cl_ch_channel->clear();
  cl_ch_pdo->clear();
  
  for (int cl=0; cl<cluster->nclusters; cl++) {

    if (!cluster->cl_tight->at(cl)) continue;
    
    cl_y = cluster->cl_y_fitChi2->at(cl);
    // raw translation correction of 1.1 cm
    cl_y -= 11.;
    //
    cl_yrelStrip   = cluster->cl_yrelStrip_fitChi2->at(cl);
    cl_yrelGap     = cluster->cl_yrelGap_fitChi2  ->at(cl);
    cl_nstrip      = cluster->cl_nstrip           ->at(cl);
    cl_raw_pdomax  = cluster->cl_raw_pdomax       ->at(cl);
    cl_raw_pdomax2 = cluster->cl_raw_pdomax2      ->at(cl);
    cl_raw_tdomin  = cluster->cl_raw_tdomin       ->at(cl);
    cl_raw_tdomax  = cluster->cl_raw_tdomax       ->at(cl);

    for (int i=0; i<(int)cluster->cl_ch_channel->at(cl).size(); i++) {
      cl_ch_channel->push_back( cluster->cl_ch_channel->at(cl)[i] );
      cl_ch_pdo    ->push_back( cluster->cl_ch_pdo    ->at(cl)[i] );
    }

    if (filled) {
      std::cout << "\n\nERROR:\tMakeCombEvent::Loop:\t"
	   << "Event with more than one good cluster, not ready for that"
	   << "\n\n\n";
      exit(1);
    }
    filled = true;  
  }
  
  if (!filled) {
    std::cout << "\n\nERROR:\tMakeCombEvent::Loop:\t"
	 << "NO good cluster on this event, but it passed..."
	 << "\n\n\n";
    exit(1);
    //    return;
  }

  double zpos = 0.;
  //       if (layer==1) zpos = 0.;
  //       if (layer==2) zpos = 11.25;
  //       if (layer==3) zpos = 22.70;
  //       if (layer==4) zpos = 33.95;
  if (layer==1) zpos = 8.55;
  if (layer==2) zpos = 19.80;
  if (layer==3) zpos = 31.25;
  if (layer==4) zpos = 42.50;
  zpos += (531.25 - 577.205);
  trk_nhits = track->nhits;
  trk_chi2  = track->chi2;
  trk_x = track->mx*zpos + track->x0;
  trk_y = track->my*zpos + track->y0;
  
  cl_trk_dy = cl_y - trk_y;
      
  // use microns
  trk_x           *= 1.e3;
  trk_y           *= 1.e3;
  cl_y            *= 1.e3;
  cl_trk_dy       *= 1.e3;
      
  outtree->Fill();
}
