#define MakePixelTrack_cxx

#include "sTGCTestbeamAnalysis/MakePixelTrack.h"

//using namespace std;
//using namespace Mapping;
//using namespace TMath;

MakePixelTrack::MakePixelTrack(TString in, TString _out)
  : fChain(0), out(_out)
{
  TFile f(in);
  TTree* tree;
  f.GetObject("pixel_raw",tree);
  Init(tree);

  SetupOutput();
  Loop();
  WriteOutput();

  f.Close();
}

MakePixelTrack::~MakePixelTrack()
{
  outtree->Reset();
  delete outtree;
}

void MakePixelTrack::SetupOutput()
{
  missinglayer = 0;
  dxi = 0;
  dyi = 0;

  outtree = new TTree("pixel_trk", "");
  outtree->SetAutoSave(0);
  outtree->SetAutoFlush(0);
  outtree->SetDirectory(0);

  outtree->Branch("event", &event_out, "event/I");
  outtree->Branch("ntracks", &ntracks, "ntracks/I");
  outtree->Branch("nhits", &nhits, "nhits/I");
  outtree->Branch("missinglayer", &missinglayer);
  outtree->Branch("x0", &x0, "x0/D");
  outtree->Branch("x0_error", &x0_error, "x0_error/D");
  outtree->Branch("mx", &mx, "mx/D");
  outtree->Branch("mx_error", &mx_error, "mx_error/D");
  outtree->Branch("dxi", &dxi);
  outtree->Branch("y0", &y0, "y0/D");
  outtree->Branch("y0_error", &y0_error, "y0_error/D");
  outtree->Branch("my", &my, "my/D");
  outtree->Branch("my_error", &my_error, "my_error/D");
  outtree->Branch("dyi", &dyi);
  outtree->Branch("chi2", &chi2, "chi2/D");
  outtree->Branch("prob", &prob, "prob/D");
}

void MakePixelTrack::WriteOutput()
{
  TFile file(out, "recreate");
  outtree->Write();
  file.Close();
}

void MakePixelTrack::FillCache()
{
  cache_nHitXLayer[layer]++;
  cache_layer.push_back(layer);
  cache_x.push_back(x);
  cache_y.push_back(y);
  cache_trk_chi2.push_back(trk_chi2);
}

void MakePixelTrack::CleanCache()
{
  for (int i=0; i<6; i++) cache_nHitXLayer[i] = 0;
  cache_layer.clear();
  cache_x.clear();
  cache_y.clear();
  cache_trk_chi2.clear();
}

void MakePixelTrack::CleanStorage()
{
  event_out = -9999999;
  ntracks = 0;
  nhits = 0;
  missinglayer->clear();
  x0 = -9999999.;
  x0_error = -9999999.;
  mx = -9999999.;
  mx_error = -9999999.;
  dxi->clear();
  y0 = -9999999.;
  y0_error = -9999999.;
  my = -9999999.;
  my_error = -9999999.;
  dyi->clear();
  chi2 = -9999999.;
  prob = -9999999.;
}

void MakePixelTrack::Loop()
{
  int preevent = -1;
  CleanCache();

  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntries();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<=nentries;jentry++) {
    
    if (jentry<nentries) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      // the values in the input ntuple are minus the real values
      x *= -1.;
      y *= -1.;
    }
    
    if (preevent>=0 && event!=preevent) {
      
      CleanStorage();
      StoreEvent(preevent);
      CleanCache();
      
      // after the last event, exit the loop here
      if (jentry>=nentries) break;      
    }

    // fill cache
    FillCache();

    // keep info needed in the next entry
    preevent = event;
  }
  
}

void MakePixelTrack::StoreEvent(int evtnumber)
{
  for (int i=0; i<6; i++) {
    int nHitXLayer = cache_nHitXLayer[i];
    if (nHitXLayer>1) {
      std::cout << "WARNING:\tEvent with more than 1 track"
	   << ", not reconstructed, setting default values"
	   << "\t(event " << evtnumber << ")"
		<< std::endl;
      event_out = evtnumber;
      ntracks = nHitXLayer;
      outtree->Fill();
      return;
    }
  }


  TGraph xz_g;
  TGraph yz_g;

  int pointcounter = 0;
  for (int i=0; i<(int)cache_layer.size(); i++) {
    double _z = Mapping::GetPixelLayerZ(cache_layer[i]);
    xz_g.SetPoint(pointcounter, _z, cache_x[i]);
    yz_g.SetPoint(pointcounter, _z, cache_y[i]);
    pointcounter++;
  }


  // track fit
  TF1 xz_f("xz_f", "[0]+[1]*x", -1500, 1500.);
  TF1 yz_f("yz_f", "[0]+[1]*x", -1500, 1500.);
  xz_g.Fit("xz_f", "Q");
  yz_g.Fit("yz_f", "Q");
	

  // fill tree
  event_out = evtnumber;
  ntracks = 1;
  nhits = (int)cache_layer.size();
  for (int i=0; i<6; i++)
    if (cache_nHitXLayer[i]==0) missinglayer->push_back(i);
  x0       = xz_f.GetParameter(0);
  x0_error = xz_f.GetParError(0);
  mx       = xz_f.GetParameter(1);
  mx_error = xz_f.GetParError(1);
  y0       = yz_f.GetParameter(0);
  y0_error = yz_f.GetParError(0);
  my       = yz_f.GetParameter(1);
  my_error = yz_f.GetParError(1);

  for (int i=0; i<(int)cache_layer.size(); i++) {
    double _z = Mapping::GetPixelLayerZ(i);
    double _dx = cache_x[i] - xz_f.Eval(_z);
    double _dy = cache_y[i] - yz_f.Eval(_z);
    dxi->push_back(_dx);
    dyi->push_back(_dy);
  }

  chi2 = cache_trk_chi2[0];
  prob = TMath::Prob(chi2, nhits-2);

  outtree->Fill();
}
