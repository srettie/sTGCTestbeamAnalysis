#define PlotFitResult_cxx
#include "sTGCTestbeamAnalysis/PlotFitResult.h"



//using namespace std;
//using namespace TMath;

const int PlotFitResult::nRuns = 6;
//const int PlotFitResult::runList[] = {321, 323, 325, 326, 334, 335};
const int PlotFitResult::runList[] = {335, 334, 326, 325, 323, 321};
//const char* PlotFitResult::runLabelList[] = {"A", "B", "C", "D", "E", "F"};
const char PlotFitResult::runLabelList[] = {'A', 'B', 'C', 'D', 'E', 'F'};

PlotFitResult::PlotFitResult(TString _in, TString _out)
  : Plotter(_in, _out, "fitresult")
{
//   int nConfigs = 3;

//   TString selections[] = {"pedestal==Pedestals::OldGlobal()",
// 			  "pedestal==Pedestals::Global()",
// 			  "pedestal==Pedestals::PerChannel()"};

//   TString labels[] = {"pedestalOldGlobal",
// 		      "pedestalNewGlobal",
// 		      "pedestalPerChannel"};

//   TString titles[] = {"Old global pedestals",
// 		      "New global pedestals",
// 		      "Channel dependent pedestals"};

  int nConfigs = 1;

  //TString selections[] = {"pedestal==Pedestals::PerChannel()"};
  TString selections[] = {"pedestal==-2"};
  
  TString labels[] = {"pedestalPerChannel"};

  TString titles[] = {"Channel dependent pedestals"};

  for (int i=0; i<nConfigs; i++) {

    //    if (i!=2) continue;

    PlotVsRunLayerID("full2.sigma", selections[i], "sigma_"+labels[i],
		     titles[i], "#sigma [#mum]", 30., 85.);
    //		     titles[i], "#sigma [#mum]", 30., 95.);


    PlotVsRunLayerID("full2.catSigma[0]", selections[i],
		     "sigma_nstrip3_"+labels[i], titles[i],
		     "#sigma (3 strip cl.) [#mum]",
		     30., 110.);
    PlotVsRunLayerID("full2.catSigma[1]", selections[i],
		     "sigma_nstrip4_"+labels[i], titles[i],
		     "#sigma (4 strip cl.) [#mum]",
		     30., 110.);
    PlotVsRunLayerID("full2.catSigma[2]", selections[i],
		     "sigma_nstrip5_"+labels[i], titles[i],
		     "#sigma (5 strip cl.) [#mum]",
		     30., 110.);
    


    PlotVsRunLayerID("full2.phi", selections[i], "alignPhi_"+labels[i],
		     titles[i], "#phi_{align} [degrees]", -0.5, 0.5);
    PlotVsRunLayerID("full2.offset", selections[i], "alignOffset_"+labels[i],
		     titles[i], "#deltay_{align} [#mum]", -1.e3, 1.e3);
    
    PlotVsRunLayerID("full2.phi", selections[i],
		     "alignPhiRel1_"+labels[i], titles[i], 
		     "#phi_{align} - #phi_{align}^{layer 1} [degrees]",
		     //		     "#phi_{align}^{rel. to layer 1} [degrees]",
		     -0.25, 0.25, "rel1");
    PlotVsRunLayerID("full2.offset", selections[i],
		     "alignOffsetRel1_"+labels[i], titles[i],
		     "#deltay_{align} - #deltay_{align}^{layer 1} [#mum]",
		     //		     "#deltay_{align}^{rel. to layer 1} [#mum]",
		     -1.e2, 4.e2, "rel1");


    PlotVsRunLayerID("full2.goodFrac", selections[i],
		     "goodFrac_"+labels[i], titles[i],
		     "Thin Gaussian fraction",
		     0.6, 1.1);
    PlotVsRunLayerID("full2.wideSigma", selections[i],
		     "wideSigma_"+labels[i], titles[i],
		     "#sigma_{wide} [#mum]",
		     50., 800.);


    
    PlotVsRunLayerID("full2.catAmpl[0]", selections[i],
		     "sineAmpl_nstrip3_"+labels[i], titles[i],
		     "Sine corr. amplitude (3 strip cl.) [#mum]",
		     50., 350.);
    PlotVsRunLayerID("full2.catAmpl[1]", selections[i],
		     "sineAmpl_nstrip4_"+labels[i], titles[i],
		     "Sine corr. amplitude (4 strip cl.) [#mum]",
		     0., 300.);
    PlotVsRunLayerID("full2.catAmpl[2]", selections[i],
		     "sineAmpl_nstrip5_"+labels[i], titles[i],
		     "Sine corr. amplitude (5 strip cl.) [#mum]",
		     0., 300.);


    PlotVsRunLayerID("full2.catSigfrac[0]", selections[i],
		     "signalFrac_nstrip3_"+labels[i], titles[i],
		     "Signal fraction (3 strip cl.)",
		     0.5, 1.1);
    PlotVsRunLayerID("full2.catSigfrac[1]", selections[i],
		     "signalFrac_nstrip4_"+labels[i], titles[i],
		     "Signal fraction (4 strip cl.)",
		     0.5, 1.1);
    PlotVsRunLayerID("full2.catSigfrac[2]", selections[i],
		     "signalFrac_nstrip5_"+labels[i], titles[i],
		     "Signal fraction (5 strip cl.)",
		     0.5, 1.1);


  }


}

double PlotFitResult::GetRunLayerID(int run, int layer)
{
  for (int i=0; i<nRuns; i++)
    if (run==runList[i])
      return i + 0.2*layer;

  return -9.;
}

int PlotFitResult::GetRun(double runLayerID)
{
  return runList[int(runLayerID)];
}

int PlotFitResult::GetLayer(double runLayerID)
{
  return TMath::Nint( (runLayerID-int(runLayerID)) / 0.2 );
}

void PlotFitResult::PlotVsRunLayerID(TString var, TString selection,
				     TString name,
				     TString title, TString ytitle,
				     float min, float max, TString opt)
{
  c->SetGridx();
  c->SetGridy();
  gStyle->SetEndErrorSize(2.);

  TString xvar = "PlotFitResult::GetRunLayerID(run, layer)";
  
  //TString xvar = "2.2";

  TString xtitle = "Run, layer";

  TString fullexp = TString::Format(" %s : %s.value : 0. : %s.error",
				    xvar.Data(), var.Data(), var.Data());

  TGraphErrors* g = Plotter::GetGraphErrors(fullexp,
				   selection+"&&!(layer==1||run==323)");
  TGraphErrors* gExcl = Plotter::GetGraphErrors(fullexp,
				       selection+"&&(layer==1||run==323)");
  if (opt=="rel1") SetRelToLayer1(g);

  TMultiGraph mg;
  mg.Add(g, "PE");
  mg.Add(gExcl, "PE");

  mg.SetMinimum(min);
  mg.SetMaximum(max);
  g->SetMarkerSize(1.);
  gExcl->SetMarkerSize(1.);

  gExcl->SetMarkerStyle(24);

  if (name.Contains("alignOffsetRel1")) g->SetMarkerSize(1.);

  mg.Draw("A");
  //  g->Draw("APE");
  
  double mean = g->GetMean(2);
  double rms  = g->GetRMS (2);

  TBox band(0., mean-rms, nRuns, mean+rms);
  band.SetFillColor(kBlue);
  band.SetFillStyle(3004);
  band.Draw("same");

  TLine line(0., mean, nRuns, mean);
  line.SetLineColor(kBlue);
  line.SetLineWidth(2);
  line.Draw("same");

  mg.GetXaxis()->SetTitle(xtitle);
  mg.GetYaxis()->SetTitle(ytitle);

  //mg.GetXaxis()->SetLabelColor(1, 0.);
  mg.GetXaxis()->SetLimits(0., nRuns);
  
  //  TString m_labels[] = {"A", "B", "C", "D", "E", "F"};
  
  for (int i=0; i<nRuns; i++) {

    int run = runList[i];
    double ypos = min-0.1*(max-min);
    TString runLabel(runLabelList[i]);
    //    TString runLabel(m_labels[i]);
    if (!publicFormat) {
      runLabel.Form("%i", run);
      latex.DrawText(i+0.3, ypos, runLabel);
    }
    else latex.DrawText(i+0.4, ypos, runLabel);

    ypos = min-0.03*(max-min);
    latex.SetTextSize(0.025);
    for (int layer=1; layer<=4; layer++) {
      double xpos = GetRunLayerID(run, layer)-0.03;
      latex.DrawText(xpos, ypos, TString::Format("%i", layer));
    }
    latex.SetTextSize(0.05);
  }
    
  SetPlotTitle(title);
  SavePlot(name);

  delete g;
  delete gExcl;

  c->SetGridx(0);
  c->SetGridy(0);
  gStyle->SetEndErrorSize(0.);
}

void PlotFitResult::SetRelToLayer1(TGraphErrors* graph)
{
  TGraphErrors* aux = (TGraphErrors*)graph->Clone();

  for (int point=0; point < graph->GetN(); point++) {
    double x = -99999.;
    double y = -99999.;
    graph->GetPoint(point, x, y);
    int run = GetRun(x);
    double ref = GetValue(aux, run, 1);
    graph->SetPoint(point, x, y-ref);
  }

  delete aux;
}

double PlotFitResult::GetValue(TGraphErrors* graph, int run, int layer)
{
  for (int point=0; point < graph->GetN(); point++) {
    double x = -9.;
    double y = -9.;
    graph->GetPoint(point, x, y);
    if (run==GetRun(x) && layer==GetLayer(x)) return y;
  }

  std::cout << "\n\nWARNING:\tPlotFitResult::GetValue:\t"
       << "didn't find graph point for run " << run
	    << " layer " << layer << std::endl;
  return -99999.;
}
