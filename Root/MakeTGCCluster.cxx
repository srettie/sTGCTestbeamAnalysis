#define MakeTGCCluster_cxx
#include "sTGCTestbeamAnalysis/MakeTGCCluster.h"

//using namespace std;
//using namespace Mapping;
//using namespace TMath;

MakeTGCCluster::MakeTGCCluster(TString in, TString _out, int _run,
			       int pedestalOpt)
  : fChain(0), out(_out), run(_run), pedestals(pedestalOpt)
{
  tdomin_cut = 2300;
  tdomax_cut = 3300;

  pdomax_cut[0] = 5500;      // for layer 1
  pdomax_cut[1] = 999999999;
  pdomax_cut[2] = 999999999;
  pdomax_cut[3] = 999999999;

  //    chi2FitOpt = "";
  chi2FitOpt = "QW";
  //  chi2FitOpt = "WW";
  //  chi2FitOpt = "I";
  //  chi2FitOpt = "WI";

  TFile f(in);
  TTree* tree;
  f.GetObject("tgc_raw",tree);
  Init(tree);

  SetupOutput();
  Loop();

  f.Close();
}

MakeTGCCluster::~MakeTGCCluster()
{
  delete outtree;
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

void MakeTGCCluster::SetupOutput()
{
  outtree = new TTree("tgc_cluster", "");
  outtree->SetAutoSave(0);
  outtree->SetAutoFlush(0);
  outtree->SetDirectory(0);

  cl_raw_nstrip = 0;
  cl_raw_pdomin = 0;
  cl_raw_pdominneighbor = 0;
  cl_raw_pdomax = 0;
  cl_raw_pdomax2 = 0;
  cl_raw_tdomin = 0;
  cl_raw_tdomax = 0;
  cl_raw_ch_channel = 0;
  cl_raw_ch_pdo = 0;

  cl_y_chhmean = 0;
  cl_sigma_chhmean = 0;
  cl_yrelStrip_hmean = 0;
  cl_yrelGap_hmean = 0;
  cl_y_hmean = 0;

  cl_y_chfitChi2 = 0;
  cl_sigma_chfitChi2 = 0;
  cl_yrelStrip_fitChi2 = 0;
  cl_yrelGap_fitChi2 = 0;
  cl_y_fitChi2 = 0;

  cl_y_chfitLH = 0;
  cl_sigma_chfitLH = 0;
  cl_yrelStrip_fitLH = 0;
  cl_yrelGap_fitLH = 0;
  cl_y_fitLH = 0;

  cl_nstrip = 0;
  cl_pdomin = 0;
  cl_pdomax = 0;
  cl_loose = 0;
  cl_medium = 0;
  cl_tight = 0;

  cl_ch_channel = 0;
  cl_ch_pdo = 0;

  outtree->Branch("run", &run, "run/I");
  outtree->Branch("event", &preevent, "event/I");
  outtree->Branch("layer", &prelayer, "layer/I");
  outtree->Branch("globevent", &preglobevent, "globevent/I");
  outtree->Branch("nclusters", &nclusters, "nclusters/I");
  outtree->Branch("ngoodclusters", &ngoodclusters, "ngoodclusters/I");

  outtree->Branch("cl_raw_nstrip", &cl_raw_nstrip);
  outtree->Branch("cl_raw_pdomin", &cl_raw_pdomin);
  outtree->Branch("cl_raw_pdominneighbor", &cl_raw_pdominneighbor);
  outtree->Branch("cl_raw_pdomax", &cl_raw_pdomax);
  outtree->Branch("cl_raw_pdomax2", &cl_raw_pdomax2);
  outtree->Branch("cl_raw_tdomin", &cl_raw_tdomin);
  outtree->Branch("cl_raw_tdomax", &cl_raw_tdomax);
  outtree->Branch("cl_raw_ch_channel", &cl_raw_ch_channel);
  outtree->Branch("cl_raw_ch_pdo", &cl_raw_ch_pdo);

  outtree->Branch("cl_y_chhmean", &cl_y_chhmean);
  outtree->Branch("cl_sigma_chhmean", &cl_sigma_chhmean);
  outtree->Branch("cl_yrelStrip_hmean", &cl_yrelStrip_hmean);
  outtree->Branch("cl_yrelGap_hmean", &cl_yrelGap_hmean);
  outtree->Branch("cl_y_hmean", &cl_y_hmean);

  outtree->Branch("cl_y_chfitChi2", &cl_y_chfitChi2);
  outtree->Branch("cl_sigma_chfitChi2", &cl_sigma_chfitChi2);
  outtree->Branch("cl_yrelStrip_fitChi2", &cl_yrelStrip_fitChi2);
  outtree->Branch("cl_yrelGap_fitChi2", &cl_yrelGap_fitChi2);
  outtree->Branch("cl_y_fitChi2", &cl_y_fitChi2);

  outtree->Branch("cl_y_chfitLH", &cl_y_chfitLH);
  outtree->Branch("cl_sigma_chfitLH", &cl_sigma_chfitLH);
  outtree->Branch("cl_yrelStrip_fitLH", &cl_yrelStrip_fitLH);
  outtree->Branch("cl_yrelGap_fitLH", &cl_yrelGap_fitLH);
  outtree->Branch("cl_y_fitLH", &cl_y_fitLH);

  outtree->Branch("cl_nstrip", &cl_nstrip);
  outtree->Branch("cl_pdomin", &cl_pdomin);
  outtree->Branch("cl_pdomax", &cl_pdomax);
  outtree->Branch("cl_loose", &cl_loose);
  outtree->Branch("cl_medium", &cl_medium);
  outtree->Branch("cl_tight", &cl_tight);

  outtree->Branch("cl_ch_channel", &cl_ch_channel);
  outtree->Branch("cl_ch_pdo", &cl_ch_pdo);
}

void MakeTGCCluster::Loop()
{

  preevent     = -1;
  prelayer     = -1;
  preglobevent = -1;

  TH1D pdo_h("pdo_h", "", 64, -0.5, 63.5);
  TH1D tdo_h("tdo_h", "", 64, -0.5, 63.5);
  TCanvas c; // dummy canvas, it's used in the cluster fits
  
  bool firstentry = true;
  
  if (fChain == 0) return;
  
  Long64_t nentries = fChain->GetEntries();
  Long64_t nbytes = 0, nb = 0;

  TMVA::Timer timer((Int_t)nentries, "MakeTGCCluster");
  
  for (Long64_t jentry=0; jentry<=nentries;jentry++) {
    // don't load tree event for the last loop cycle
    if (jentry<nentries) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
    }
    
    //
    // WARNING !!!
    //
    // Only considering the "Module -1" strip layers
    // The "60x40" layers are being skipped here
    //
    if (layerip!=18 && layerip!=19 && layerip!=25 && layerip!=29) continue;

    int layer = Mapping::GetLayerNumber(run, layerip);
    
    // store this layer info and reset cache
    if (!firstentry && (layer!=prelayer || event!=preevent)) {

      timer.DrawProgressBar( (Int_t) jentry );
      
      SearchStoreClusters(prelayer, pdo_h, tdo_h);

      ////
      pdo_h.Reset();
      tdo_h.Reset();
      ////
      
      // after the last event, exit the loop here
      if (jentry==nentries) continue;
    }

    int m_channel = channel;
    if (layer==2 || layer==3) m_channel = 63-channel;
    pdo_h.Fill(m_channel, pdo);
    tdo_h.Fill(m_channel, tdo);
    
    preevent     = event;
    prelayer     = layer;
    preglobevent = globevent;
    firstentry = false;
  }

  std::cout << "MakeTGCCluster: elapsed time: " << timer.GetElapsedTime() << std::endl;

  TFile file(out, "recreate");
  outtree->Write();
  file.Close();
  outtree->Reset();
}

void MakeTGCCluster::SearchStoreClusters(int layer, TH1D& pdo_h, TH1D& tdo_h)
{
  nclusters = 0;
  ngoodclusters = 0;

  cl_raw_nstrip->clear();
  cl_raw_pdomin->clear();
  cl_raw_pdominneighbor->clear();
  cl_raw_pdomax->clear();
  cl_raw_pdomax2->clear();
  cl_raw_tdomin->clear();
  cl_raw_tdomax->clear();
  cl_raw_ch_channel->clear();
  cl_raw_ch_pdo->clear();

  cl_y_chhmean->clear();
  cl_sigma_chhmean->clear();
  cl_yrelStrip_hmean->clear();
  cl_yrelGap_hmean->clear();
  cl_y_hmean->clear();

  cl_y_chfitChi2->clear(); 
  cl_sigma_chfitChi2->clear(); 
  cl_yrelStrip_fitChi2->clear();
  cl_yrelGap_fitChi2->clear();
  cl_y_fitChi2->clear();

  cl_y_chfitLH->clear();
  cl_sigma_chfitLH->clear();
  cl_yrelStrip_fitLH->clear();
  cl_yrelGap_fitLH->clear();
  cl_y_fitLH->clear();

  cl_nstrip->clear();
  cl_pdomin->clear();
  cl_pdomax->clear();
  cl_loose->clear();
  cl_medium->clear();
  cl_tight->clear();
  
  cl_ch_channel->clear();
  cl_ch_pdo->clear();
  
  TH1D clpdo_h("clpdo_h", "", 64, -0.5, 63.5);
  TH1D cltdo_h("cltdo_h", "", 64, -0.5, 63.5);
  
  for (int bin=1; bin<=pdo_h.GetNbinsX()+1; bin++) {
    double _pdo   = pdo_h.GetBinContent(bin);
    double prepdo = pdo_h.GetBinContent(bin-1);
    double _tdo   = tdo_h.GetBinContent(bin);
    
    if (_pdo==0. && prepdo>0.) {
      StoreCluster(layer, clpdo_h, cltdo_h);
      clpdo_h.Reset();
      cltdo_h.Reset();
    }
    
    clpdo_h.SetBinContent(bin, _pdo);
    cltdo_h.SetBinContent(bin, _tdo);
  }
  
  outtree->Fill();
}

double MakeTGCCluster::GetMappedClY(double _y_ch, int _layer)
{
  double pitch;
  int ip = Mapping::GetLayerIP(run, _layer);
  TString ipaddress = TString::Format("10.0.0.%i", ip);
  int ch0 = 0;
  if (_layer==2 || _layer==3) ch0 = 63;
  
  int strip0  = Mapping::ChannelToStrip(ch0 , ipaddress.Data());
  double strip0_y = Mapping::PositionStrip(_layer, strip0 , pitch);
  
  double tablepos = 435.1;
  if (run==321) tablepos -= 404.2;
  if (run==323) tablepos -= 360.6;
  if (run==325) tablepos -= 340.2;
  if (run==326) tablepos -= 328.2;
  if (run==334) tablepos -= 328.2;
  if (run==335) tablepos -= 328.2;
  tablepos += 573.; // distance between table (at 435.1) and telescophe

  strip0_y -= tablepos;

  double _y = _y_ch*pitch + strip0_y;
  
  return _y;
}

double MakeTGCCluster::GetMappedClX(int m_run)
{ 
  double tablepos = 2262;
 
  if (m_run==321) tablepos -= 2183;
  if (m_run==323) tablepos -= 2183;
  if (m_run==325) tablepos -= 2183;
  if (m_run==326) tablepos -= 2183;
  if (m_run==334) tablepos -= 2354;
  if (m_run==335) tablepos -= 2354;

  //  tablepos += 365.; // distance between table (at 365.) and telescophe
  tablepos -= 365.; // distance between table (at 365.) and telescophe
  
  return tablepos;
}

void MakeTGCCluster::StoreCluster(int layer, TH1D& pdo_h, TH1D& tdo_h)
{
  // store the raw info  
  int raw_nstrip = 0;
  std::vector<int> raw_ch_channel;
  std::vector<int> raw_ch_pdo;
  for (int bin=1; bin<=pdo_h.GetNbinsX(); bin++) {

    int ch  = pdo_h.GetBinCenter(bin);
    int val = pdo_h.GetBinContent(bin);

    if (val>0) {
      raw_nstrip++;
      raw_ch_channel.push_back(ch);
      raw_ch_pdo    .push_back(val);
    }
  }
  
  // quality checks  
  int raw_pdomin  = pdo_h.GetMinimum(0.);
  int raw_pdomax  = pdo_h.GetMaximum();
  int raw_pdomax2 = 0;
  
  int raw_tdomin = tdo_h.GetMinimum(0.);
  int raw_tdomax = tdo_h.GetMaximum();
  
  bool _loose  = IsGoodLoose(run, layer, raw_tdomin, raw_tdomax, raw_pdomax);
  bool _medium = IsGoodMedium(_loose, raw_nstrip, raw_ch_pdo);
  
  int raw_pdominneighbor = -9999;
  if (raw_nstrip==2) raw_pdominneighbor = raw_pdomax;
  if (raw_nstrip>2) {
    int raw_pdominbin = pdo_h.GetMaximumBin(); // start with the max
    for (int bin=1; bin<=pdo_h.GetNbinsX(); bin++) {
      int val    = pdo_h.GetBinContent(bin);
      int minval = pdo_h.GetBinContent(raw_pdominbin);
      if (val>0. && val<minval) raw_pdominbin = bin;
      if (val>raw_pdomax2 && val<raw_pdomax) raw_pdomax2 = val;
    }
    int val1 = pdo_h.GetBinContent(raw_pdominbin-1);
    int val2 = pdo_h.GetBinContent(raw_pdominbin+1);
    if (val1>=raw_pdomin && val2>=raw_pdomin) {
      //       if (_good && prelayerip!=25 && prelayerip!=18) {
      // 	cout << "\n\nERROR:\tMakeTGCCluster::StoreCluster:\t"
      //    << "Strange cluster with min-PDO strip in the middle"
      // 	     << ", event " << preevent
      // 	     << ", layerip " << prelayerip
      // 	     << ", raw_pdominbin " << raw_pdominbin
      // 	     << ", raw_pdomin " << raw_pdomin
      // 	     << ", val1 " << val1
      // 	     << ", val2 " << val2
      // 	     << "\n\n\n";
      // 	pdo_h.Print("all");
      // 	exit(1);
      //       }
      //      else
      raw_pdominneighbor = TMath::Min(val1, val2);
    }
    else if (val1>=raw_pdomin) raw_pdominneighbor = val1;
    else if (val2>=raw_pdomin) raw_pdominneighbor = val2;
    else {
      std::cout << "\n\nERROR:\tMakeTGCCluster::StoreCluster:\t"
	   << "Bug when looking for raw_pdominneighbor"
	   << ", event " << preevent
	   << ", layer " << layer
	   << ", raw_pdominbin " << raw_pdominbin
	   << ", raw_pdomin " << raw_pdomin
	   << ", val1 " << val1
	   << ", val2 " << val2
	   << "\n\n\n";
      exit(1);
    }
  }


  // do pedestal subtraction
  int _nstrip = 0;
  std::vector<int> _ch_channel;
  std::vector<int> _ch_pdo;
  for (int bin=1; bin<=pdo_h.GetNbinsX(); bin++) {
    
    int ch      = pdo_h.GetBinCenter(bin);
    int raw_val = pdo_h.GetBinContent(bin);
    
    int val = raw_val - pedestals.Get(layer, ch);
    if (val<0) val = 0;
    
    double error = TMath::Sqrt(val);
    //    if (raw_val>4250) error = 0.3*val;
    //    if (raw_val>4250) error = 9.e9;
    //    if (raw_val>3500) error = 9.e9;

    pdo_h.SetBinContent(bin, val);
    pdo_h.SetBinError(bin, error);

    if (val>0) {
      _nstrip++;
      _ch_channel.push_back(ch);
      _ch_pdo.push_back(val);
    }
  }
  int _pdomin = pdo_h.GetMinimum(0.);
  int _pdomax = pdo_h.GetMaximum();

  bool _tight  = _loose && IsGoodTight(_medium, _nstrip, _ch_pdo);
  
  double _y_chhmean   = -9.e20;
  double _y_chfitChi2 = -9.e20;
  double _y_chfitLH   = -9.e20;

  double _sigma_chhmean   = -9.e20;
  double _sigma_chfitChi2 = -9.e20;
  double _sigma_chfitLH   = -9.e20;

  if (_tight) {
    _y_chhmean = pdo_h.GetMean();
    _sigma_chhmean = pdo_h.GetRMS();

    TF1 genGaus("genGaus",
		"[0]*exp(-0.5*(TMath::Abs(x-[1])/[2])**[3])", -0.5, 63.5);
    genGaus.SetParameter(0, 2000.);
    genGaus.SetParameter(1, pdo_h.GetMean());
    genGaus.SetParameter(2, pdo_h.GetRMS());
    genGaus.SetParameter(3, 2.);

    TString fname = "gaus";
    //    if (_nstrip>=4) fname = "genGaus";
    
    //    TFitResultPtr fitChi2 = pdo_h.Fit(fname, "QS"+chi2FitOpt);
    TFitResultPtr fitChi2 = pdo_h.Fit(fname, "S"+chi2FitOpt);
    if (fitChi2.Get()) {
      _y_chfitChi2     = fitChi2->Parameter(1);
      _sigma_chfitChi2 = fitChi2->Parameter(2);
    }


    // likelihood gaussian fit

    TFitResultPtr fitLH   = pdo_h.Fit("gaus", "QSL");
    if (fitLH.Get()) {
      _y_chfitLH     = fitLH->Parameter(1);
      _sigma_chfitLH = fitLH->Parameter(2);
    }
  }
  
  double _yrelStrip_hmean   = GetYRelToStripCenter(_y_chhmean  );
  double _yrelStrip_fitChi2 = GetYRelToStripCenter(_y_chfitChi2);
  double _yrelStrip_fitLH   = GetYRelToStripCenter(_y_chfitLH  );

  double _yrelGap_hmean     = GetYRelToGapCenter(_y_chhmean  );
  double _yrelGap_fitChi2   = GetYRelToGapCenter(_y_chfitChi2);
  double _yrelGap_fitLH     = GetYRelToGapCenter(_y_chfitLH  );
  
  double _y_hmean   = GetMappedClY(_y_chhmean  , layer);
  double _y_fitChi2 = GetMappedClY(_y_chfitChi2, layer);
  double _y_fitLH   = GetMappedClY(_y_chfitLH  , layer);

  nclusters++;
  if (_tight) ngoodclusters++;

  cl_raw_nstrip->push_back(raw_nstrip);
  cl_raw_pdomin->push_back(raw_pdomin);
  cl_raw_pdominneighbor->push_back(raw_pdominneighbor);
  cl_raw_pdomax->push_back(raw_pdomax);
  cl_raw_pdomax2->push_back(raw_pdomax2);
  cl_raw_tdomin->push_back(raw_tdomin);
  cl_raw_tdomax->push_back(raw_tdomax);
  cl_raw_ch_channel->push_back(raw_ch_channel);
  cl_raw_ch_pdo->push_back(raw_ch_pdo);

  cl_y_chhmean  ->push_back(_y_chhmean);
  cl_y_chfitChi2->push_back(_y_chfitChi2);
  cl_y_chfitLH  ->push_back(_y_chfitLH);

  cl_sigma_chhmean  ->push_back(_sigma_chhmean);
  cl_sigma_chfitChi2->push_back(_sigma_chfitChi2);
  cl_sigma_chfitLH  ->push_back(_sigma_chfitLH);

  cl_yrelStrip_hmean  ->push_back(_yrelStrip_hmean);
  cl_yrelStrip_fitChi2->push_back(_yrelStrip_fitChi2);
  cl_yrelStrip_fitLH  ->push_back(_yrelStrip_fitLH);

  cl_yrelGap_hmean  ->push_back(_yrelGap_hmean);
  cl_yrelGap_fitChi2->push_back(_yrelGap_fitChi2);
  cl_yrelGap_fitLH  ->push_back(_yrelGap_fitLH);

  cl_y_hmean  ->push_back(_y_hmean);
  cl_y_fitChi2->push_back(_y_fitChi2);
  cl_y_fitLH  ->push_back(_y_fitLH);

  cl_nstrip->push_back(_nstrip);
  cl_pdomin->push_back(_pdomin);
  cl_pdomax->push_back(_pdomax);

  cl_loose ->push_back(_loose);
  cl_medium->push_back(_medium);
  cl_tight ->push_back(_tight);

  cl_ch_channel->push_back(_ch_channel);
  cl_ch_pdo->push_back(_ch_pdo);
}

bool MakeTGCCluster::IsGoodLoose(int _run, int _layer,
				 int tdomin, int tdomax, int pdomax)
{
  _run = _run; // dummy line to avoid warnings
  if (tdomin<tdomin_cut) return false;
  if (tdomax>tdomax_cut) return false;
  if (pdomax>pdomax_cut[_layer-1]) return false;
  return true;
}

bool MakeTGCCluster::IsGoodMedium(bool loose, int nstrips, std::vector<int> _pdo)
{
  if (nstrips!=(int)_pdo.size()) {
    std::cout << "\n\nERROR:\tMakeTGCCluster::IsGoodMedium:\t"
	      << "nstrips!=_pdo.size()" << std::endl;
    exit(1);
  }
  
  if (!loose) return false;
  
  if (nstrips<3) return false;
  
  // each border below its neighbor
  int pdo_1st = _pdo[0];
  int pdo_2nd = _pdo[1];
  int pdo_nextlast = _pdo[nstrips-2];
  int pdo_last     = _pdo[nstrips-1];
  if (pdo_1st  > pdo_2nd     ) return false;
  if (pdo_last > pdo_nextlast) return false;
  
  return true;
}

bool MakeTGCCluster::IsGoodTight(bool medium, int nstrips, std::vector<int> _pdo)
{
  if (nstrips!=(int)_pdo.size()) {
    std::cout << "\n\nERROR:\tMakeTGCCluster::IsGoodTight:\t"
	      << "nstrips!=_pdo.size()" << std::endl;
    exit(1);
  }
  
  if (!medium) return false;
  
  if (nstrips<3) return false;
  
  for (int i=1; i<nstrips-1; i++) {
    if (_pdo[i] < TMath::Min(_pdo[i-1], _pdo[i+1]) ) return false;
  }
  
  return true;
}

double MakeTGCCluster::YRelConvert(double yrel)
{
  return yrel - TMath::Sign(0.5, yrel);
}

double MakeTGCCluster::GetYRelToStripCenter(double y_pitchUnits)
{
  return y_pitchUnits - TMath::Nint(y_pitchUnits);
}

double MakeTGCCluster::GetYRelToGapCenter(double y_pitchUnits)
{
  return YRelConvert( GetYRelToStripCenter(y_pitchUnits) );
}

Int_t MakeTGCCluster::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}

Long64_t MakeTGCCluster::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void MakeTGCCluster::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("event", &event, &b_event);
  fChain->SetBranchAddress("channel", &channel, &b_channel);
  fChain->SetBranchAddress("tdo", &tdo, &b_tdo);
  fChain->SetBranchAddress("pdo", &pdo, &b_pdo);
  fChain->SetBranchAddress("layerip", &layerip, &b_layerip);
  fChain->SetBranchAddress("globevent", &globevent, &b_globevent);
  Notify();
}

Bool_t MakeTGCCluster::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void MakeTGCCluster::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}

