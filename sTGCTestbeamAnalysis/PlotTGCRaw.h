#ifndef PlotTGCRaw_h
#define PlotTGCRaw_h

#include <iostream>
#include <vector>

#include "sTGCTestbeamAnalysis/Mapping.h"
#include "sTGCTestbeamAnalysis/Plotter.h"

class PlotTGCRaw: public Plotter {
 public :
  PlotTGCRaw(TString _in, TString _out, TString treename, int run);

};
#endif
