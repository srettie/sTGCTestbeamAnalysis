#ifndef Pedestals_h
#define Pedestals_h

#include <TROOT.h>

class Pedestals {
  
 public:
  
  enum PedestalOpt{ oldGlobal=0,
		    global=-1,
		    perChannel=-2,
		    fitted=-3};
  
  static int OldGlobal () { return oldGlobal ; };
  static int Global    () { return global    ; };
  static int PerChannel() { return perChannel; };
  static int Fitted    () { return fitted; };
  
 private:
  
  int option;

  int perChannelLayer2[64];
  int perChannelLayer3[64];
  int perChannelLayer3Gain1[64];
  int perChannelLayer4[64];

  int fittedLayer4[64];

  int globalLayer1;
  int globalLayer2;
  int globalLayer3;
  int globalLayer3Gain1;
  int globalLayer4;

  int oldGlobalLayer1;
  int oldGlobalLayer2;
  int oldGlobalLayer3;
  int oldGlobalLayer4;

 public:

  Pedestals(int _option);
  int Get(int layer, int ch=-1);
  int GetPerChannel(int layer, int ch);
  int GetFitted(int layer, int ch);
  int GetLayer3Gain1PerChannel(int ch);
  int GetGlobal(int layer);
  int GetOldGlobal(int layer);
  void Plot(TString out);
};

#endif
