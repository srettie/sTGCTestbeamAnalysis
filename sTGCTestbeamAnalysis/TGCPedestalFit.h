#ifndef TGCPedestalFit_h
#define TGCPedestalFit_h

#include <vector>
#include <iostream>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TF2.h>
#include <TMath.h>
#include <TFitResult.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGraphErrors.h>
#include <TMVA/Timer.h>

#include "sTGCTestbeamAnalysis/TGCCluster.h"
#include "sTGCTestbeamAnalysis/Tools.h"


//using namespace std;
//using namespace TMath;

namespace PedestalFit {

  struct EventInfo {
    int num;
    int index_norm;
    int index_mean;
    int index_sigma;
    Parameter norm;
    Parameter mean;
    Parameter sigma;
    std::vector<int> ch_channel;
    std::vector<int> ch_pdo;

    EventInfo() {
      num = -1;
      int index_norm  = -1;
      int index_mean  = -1;
      int index_sigma = -1;
    };
    EventInfo(int number) {
      num = number;
      int index_norm  = -1;
      int index_mean  = -1;
      int index_sigma = -1;
    };
  };

  static std::vector<int> channelList;
  static std::vector<int> channelListLowStat;
  static int nPedestals = 0;
  static int nGausPars  = 0;
  static int nTotPars   = 0;
  
  double PedestalF(double* x, double* par);
  
  double MultiGausF(double* x, double* par);

  double FullModel(double* x, double* par);
}

using namespace PedestalFit;

class TGCPedestalFit {
 public :

  TGCCluster* cluster;

  TString out;
  int run;
  int layer;
  int maxNTotCluster;
  int nClusterPerFit;
  int minEntriesPerChannel;

  bool printINFO;

  TH1D* fitres[64];

  TTree* outtree;
  
  TGCPedestalFit(TString in, TString _out, int _run, int _layer);
  ~TGCPedestalFit();
  void SetupOutput();
  void Execute();
  std::vector<EventInfo>* GetEventList();
  TH2D* GetHisto(std::vector<EventInfo>& eventList);
  void SetupPedestalF(std::vector<EventInfo>& eventList);
  TF2* GetFunction(std::vector<EventInfo>& eventList);
  void FillResultHistos(TFitResultPtr res);
  void SaveResult();
  void SaveResult(TFitResultPtr res);
};

#endif
