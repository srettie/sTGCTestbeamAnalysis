#ifndef MakePixelTrack_h
#define MakePixelTrack_h


#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TGraph.h>
#include <TF1.h>
#include <TMath.h>

#include "sTGCTestbeamAnalysis/Mapping.h"

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class MakePixelTrack {
 public :
  
  TString out;

  int cache_nHitXLayer[6];
  std::vector<int> cache_layer;
  std::vector<double> cache_x;
  std::vector<double> cache_y;
  std::vector<double> cache_trk_chi2;
  
  // output
  TTree* outtree;
  int event_out;
  int ntracks;
  int nhits;
  std::vector<int>* missinglayer;
  double x0;
  double x0_error;
  double mx;
  double mx_error;
  std::vector<double>* dxi;
  double y0;
  double y0_error;
  double my;
  double my_error;
  std::vector<double>* dyi;
  double chi2;
  double prob;

  // input

  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  Int_t           event;
  Char_t          p[2];
  Int_t           layer;
  Double_t        y;
  Double_t        x;
  Double_t        trk_chi2;

  // List of branches
  TBranch        *b_event;   //!
  TBranch        *b_p;   //!
  TBranch        *b_layer;   //!
  TBranch        *b_y;   //!
  TBranch        *b_x;   //!
  TBranch        *b_trk_chi2;   //!

  MakePixelTrack(TString in, TString _out);
  ~MakePixelTrack();  
  void SetupOutput();
  void WriteOutput();
  void Loop();
  void StoreEvent(int evtnumber);
  void FillCache();
  void CleanCache();
  void CleanStorage();
  
  //  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MakePixelTrack_cxx

Int_t MakePixelTrack::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t MakePixelTrack::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void MakePixelTrack::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("event", &event, &b_event);
  fChain->SetBranchAddress("p", p, &b_p);
  fChain->SetBranchAddress("layer", &layer, &b_layer);
  fChain->SetBranchAddress("y", &y, &b_y);
  fChain->SetBranchAddress("x", &x, &b_x);
  fChain->SetBranchAddress("trk_chi2", &trk_chi2, &b_trk_chi2);
  Notify();
}

Bool_t MakePixelTrack::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void MakePixelTrack::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
/* Int_t MakePixelTrack::Cut(Long64_t entry) */
/* { */
/*   // This function may be called from Loop. */
/*   // returns  1 if entry is accepted. */
/*   // returns -1 otherwise. */
/*   return 1; */
/* } */
#endif // #ifdef MakePixelTrack_cxx
