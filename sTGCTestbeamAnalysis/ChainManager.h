#ifndef ChainManager_h
#define ChainManager_h

#include <vector>
#include <iostream>

#include "sTGCTestbeamAnalysis/TxtToRoot.h"
#include "sTGCTestbeamAnalysis/PlotTGCRaw.h"
#include "sTGCTestbeamAnalysis/Pedestals.h"
#include "sTGCTestbeamAnalysis/MakeTGCCluster.h"
#include "sTGCTestbeamAnalysis/PlotTGCCluster.h"
#include "sTGCTestbeamAnalysis/TGCPedestalFit.h"
#include "sTGCTestbeamAnalysis/MakePixelTrack.h"
#include "sTGCTestbeamAnalysis/MakeCombEvent.h"
#include "sTGCTestbeamAnalysis/PlotCombEvent.h"
#include "sTGCTestbeamAnalysis/ResidualFit.h"
#include "sTGCTestbeamAnalysis/PlotFinalData.h"
#include "sTGCTestbeamAnalysis/EfficiencyFit.h"
#include "sTGCTestbeamAnalysis/PlotFitResult.h"


struct RunInput {
  int num;
  TString fileTGCRawTxt;
  TString filePixelRawTxt;
  
  RunInput(int _num, TString _fileTGCRawTxt, TString _filePixelRawTxt)
  {
    num     = _num;
    filePixelRawTxt = _filePixelRawTxt;
    fileTGCRawTxt   = _fileTGCRawTxt;
  };
};

struct Options {
  // TGC chain
  bool doMakeTGCRaw;
  bool doPlotTGCRaw;
  bool doPlotTGCPedestals;
  bool doMakeTGCCluster;
  bool doPlotTGCCluster;
  bool doTGCPedestalFit;
  // pixel chain
  bool doMakePixelRaw;
  bool doMakePixelTrack;
  // combined
  bool doMakeCombEvent;
  bool doPlotCombEvent;
  bool doResidualFit;
  bool doMergeFinalData;
  bool doPlotFinalData;
  bool doMergeFitResult;
  bool doPlotFitResult;
  
  Options()
  {
    doMakeTGCRaw       = false;
    doPlotTGCRaw       = false;
    doPlotTGCPedestals = false;
    doMakeTGCCluster   = false;
    doPlotTGCCluster   = false;
    doTGCPedestalFit   = false;
    doMakePixelRaw     = false;
    doMakePixelTrack   = false;
    doMakeCombEvent    = false;
    doPlotCombEvent    = false;
    doResidualFit      = false;
    doMergeFinalData   = false;
    doPlotFinalData    = false;
    doMergeFitResult   = false;
    doPlotFitResult    = false;
  };
};

class ChainManager{
 public:
  ChainManager(TString name, vector<RunInput*> runList, Options opt, TString descriptorTGCRawTxt, TString descriptorPixelRawTxt, vector<int>& layerList, vector<int>& pedestalList);
 ~ChainManager();

};
#endif
