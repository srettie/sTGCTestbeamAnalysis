#ifndef FitResult_h
#define FitResult_h

#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>

#include "sTGCTestbeamAnalysis/Tools.h"
#include "sTGCTestbeamAnalysis/Parameter.h"

class FitResult: public TObject {
 public:
  int nevtsTot;
  int status;
  int covqual;
  Parameter offset;
  Parameter phi;
  Parameter sigma;
  Parameter wideSigma;
  Parameter goodFrac;
  std::vector<Parameter> catAmpl;
  std::vector<Parameter> catSigma;
  std::vector<Parameter> catSigfrac;
  std::vector<Parameter> catNevts;
  std::vector<int> catNevtsTot;
  
  FitResult();
  ~FitResult();
  
  ClassDef(FitResult,1);
};
#endif
