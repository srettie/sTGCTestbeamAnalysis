#ifndef Tools_h
#define Tools_h

#include <iostream>

#include <TString.h>
#include <TMath.h>
#include <TChain.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TObjString.h>

#include "sTGCTestbeamAnalysis/Parameter.h"

struct AsymErr{
  double u;
  double d;
  AsymErr() : u(0.), d(0.) {};
};

struct NEvt{
  double val;
  double err;
  double relErr;
  NEvt() : val(0.), err(0.), relErr(0.) {};
  NEvt( double _val, double _err)
    : val(_val), err(_err), relErr(0.) {
    if (val!=0.) relErr = err/val;
  };
  void Set(double _val, double _err) {
    val = _val;
    err = _err;
    if (val!=0.) relErr = err/val;
  };
};

namespace Tools{

NEvt GetNEvt(TChain& chain, TString selection, TString wname="w");

int Magnitude(double x);

int NDecimals(double x, int pre=2);

TString PrintVal(double val, double err, int pre=2);

TString Print(double x, int pre=2);

TString PrintErr(double err, int pre=2);

TString Print(double val, double err, int pre=2);

TString PrintRawAndClean(float val, float err, int pre=2);

std::vector<TString> StrVec(TString str, TString delim=";");

int PedestalLayer3(int ch);

int PedestalLayer4(int ch);
 
int GetRefinedPedestal(int layer, int ch);
}
#endif
