#ifndef FitTools_h
#define FitTools_h

#include <iostream>

#include <TString.h>
#include <TMath.h>
#include <TChain.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TMatrixDSym.h>

#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooGaussian.h>
#include <RooUniform.h>
#include <RooAddPdf.h>
#include <RooFitResult.h>

#include "sTGCTestbeamAnalysis/Tools.h"
#include "sTGCTestbeamAnalysis/Plotter.h"

using namespace std;
using namespace TMath;
using namespace RooFit;

struct GausFitRes {
  
  Parameter mean;
  Parameter sigma;
  Parameter sigfrac;
  int nevts;
  int status;
  int covqual;
  
  GausFitRes()
  {
    mean   .value = -9999.;
    mean   .error = 0.;
    sigma  .value = -9999.;
    sigma  .error = 0.;
    sigfrac.value = -9999.;
    sigfrac.error = 0.;
    
    nevts = -9;
    status = -9;
    covqual = -9;
  };    
};


namespace FitTools{
  
  RooDataSet* SelectData(RooDataSet& fulldata,RooRealVar& var, double min, double max);

  GausFitRes GausFit(TString name, TString title,RooRealVar& var, double min, double max,RooDataSet& fulldata, Plotter* plotter=NULL,int plotNBins=50, double plotmin=0., double plotmax=0., bool quoteMean=false);

  double EffModel(double* x, double* par);
}

#endif
