#ifndef PlotCombEvent_h
#define PlotCombEvent_h

#include "Plotter.h"

class PlotCombEvent: public Plotter {
 public :
  
  PlotCombEvent(TString _in, TString _out, TString treename, int run, int layer);
};
#endif
