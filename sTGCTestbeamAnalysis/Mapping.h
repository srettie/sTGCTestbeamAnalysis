#ifndef Mapping_h
#define Mapping_h

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <TROOT.h>


namespace Mapping {

  // Global variabales
  const double Aactive = 691.8;
  const double Bactive = 1040.4;
  const double OpenAngleDegree = 0.5*17;
  const double H1active = 2246.8;
  const double H2active = 3413.2;
  const double Achamber = 741.1;
  const double H1chamber = 2235.8;
  const double Apads = 677.6;
  const double Bpads = 1026.3;

  // For Strips
  const double WidthStrip = 3.2;
  const int NbStrips = 365;

  // For Wires
  const double GroupWidth = 20;
  const double Pitch = 1.8;
  const double NbGroup[4]={29,30,29,29};
  const double SizeFirstGroup[4]={20,5,10,15};
  const double SizeLastGroup[4]={9,5,20,15};
  const double LeftPositionWires[4]={-511.2,-511.7,-512.6,-512.1};
  const double RightPositionWires[4]={511.2,512.6,511.7,512.1};

  // For Pads
  const double APads = 677.6;
  const double AngleFirstColumnDegree = 3.75;
  const int NbRaw = 15;
  const int NbColumn[4]={2,2,3,3};
  const double ExtraShift[4]={2.0,-2.0,2.0,-2.0};
  const double PadHeight = 80;
  const double FirstRawHeight[4]={93.2,93.2,60.2,60.2};
  const double LastRawHeight[4]={33.2,33.2,63.0,63.0};

  
  enum LAYERTYPE { STRIPLAYER=0, PADLAYER};

  // Pixel layers
  //  set the z=0 at the middle of the 6 pixel planes
  const double pixellayer_z[6] = { 0.      - 577.205,
				   132.52  - 577.205,
				   274.07  - 577.205,
				   914.07  - 577.205,
				   1032.46 - 577.205,
				   1154.41 - 577.205};
  

  double GetPixelLayerZ(int layer);
  int GetLayerIP(int run, int layer, LAYERTYPE type=STRIPLAYER);
  int GetLayerNumber(int run, int layerip, Mapping::LAYERTYPE type=STRIPLAYER);
  TString GetLayerName(int layer, LAYERTYPE type=STRIPLAYER);

  double PositionStrip(int, int, double&);
  //  int ChannelToStrip(int, string, bool&);
  int ChannelToStrip(int, std::string);
  void CardMapping(std::string, int&, int&, int&, int&, int&, int&, int&);
}
#endif
