//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Oct  9 17:03:59 2014 by ROOT version 5.34/21
// from TTree pixel_trk/
// found on file: output/test1006_v00/pixel_trk/pixel_trk_run323.root
//////////////////////////////////////////////////////////

#ifndef PixelTrack_h
#define PixelTrack_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class PixelTrack {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           event;
   Int_t           ntracks;
   Int_t           nhits;
   vector<int>     *missinglayer;
   Double_t        x0;
   Double_t        x0_error;
   Double_t        mx;
   Double_t        mx_error;
   vector<double>  *dxi;
   Double_t        y0;
   Double_t        y0_error;
   Double_t        my;
   Double_t        my_error;
   vector<double>  *dyi;
   Double_t        chi2;
   Double_t        prob;

   // List of branches
   TBranch        *b_event;   //!
   TBranch        *b_ntracks;   //!
   TBranch        *b_nhits;   //!
   TBranch        *b_missinglayer;   //!
   TBranch        *b_x0;   //!
   TBranch        *b_x0_error;   //!
   TBranch        *b_mx;   //!
   TBranch        *b_mx_error;   //!
   TBranch        *b_dxi;   //!
   TBranch        *b_y0;   //!
   TBranch        *b_y0_error;   //!
   TBranch        *b_my;   //!
   TBranch        *b_my_error;   //!
   TBranch        *b_dyi;   //!
   TBranch        *b_chi2;   //!
   TBranch        *b_prob;   //!

   PixelTrack(TTree *tree=0);
   virtual ~PixelTrack();
   //   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef PixelTrack_cxx
PixelTrack::PixelTrack(TTree *tree) : fChain(0) 
{
  /* // if parameter tree is not specified (or zero), connect the file */
  /* // used to generate this class and read the Tree. */
  /*    if (tree == 0) { */
  /*       TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("output/test1006_v00/pixel_trk/pixel_trk_run323.root"); */
  /*       if (!f || !f->IsOpen()) { */
  /*          f = new TFile("output/test1006_v00/pixel_trk/pixel_trk_run323.root"); */
  /*       } */
  /*       f->GetObject("pixel_trk",tree); */
  
  /*    } */
   Init(tree);
}

PixelTrack::~PixelTrack()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t PixelTrack::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t PixelTrack::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void PixelTrack::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   missinglayer = 0;
   dxi = 0;
   dyi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("ntracks", &ntracks, &b_ntracks);
   fChain->SetBranchAddress("nhits", &nhits, &b_nhits);
   fChain->SetBranchAddress("missinglayer", &missinglayer, &b_missinglayer);
   fChain->SetBranchAddress("x0", &x0, &b_x0);
   fChain->SetBranchAddress("x0_error", &x0_error, &b_x0_error);
   fChain->SetBranchAddress("mx", &mx, &b_mx);
   fChain->SetBranchAddress("mx_error", &mx_error, &b_mx_error);
   fChain->SetBranchAddress("dxi", &dxi, &b_dxi);
   fChain->SetBranchAddress("y0", &y0, &b_y0);
   fChain->SetBranchAddress("y0_error", &y0_error, &b_y0_error);
   fChain->SetBranchAddress("my", &my, &b_my);
   fChain->SetBranchAddress("my_error", &my_error, &b_my_error);
   fChain->SetBranchAddress("dyi", &dyi, &b_dyi);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   fChain->SetBranchAddress("prob", &prob, &b_prob);
   Notify();
}

Bool_t PixelTrack::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void PixelTrack::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
/* Int_t PixelTrack::Cut(Long64_t entry) */
/* { */
/* // This function may be called from Loop. */
/* // returns  1 if entry is accepted. */
/* // returns -1 otherwise. */
/*    return 1; */
/* } */
#endif // #ifdef PixelTrack_cxx
