#ifndef PlotFitResult_h
#define PlotFitResult_h

#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TAxis.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TMath.h>
#include <TLine.h>
#include <TBox.h>

#include "sTGCTestbeamAnalysis/Pedestals.h"
#include "sTGCTestbeamAnalysis/Plotter.h"

class PlotFitResult: public Plotter {
 public :
  
  const static int nRuns;
  const static int runList[];
  const static char runLabelList[];
  
  PlotFitResult(TString _in, TString _out);
  double GetRunLayerID(int run, int layer);
  int GetRun(double runLayerID);
  int GetLayer(double runLayerID);
  void PlotVsRunLayerID(TString var, TString selection,
			TString name,
			TString title, TString ytitle,
			float min, float max, TString opt="");
  void SetRelToLayer1(TGraphErrors* graph);
  double GetValue(TGraphErrors* graph, int run, int layer);
};
#endif
