#ifndef MakeTGCCluster_h
#define MakeTGCCluster_h

#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TFitResult.h>
#include <TMVA/Timer.h>
#include <TF1.h>

#include "sTGCTestbeamAnalysis/Mapping.h"
#include "sTGCTestbeamAnalysis/Tools.h"
#include "sTGCTestbeamAnalysis/Pedestals.h"

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class MakeTGCCluster {
 public :
  
  TString out;
  int run;
  
  int tdomin_cut;
  int tdomax_cut;
  int pdomax_cut[4]; // for each layer
  
  Pedestals pedestals;
  TString chi2FitOpt;
  
  TTree* outtree;

  int preevent;
  int prelayer;
  int preglobevent;

  int nclusters;
  int ngoodclusters;

  std::vector<int>* cl_raw_nstrip;
  std::vector<int>* cl_raw_pdomin;
  std::vector<int>* cl_raw_pdominneighbor;
  std::vector<int>* cl_raw_pdomax;
  std::vector<int>* cl_raw_pdomax2;
  std::vector<int>* cl_raw_tdomin;
  std::vector<int>* cl_raw_tdomax;
  std::vector<std::vector<int> >* cl_raw_ch_channel;
  std::vector<std::vector<int> >* cl_raw_ch_pdo;

  std::vector<double>* cl_y_chhmean;
  std::vector<double>* cl_sigma_chhmean;
  std::vector<double>* cl_yrelStrip_hmean;
  std::vector<double>* cl_yrelGap_hmean;
  std::vector<double>* cl_y_hmean;

  std::vector<double>* cl_y_chfitChi2;
  std::vector<double>* cl_sigma_chfitChi2;
  std::vector<double>* cl_yrelStrip_fitChi2;
  std::vector<double>* cl_yrelGap_fitChi2;
  std::vector<double>* cl_y_fitChi2;

  std::vector<double>* cl_y_chfitLH;
  std::vector<double>* cl_sigma_chfitLH;
  std::vector<double>* cl_yrelStrip_fitLH;
  std::vector<double>* cl_yrelGap_fitLH;
  std::vector<double>* cl_y_fitLH;

  /*   vector<double>* cl_mean; */
  /*   vector<double>* cl_mean_mapped; */
  /*   vector<double>* cl_mean_fitChi2; */
  /*   vector<double>* cl_mean_fitLH; */
  std::vector<int>* cl_nstrip;
  std::vector<int>* cl_pdomin;
  std::vector<int>* cl_pdomax;
  std::vector<bool>* cl_loose;
  std::vector<bool>* cl_medium;
  std::vector<bool>* cl_tight;
  std::vector<std::vector<int> >* cl_ch_channel;
  std::vector<std::vector<int> >* cl_ch_pdo;


  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  Int_t           event;
  Int_t           channel;
  Int_t           tdo;
  Int_t           pdo;
  Int_t           layerip;
  Int_t           globevent;

  // List of branches
  TBranch        *b_event;   //!
  TBranch        *b_channel;   //!
  TBranch        *b_tdo;   //!
  TBranch        *b_pdo;   //!
  TBranch        *b_layerip;   //!
  TBranch        *b_globevent;   //!

  MakeTGCCluster(TString in, TString _out, int _run,
		 int pedestalOpt=0);
  ~MakeTGCCluster();

  void Loop();
  void SearchStoreClusters(int layer, TH1D& pdo_h, TH1D& tdo_h);
  void StoreCluster(int layer, TH1D& pdo_h, TH1D& tdo_h);
  void SetupOutput();
  bool IsGoodLoose(int _run, int _layer,
		   int tdomin, int tdomax, int pdomax);
  bool IsGoodMedium(bool loose , int nstrips, std::vector<int> _pdo);
  bool IsGoodTight (bool medium, int nstrips, std::vector<int> _pdo);
  double GetMappedClY(double _y_ch, int _layer);
  static double GetMappedClX(int m_run);
  static double YRelConvert(double yrel);
  static double GetYRelToStripCenter(double y_pitchUnits);
  static double GetYRelToGapCenter(double y_pitchUnits);

  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);

  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif
