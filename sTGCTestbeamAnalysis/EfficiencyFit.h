#ifndef EfficiencyFit_h
#define EfficiencyFit_h

#include <iostream>
#include <vector>

#include <TGraphErrors.h>
#include <TAxis.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TH2D.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TMatrixDSym.h>
#include <TMath.h>

#include "sTGCTestbeamAnalysis/Plotter.h"
#include "sTGCTestbeamAnalysis/Tools.h"
#include "sTGCTestbeamAnalysis/FitTools.h"


//using namespace FitTools;
//using namespace TMath;

struct EffFitResult {
  
  Parameter Nevt;
  Parameter eff1;
  Parameter eff2;
  Parameter eff3;
  Parameter eff4;
};

//double EffModel(double* x, double* par)
//{
//  int id = TMath::Nint(x[0]);
//
//  double n    = par[0];
//  double eff1 = par[1];
//  double eff2 = par[2];
//  double eff3 = par[3];
//  double eff4 = par[4];
//  
//  double rej1 = 1.-eff1;
//  double rej2 = 1.-eff2;
//  double rej3 = 1.-eff3;
//  double rej4 = 1.-eff4;
//
//  if (id== 1) return n * eff1 * eff2 * eff3 * eff4;
//  if (id== 2) return n * rej1 * eff2 * eff3 * eff4;
//  if (id== 3) return n * eff1 * rej2 * eff3 * eff4;
//  if (id== 4) return n * eff1 * eff2 * rej3 * eff4;
//  if (id== 5) return n * eff1 * eff2 * eff3 * rej4;
//  if (id== 6) return n * rej1 * rej2 * eff3 * eff4;
//  if (id== 7) return n * rej1 * eff2 * rej3 * eff4;
//  if (id== 8) return n * rej1 * eff2 * eff3 * rej4;
//  if (id== 9) return n * eff1 * rej2 * rej3 * eff4;
//  if (id==10) return n * eff1 * rej2 * eff3 * rej4;
//  if (id==11) return n * eff1 * eff2 * rej3 * rej4;
//  
//  cout << "STRANGE!!" << endl;
//  //  exit(1);
//  return 0.;
//}

class EfficiencyFit: public Plotter {
 public :

  const static int nRuns;
  const static int runList[];

  EfficiencyFit(TString _in, TString _out, TString treename);

  double GetRunLayerID(int run, int layer);
  int GetRun(double runLayerID);
  int GetLayer(double runLayerID);
  EffFitResult Execute(int run);
  int GetConfigID(bool l1, bool l2, bool l3, bool l4);
};
#endif
