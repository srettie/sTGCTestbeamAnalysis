#ifndef Parameter_h
#define Parameter_h

#include <TROOT.h>
#include <TObject.h>
#include <TMath.h>

class Parameter: public TObject {
 public:
  double value;
  double error;
  void Set(double _value, double _error);
 
  Parameter();
  Parameter(double _value, double _error);
  ~Parameter();

  ClassDef(Parameter,1);
};
#endif
