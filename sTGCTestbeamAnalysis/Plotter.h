#ifndef Plotter_h
#define Plotter_h

#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TString.h>
#include <TH1D.h>
#include <TDirectory.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <THStack.h>
#include <RooPlot.h>
#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooAbsPdf.h>

#include "sTGCTestbeamAnalysis/AtlasStyle.h"
#include "sTGCTestbeamAnalysis/Tools.h"


using namespace std;

class Plotter {

 protected:
  
  TString out;
  TFile* file;
  TTree* tree;  
  
 public :
  
  bool publicFormat;
  
  vector<TString> formatList;
  bool makingPdfs;
  TCanvas* c;
  TLatex latex;
  //  TLegend* legend;
  vector<TH1*> hlist;

  enum Type {
    type1D = 0,
    type2D,
    typeProfile};
  
  Plotter(TString _in, TString _out, TString treename);
  Plotter(TString _out);
  ~Plotter();
  void Plot(Type type,
	    TString varexp, TString selection, TString name,
	    TString title, TString xtitle, TString ytitle,
	    TString bin, bool log,
	    float min, float max, TString drawopt);

  void Plot(TString varexp, TString selection, TString name,
	    TString title, TString xtitle, TString ytitle,
	    TString bin="", bool log=false,
	    float min=-1111, float max=-1111, TString drawopt="hist");
  void Plot2D(TString varexp, TString selection, TString name,
	      TString title, TString xtitle, TString ytitle,
	      TString bin="",
	      float min=-1111, float max=-1111);
  void PlotProfile(TString varexp, TString selection, TString name,
		   TString title, TString xtitle, TString ytitle,
		   TString bin="", bool log=false,
		   float min=-1111, float max=-1111);
  void PlotStack(TString varexp, TString selection,
		 vector<TString> selList, vector<TString> nameList,
		 vector<TString> titleList,
		 TString name,
		 TString title, TString xtitle, TString ytitle,
		 TString bin="", bool log=false,
		 float min=-1111, float max=-1111);
  TGraphErrors* GetGraphErrors(TString varexp, TString selection);
  
  void PlotFit(TString name, TString title,
	       RooRealVar& var, int nbins, double min, double max,
	       RooDataSet& dat, RooAbsPdf& pdf,
	       RooAbsPdf* bkg=NULL, TString pdfNormRange="");
  void Plot(TString name, TString title,
	    RooRealVar& var, int nbins, double min, double max,
	    RooDataSet& dat);
  
  void SetPlotTitle(TString title);
  void SavePlot(TString name, bool log=false);
  void SaveHistos();
  void MergePdfs(TString plotlist="", TString name="", TString nupOpt="1x1");
};
#endif
