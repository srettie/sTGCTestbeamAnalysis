#ifndef PlotTGCCluster_h
#define PlotTGCCluster_h

#include "Plotter.h"

class PlotTGCCluster: public Plotter {
 public :

  PlotTGCCluster(TString _in, TString _out, TString treename, int run);

};
#endif
