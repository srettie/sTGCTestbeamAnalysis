//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec  8 11:10:32 2014 by ROOT version 5.34/23
// from TTree tgc_cluster/
// found on file: output/test1208_pfit_v00/tgc_cluster/tgc_cluster_run335_p-2.root
//////////////////////////////////////////////////////////

#ifndef TGCCluster_h
#define TGCCluster_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class TGCCluster {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           run;
   Int_t           event;
   Int_t           layer;
   Int_t           globevent;
   Int_t           nclusters;
   Int_t           ngoodclusters;
   vector<int>     *cl_raw_nstrip;
   vector<int>     *cl_raw_pdomin;
   vector<int>     *cl_raw_pdominneighbor;
   vector<int>     *cl_raw_pdomax;
   vector<int>     *cl_raw_pdomax2;
   vector<int>     *cl_raw_tdomin;
   vector<int>     *cl_raw_tdomax;
   vector<vector<int> > *cl_raw_ch_channel;
   vector<vector<int> > *cl_raw_ch_pdo;
   vector<double>  *cl_y_chhmean;
   vector<double>  *cl_sigma_chhmean;
   vector<double>  *cl_yrelStrip_hmean;
   vector<double>  *cl_yrelGap_hmean;
   vector<double>  *cl_y_hmean;
   vector<double>  *cl_y_chfitChi2;
   vector<double>  *cl_sigma_chfitChi2;
   vector<double>  *cl_yrelStrip_fitChi2;
   vector<double>  *cl_yrelGap_fitChi2;
   vector<double>  *cl_y_fitChi2;
   vector<double>  *cl_y_chfitLH;
   vector<double>  *cl_sigma_chfitLH;
   vector<double>  *cl_yrelStrip_fitLH;
   vector<double>  *cl_yrelGap_fitLH;
   vector<double>  *cl_y_fitLH;
   vector<int>     *cl_nstrip;
   vector<int>     *cl_pdomin;
   vector<int>     *cl_pdomax;
   vector<bool>    *cl_loose;
   vector<bool>    *cl_medium;
   vector<bool>    *cl_tight;
   vector<vector<int> > *cl_ch_channel;
   vector<vector<int> > *cl_ch_pdo;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_layer;   //!
   TBranch        *b_globevent;   //!
   TBranch        *b_nclusters;   //!
   TBranch        *b_ngoodclusters;   //!
   TBranch        *b_cl_raw_nstrip;   //!
   TBranch        *b_cl_raw_pdomin;   //!
   TBranch        *b_cl_raw_pdominneighbor;   //!
   TBranch        *b_cl_raw_pdomax;   //!
   TBranch        *b_cl_raw_pdomax2;   //!
   TBranch        *b_cl_raw_tdomin;   //!
   TBranch        *b_cl_raw_tdomax;   //!
   TBranch        *b_cl_raw_ch_channel;   //!
   TBranch        *b_cl_raw_ch_pdo;   //!
   TBranch        *b_cl_y_chhmean;   //!
   TBranch        *b_cl_sigma_chhmean;   //!
   TBranch        *b_cl_yrelStrip_hmean;   //!
   TBranch        *b_cl_yrelGap_hmean;   //!
   TBranch        *b_cl_y_hmean;   //!
   TBranch        *b_cl_y_chfitChi2;   //!
   TBranch        *b_cl_sigma_chfitChi2;   //!
   TBranch        *b_cl_yrelStrip_fitChi2;   //!
   TBranch        *b_cl_yrelGap_fitChi2;   //!
   TBranch        *b_cl_y_fitChi2;   //!
   TBranch        *b_cl_y_chfitLH;   //!
   TBranch        *b_cl_sigma_chfitLH;   //!
   TBranch        *b_cl_yrelStrip_fitLH;   //!
   TBranch        *b_cl_yrelGap_fitLH;   //!
   TBranch        *b_cl_y_fitLH;   //!
   TBranch        *b_cl_nstrip;   //!
   TBranch        *b_cl_pdomin;   //!
   TBranch        *b_cl_pdomax;   //!
   TBranch        *b_cl_loose;   //!
   TBranch        *b_cl_medium;   //!
   TBranch        *b_cl_tight;   //!
   TBranch        *b_cl_ch_channel;   //!
   TBranch        *b_cl_ch_pdo;   //!

   TGCCluster(TTree *tree=0);
   virtual ~TGCCluster();
/*    virtual Int_t    Cut(Long64_t entry); */
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TGCCluster_cxx
TGCCluster::TGCCluster(TTree *tree) : fChain(0) 
{
/* // if parameter tree is not specified (or zero), connect the file */
/* // used to generate this class and read the Tree. */
/*    if (tree == 0) { */
/*       TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("output/test1208_pfit_v00/tgc_cluster/tgc_cluster_run335_p-2.root"); */
/*       if (!f || !f->IsOpen()) { */
/*          f = new TFile("output/test1208_pfit_v00/tgc_cluster/tgc_cluster_run335_p-2.root"); */
/*       } */
/*       f->GetObject("tgc_cluster",tree); */

/*    } */
   Init(tree);
}

TGCCluster::~TGCCluster()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TGCCluster::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TGCCluster::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TGCCluster::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   cl_raw_nstrip = 0;
   cl_raw_pdomin = 0;
   cl_raw_pdominneighbor = 0;
   cl_raw_pdomax = 0;
   cl_raw_pdomax2 = 0;
   cl_raw_tdomin = 0;
   cl_raw_tdomax = 0;
   cl_raw_ch_channel = 0;
   cl_raw_ch_pdo = 0;
   cl_y_chhmean = 0;
   cl_sigma_chhmean = 0;
   cl_yrelStrip_hmean = 0;
   cl_yrelGap_hmean = 0;
   cl_y_hmean = 0;
   cl_y_chfitChi2 = 0;
   cl_sigma_chfitChi2 = 0;
   cl_yrelStrip_fitChi2 = 0;
   cl_yrelGap_fitChi2 = 0;
   cl_y_fitChi2 = 0;
   cl_y_chfitLH = 0;
   cl_sigma_chfitLH = 0;
   cl_yrelStrip_fitLH = 0;
   cl_yrelGap_fitLH = 0;
   cl_y_fitLH = 0;
   cl_nstrip = 0;
   cl_pdomin = 0;
   cl_pdomax = 0;
   cl_loose = 0;
   cl_medium = 0;
   cl_tight = 0;
   cl_ch_channel = 0;
   cl_ch_pdo = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("layer", &layer, &b_layer);
   fChain->SetBranchAddress("globevent", &globevent, &b_globevent);
   fChain->SetBranchAddress("nclusters", &nclusters, &b_nclusters);
   fChain->SetBranchAddress("ngoodclusters", &ngoodclusters, &b_ngoodclusters);
   fChain->SetBranchAddress("cl_raw_nstrip", &cl_raw_nstrip, &b_cl_raw_nstrip);
   fChain->SetBranchAddress("cl_raw_pdomin", &cl_raw_pdomin, &b_cl_raw_pdomin);
   fChain->SetBranchAddress("cl_raw_pdominneighbor", &cl_raw_pdominneighbor, &b_cl_raw_pdominneighbor);
   fChain->SetBranchAddress("cl_raw_pdomax", &cl_raw_pdomax, &b_cl_raw_pdomax);
   fChain->SetBranchAddress("cl_raw_pdomax2", &cl_raw_pdomax2, &b_cl_raw_pdomax2);
   fChain->SetBranchAddress("cl_raw_tdomin", &cl_raw_tdomin, &b_cl_raw_tdomin);
   fChain->SetBranchAddress("cl_raw_tdomax", &cl_raw_tdomax, &b_cl_raw_tdomax);
   fChain->SetBranchAddress("cl_raw_ch_channel", &cl_raw_ch_channel, &b_cl_raw_ch_channel);
   fChain->SetBranchAddress("cl_raw_ch_pdo", &cl_raw_ch_pdo, &b_cl_raw_ch_pdo);
   fChain->SetBranchAddress("cl_y_chhmean", &cl_y_chhmean, &b_cl_y_chhmean);
   fChain->SetBranchAddress("cl_sigma_chhmean", &cl_sigma_chhmean, &b_cl_sigma_chhmean);
   fChain->SetBranchAddress("cl_yrelStrip_hmean", &cl_yrelStrip_hmean, &b_cl_yrelStrip_hmean);
   fChain->SetBranchAddress("cl_yrelGap_hmean", &cl_yrelGap_hmean, &b_cl_yrelGap_hmean);
   fChain->SetBranchAddress("cl_y_hmean", &cl_y_hmean, &b_cl_y_hmean);
   fChain->SetBranchAddress("cl_y_chfitChi2", &cl_y_chfitChi2, &b_cl_y_chfitChi2);
   fChain->SetBranchAddress("cl_sigma_chfitChi2", &cl_sigma_chfitChi2, &b_cl_sigma_chfitChi2);
   fChain->SetBranchAddress("cl_yrelStrip_fitChi2", &cl_yrelStrip_fitChi2, &b_cl_yrelStrip_fitChi2);
   fChain->SetBranchAddress("cl_yrelGap_fitChi2", &cl_yrelGap_fitChi2, &b_cl_yrelGap_fitChi2);
   fChain->SetBranchAddress("cl_y_fitChi2", &cl_y_fitChi2, &b_cl_y_fitChi2);
   fChain->SetBranchAddress("cl_y_chfitLH", &cl_y_chfitLH, &b_cl_y_chfitLH);
   fChain->SetBranchAddress("cl_sigma_chfitLH", &cl_sigma_chfitLH, &b_cl_sigma_chfitLH);
   fChain->SetBranchAddress("cl_yrelStrip_fitLH", &cl_yrelStrip_fitLH, &b_cl_yrelStrip_fitLH);
   fChain->SetBranchAddress("cl_yrelGap_fitLH", &cl_yrelGap_fitLH, &b_cl_yrelGap_fitLH);
   fChain->SetBranchAddress("cl_y_fitLH", &cl_y_fitLH, &b_cl_y_fitLH);
   fChain->SetBranchAddress("cl_nstrip", &cl_nstrip, &b_cl_nstrip);
   fChain->SetBranchAddress("cl_pdomin", &cl_pdomin, &b_cl_pdomin);
   fChain->SetBranchAddress("cl_pdomax", &cl_pdomax, &b_cl_pdomax);
   fChain->SetBranchAddress("cl_loose", &cl_loose, &b_cl_loose);
   fChain->SetBranchAddress("cl_medium", &cl_medium, &b_cl_medium);
   fChain->SetBranchAddress("cl_tight", &cl_tight, &b_cl_tight);
   fChain->SetBranchAddress("cl_ch_channel", &cl_ch_channel, &b_cl_ch_channel);
   fChain->SetBranchAddress("cl_ch_pdo", &cl_ch_pdo, &b_cl_ch_pdo);
   Notify();
}

Bool_t TGCCluster::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TGCCluster::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
/* Int_t TGCCluster::Cut(Long64_t entry) */
/* { */
/* // This function may be called from Loop. */
/* // returns  1 if entry is accepted. */
/* // returns -1 otherwise. */
/*    return 1; */
/* } */
#endif // #ifdef TGCCluster_cxx
