#ifndef MakeCombEvent_h
#define MakeCombEvent_h

#include<iostream>
#include <vector>
#include <stdlib.h>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

#include "sTGCTestbeamAnalysis/TGCCluster.h"
#include "sTGCTestbeamAnalysis/PixelTrack.h"
#include "sTGCTestbeamAnalysis/Mapping.h"


// Header file for the classes stored in the TTree if any.


// Fixed size dimensions of array or collections stored in the TTree if any.

class MakeCombEvent {
 public :

  TGCCluster* cluster;
  PixelTrack* track;

  TString out;
  int run;
  int layer;

  TTree* outtree;

  int trk_nhits;
  double trk_chi2;
  double trk_x;
  double trk_y;
  double cl_y;
  double cl_yrelStrip;
  double cl_yrelGap;
  double cl_trk_dy;
  int cl_nstrip;
  int cl_raw_pdomax;
  int cl_raw_pdomax2;
  int cl_raw_tdomin;
  int cl_raw_tdomax;
  vector<int>* cl_ch_channel;
  vector<int>* cl_ch_pdo;
  int evtoffset;

  MakeCombEvent(TString intrack, TString incluster, TString _out,
		int _run, int _layer);
  ~MakeCombEvent();
  void SetupOutput();
  void Loop();
  void StoreEvent();
  
};

#endif
