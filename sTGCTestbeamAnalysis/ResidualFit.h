#ifndef ResidualFit_h
#define ResidualFit_h

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>

#include <TROOT.h>
#include <TObject.h>
#include <TTree.h>
#include <TF1.h>
#include <TF2.h>
#include <TStyle.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TString.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TChain.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TFile.h>
#include <TLine.h>
#include <TMatrixDSym.h>

#include <RooPlot.h>
#include <RooCmdArg.h>
#include <RooAbsPdf.h>
#include <RooRealVar.h>
#include <RooFormulaVar.h>
#include <RooGaussian.h>
#include <RooProdPdf.h>
#include <RooArgSet.h>
#include <RooConstVar.h>
#include <RooDataSet.h>
#include <RooFitResult.h>
#include <RooRandom.h>
#include <RooNumConvPdf.h>
#include <RooGenericPdf.h>
#include <RooAbsDataStore.h>
#include <RooTreeDataStore.h>
#include <RooAddPdf.h>
#include <RooUniform.h>
#include <RooBinning.h>
#include <RooBernstein.h>
#include <RooPolynomial.h>

#include "sTGCTestbeamAnalysis/Mapping.h"
#include "sTGCTestbeamAnalysis/Plotter.h"
#include "sTGCTestbeamAnalysis/Tools.h"
#include "sTGCTestbeamAnalysis/FitTools.h"
#include "sTGCTestbeamAnalysis/FitResult.h"


//using namespace FitTools;

void RunResidualFit(TString in, TString out, int run, int layer, int pedestal);

//#if !defined(__CINT__) || defined(__MAKECINT__)
//#pragma link C++ class FitResult+;
//#pragma link C++ class std::vector<FitResult*>+;
//#endif

/* struct FitResult { */
  
/*   Parameter offset; */
/*   Parameter phi; */
/*   Parameter ampl3; */
/*   Parameter ampl4; */
/*   Parameter ampl5; */
/*   Parameter sigma; */
/*   Parameter sigfrac3; */
/*   Parameter sigfrac4; */
/*   Parameter sigfrac5; */
/*   Parameter nevts3; */
/*   Parameter nevts4; */
/*   Parameter nevts5; */
/*   int nevts_tot; */
/*   int nevts_tot3; */
/*   int nevts_tot4; */
/*   int nevts_tot5; */
/*   int status; */
/*   int covqual; */
/* }; */


class ResidualFit: public Plotter {
 public :

  TString dytitle;
  
  RooRealVar event;
  RooRealVar tgcevent;

  RooRealVar trk_nhits;
  RooRealVar trk_chi2;
  RooRealVar trk_x;
  RooRealVar trk_y;

  RooRealVar cl_y;
  RooRealVar cl_yrelStrip;
  RooRealVar cl_yrelGap;
  RooRealVar cl_nstrip;
  RooRealVar cl_raw_pdomax;
  RooRealVar cl_raw_tdomin;
  RooRealVar cl_raw_tdomax;

  RooRealVar dy;

  RooRealVar* dy_offset0;
  RooRealVar* new_dy;
  
  RooArgSet argSet;
  
  int run;
  int layer;
  int pedestal;
  TString globname;
  TString globtitle;
  
  RooDataSet* fulldata;
  FitResult* fitresult;

  double fitmin;
  double fitmax;
  double fitrangeSize;

  int plotNBins;
  double plotmin;
  double plotmax;

  ResidualFit(TString _name, TString _title,
	      TString _in, TString _out, TString selection,
	      int _run, int _layer, int _pedestal);
  ~ResidualFit();

  void Execute();
  void SaveProcessedData();
  GausFitRes BasicFit();
  FitResult* AlignSshapeFit(TString name, TString title,
			    vector<TString> catName,
			    vector<TString> catSel,
			    RooArgList catArgList);

  GausFitRes PlotGausFit(TString name, TString title,
			 RooRealVar& var,
			 RooDataSet* dat=NULL);
  void PlotResiduals(TString name, TString title, RooRealVar& var,
		     RooDataSet* dat=NULL);
  void PlotDyVsVar(TString name, TString title,
		   RooRealVar& dy, RooCmdArg dy_bins,
		   RooRealVar& var, RooCmdArg var_bins,
		   RooDataSet& dat, TF1* f=NULL);
};
#endif
