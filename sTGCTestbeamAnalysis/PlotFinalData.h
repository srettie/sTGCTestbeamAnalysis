#ifndef PlotFinalData_h
#define PlotFinalData_h

#include "Plotter.h"
#include "Tools.h"
#include "FitTools.h"

using namespace FitTools;

struct PDOMaxBin {
  int min;
  int max;
  int center;
  Parameter pdomax;
  GausFitRes res;
};

class PlotFinalData: public Plotter {
 public :

  PlotFinalData(TString _in, TString _out, TString treename);
  GausFitRes GausFit(TString name, TString title,
		     TString selection,
		     TString dyname="dy", double min=-300., double max=300.);
  void PDOMaxStudy(int run);
};
#endif
